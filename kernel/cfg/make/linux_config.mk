ASM      := $(TARGET)-as
CC       := $(TARGET)-gcc
CPP      := $(TARGET)-g++
LD       := $(TARGET)-g++
GDB      := gdb
FIND     := find
EMU      := qemu-system-i386
GRUB     := grub-mkrescue
