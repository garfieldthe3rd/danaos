VERSION      := 0.0.1
TARGET       := i686-elf
NAME         := Danaos
PLATFORM     := unknown
# If you want explanatory output, set the variable to any value
VERBOSE      :=

# Check the platform to include the correct program variables (see cfg/make)
ifeq ($(OS),Windows_NT)
	PLATFORM := windows
endif

ifeq ($(shell uname -s), Linux)
	PLATFORM := linux
endif

include cfg/make/$(PLATFORM)_config.mk

# Specify the directories of the project
SRCDIR       := ./src
INCDIR       := ./src
CFGDIR       := ./cfg
OBJDIR       := ./build/obj
BINDIR       := ./build/bin
ISODIR       := ./build/iso
DOCDIR       := ./doc

# Name of the executable/iso file
BIN          := $(BINDIR)/$(NAME)_$(TARGET)_$(VERSION).bin
ISO          := $(ISODIR)/$(NAME)_$(TARGET)_$(VERSION).iso

# Scripts for the linker, the debugger, and the code formatter
LINKERSCRIPT := $(CFGDIR)/linker/script.ld
DEBUGSCRIPT  := $(CFGDIR)/debug/gdb.script
FORMATSCRIPT := $(CFGDIR)/format/clang-format-diff.py

# The flags for compilation. TODO: is it bad practice to define them so absolutely?
CCFLAGS      := -ffreestanding -O0 -std=C11 -m32 -g -Wall -Wextra
CPPFLAGS     := -ffreestanding -O0 -std=c++14 -fno-exceptions -fno-rtti -m32 -g -Wall -Wextra
ASMFLAGS     := --32
LDFLAGS      := -ffreestanding -O0 -nostdlib -lgcc --disable-__cxa_atexit
EMUFLAGS     := -no-kvm -net none -vga std -m 64 -serial file:serial.log
EMUDEBUG     := -s -S
GDBFLAGS     := -x $(DEBUGSCRIPT)

# Gcc-specific files responsible for (amongst other things) calling global con- and destructors
CRTBEGIN     := $(shell $(CC) $(CFLAGS) -print-file-name=crtbegin.o)
CRTEND       := $(shell $(CC) $(CFLAGS) -print-file-name=crtend.o)
CRTI         := $(OBJDIR)/crti.o
CRTN         := $(OBJDIR)/crtn.o

# Scan the source directory for C, C++ and assembly files (marked by the endings .c, .cc, and .s!)
SRC          := $(shell $(FIND) -name "*.s") $(shell $(FIND) -name "*.c") $(shell $(FIND) -name "*.cc")

# Get the corresponding object files by substituting the endings and the directory
OBJ          := $(notdir $(SRC))
OBJ          := $(addprefix $(OBJDIR)/,$(OBJ))
OBJ          := $(subst .s,.o,$(OBJ))
OBJ          := $(subst .cc,.o,$(OBJ))
OBJ          := $(subst .c,.o,$(OBJ))
OBJ          := $(patsubst %/crti.o,,$(OBJ))
OBJ          := $(patsubst %/crtn.o,,$(OBJ))
# Dependencies for the build process
DEP          := $(patsubst %.o,%.d,$(OBJ))
# Tell make where to look for the files in the rules
VPATH        := $(dir $(SRC))

.PHONY: all build clean debug doc format iso main-build pre-build post-build run 


all: post-build

pre-build:
	@mkdir -p $(OBJDIR)
	@mkdir -p $(BINDIR)
	@echo "Platform: $(PLATFORM)"
	@echo "(BUILD)"

post-build: main-build
	@echo "(BUILD)  done"
	
main-build: pre-build
	@$(MAKE) --no-print-directory build

build: $(BIN)
# The binary depends on the object files which have sources and the crti etc. files from gcc (the link order is important!)
$(BIN): $(CRTI) $(CRTBEGIN) $(OBJ) $(CRTEND) $(CRTN)
ifdef VERBOSE
	@echo "    (LD)  Linking program..."
endif
	@$(LD) -o $(BIN) -T $(LINKERSCRIPT) $(LDFLAGS) $^

# Include the dependency rules (if present; if not, we have to build the obj file anyway)
-include $(DEP)

# Assembly rule
$(OBJDIR)/%.o : %.s Makefile
ifdef VERBOSE
	@echo "    (ASM)  $< --> $@"
endif
	@$(ASM) -MD $(patsubst %.o,%.d,$@) $< -o $@ -I $(INCDIR) $(ASMFLAGS)

# C rule
$(OBJDIR)/%.o : %.c Makefile
ifdef VERBOSE
	@echo "    (C)  $< --> $@"
endif
	@$(CC) -MD -c $< -o $@ -I $(INCDIR) $(CCFLAGS)

# C++ rule
$(OBJDIR)/%.o : %.cc Makefile
ifdef VERBOSE
	@echo "    (CC)  $< --> $@"
endif
	@$(CPP) -MD -c $< -o $@ -I $(INCDIR) $(CPPFLAGS)

clean:
	@echo "(CLEAN)"
	
ifdef VERBOSE
	@echo "    (CLEAN)  Deleting binary..."
endif
	-@rm -rf $(BINDIR)/*
ifdef VERBOSE
	@echo "    (CLEAN)  Deleting object files..."
endif
	-@rm -rf $(OBJDIR)/*
ifdef VERBOSE
	@echo "    (CLEAN)  Deleting documentation..."
endif
	-@rm -f $(DOCDIR)/*
ifdef VERBOSE
	@echo "    (CLEAN)  Deleting ISO directory..."
endif
	-@rm -rf $(ISODIR)/*
	
	@echo "(CLEAN)  done"

# TODO: Currenty, debug symbols are linked into the obj files even if we don't want to debug
debug: $(BIN)
	@echo "(DEBUG)"
	$(EMU) $(EMUFLAGS) -kernel $< $(EMUDEBUG) -daemonize && $(GDB) $< $(GDBFLAGS)
	@echo "(DEBUG)  done"
	
doc:
	@echo "(DOC)"
	@rm -r $(DOCDIR)
	@mkdir $(DOCDIR)
	@doxygen $(DOCDIR)/Doxyfile
	@echo "(DOC)  done"

# Use clang-format to automatically apply the style guide to all dirty git files
format:
	@echo "(FORMATTING)"
	@git diff -U0 HEAD^ | python $(FORMATSCRIPT) -i -p2
	@echo "(FORMATTING)  done"

# Create a bootable iso file with grub-mkrescue
iso: $(ISO)

$(ISO): post-build
	@echo "(ISO)"
ifdef VERBOSE
	@echo "    (ISO)  Creating directories..."
endif
	@mkdir -p $(ISODIR)/isodir
	@mkdir -p $(ISODIR)/isodir/boot
	@mkdir -p $(ISODIR)/isodir/boot/grub
ifdef VERBOSE
	@echo "    (ISO)  Copying binaries..."
endif
	@cp $(BIN) $(ISODIR)/isodir/boot/$(notdir $(BIN))
ifdef VERBOSE
	@echo "    (ISO)  Configuring..."
endif
	@echo "set timeout=15\nset default=0\n\nmenuentry \"$(basename $(notdir $(BIN)))\" {\n   multiboot /boot/$(notdir $(BIN))\n   boot\n}"\
		> $(ISODIR)/isodir/boot/grub/grub.cfg
	@$(GRUB) --output $(ISO) $(ISODIR)/isodir
	
	@echo "(ISO)  done"

run: $(BIN)
	@echo "(RUN)"
	@$(EMU) $(EMUFLAGS) -kernel $<
	@echo "(RUN)  done"
