/**
 * boot/kernelstruct.h
 *
 * The sole reason for the existence of this header file is because you cannot forward-declare
 * nested classes in C++ and including the kernel header file would result in a circular dependency.
**/

#ifndef DANAOS_BOOT_KERNELSTRUCT_H_
#define DANAOS_BOOT_KERNELSTRUCT_H_

#include <stdint.h>

struct KernelStructure {
    uintptr_t virtualAddrOffset;
    uintptr_t kernelStart, kernelEnd, kernelSize;
    uintptr_t codeStart, codeEnd;
    uintptr_t rodataStart, rodataEnd;
    uintptr_t dataStart, dataEnd;
    uintptr_t bssStart, bssEnd;
};

#endif // DANAOS_BOOT_KERNELSTRUCT_H_