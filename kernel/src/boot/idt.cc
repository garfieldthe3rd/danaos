/**
 * boot/idt.cc
**/

#include "idt.h"
#include "libk/math.h"
#include "main/kernel.h"

// Get the size of an ISR wrapper from assembly
// TODO: The actual value is within the address for some reason
extern uint32_t isrWrapperSize __asm__("ISR_WRAPPER_SIZE");
const uint32_t InterruptDescriptorTable::ISR_WRAPPER_SIZE =
    reinterpret_cast<uint32_t>(&isrWrapperSize);

InterruptDescriptorTable::Gate::Gate()
    : lowerOffset(0),
      segmentSelector(),
      reserved1(0),
      type(GateType::INTERRUPT),
      size32(0),
      reserved2(0),
      privilege(Privilege::LEVEL_0),
      present(0),
      upperOffset(0) {
}

InterruptDescriptorTable::Gate::Gate(uint32_t address,
                                     GlobalDescriptorTable::Selector segmentSelector, GateType type,
                                     Privilege privilege, bool size32)
    : lowerOffset(address & 0xFFFF),
      segmentSelector(segmentSelector),
      reserved1(0),
      type(type),
      size32(size32),
      reserved2(0),
      privilege(privilege),
      present(1),
      upperOffset((address >> 16) & 0xFFFF) {
}

InterruptDescriptorTable::Gate &InterruptDescriptorTable::Gate::operator=(const Gate &gate) {
    lowerOffset     = gate.lowerOffset;
    segmentSelector = gate.segmentSelector;
    type            = gate.type;
    size32          = gate.size32;
    privilege       = gate.privilege;
    present         = gate.present;
    upperOffset     = gate.upperOffset;

    return *this;
}

InterruptDescriptorTable::InterruptDescriptorTable()
    : table(new Gate[GATE_COUNT]),
      descriptor{static_cast<uint16_t>(sizeof(Gate) * GATE_COUNT - 1),
                 reinterpret_cast<uint32_t>(table)} {
    // Get the address of the first ISR wrapper (defined in assembly)
    // There are 256 of these wrappers, sequential in memory
    // So for each table entry, set this wrapper (which will basically
    // just call another method and give it its number) as the vector
    // TODO: do we care about the GDT selector?
    extern void firstIsrWrapper() __asm__("_isrWrapper0");
    for (size_t i = 0; i < GATE_COUNT; i++) {
        table[i] = Gate(reinterpret_cast<uint32_t>(&firstIsrWrapper) + i * ISR_WRAPPER_SIZE,
                        Kernel::gdt->getSelector(0), GateType::INTERRUPT, Privilege::LEVEL_0, 1);
    }
}

InterruptDescriptorTable::~InterruptDescriptorTable() {
    // TODO: any other clean-up? Maybe disable interrupts?
    delete[] table;
}

void InterruptDescriptorTable::setGate(uint8_t index, Gate gate) {
    // TODO: assert
    if (index >= entries) {
        // Actually cannot happen if we assume interrupt count of 256
        Kernel::panic("Cannot set IDT entry: {} >= {}!", index, GATE_COUNT);
    }

    table[index] = gate;
}

void InterruptDescriptorTable::reload() {
    // TODO: wtf when could this change?
    descriptor.size = static_cast<uint16_t>(sizeof(Gate) * GATE_COUNT - 1);

    __asm__ volatile("lidt (%0)" : : "d"(reinterpret_cast<uint32_t>(&descriptor)));
    Kernel::console.log(KernelConsole::LogLevel::STATUS, "Initialized IDT with {} entries!",
                        entries);
}