# Creates interrupt wrappers which call the 'real' ISR

.global			_isrWrapper0
.global			ISR_WRAPPER_SIZE

.set			ISR_WRAPPER_COUNT, 256
.set			EXCEPTION_COUNT, 32			// Amount of exceptions
.set			ISR_WRAPPER_SIZE, 0x10		// 16 bytes for any wrapper


.macro	gen_normal_isr
	.align		16					// We have to align this as the wrapper size apparently varies...
_isrWrapper\@:
    pushl		$0					// To replace the missing error code, we push a zero onto the stack
    pushl		$\@
	jmp			isrCaller
.endm

.macro	gen_error_isr
	.align		16					// We have to align this as the wrapper size apparently varies...
_isrWrapper\@:
    pushl		$\@
	jmp			isrCaller
.endm

// Exceptions
gen_normal_isr	// 0
gen_normal_isr	// 1
gen_normal_isr	// 2
gen_normal_isr	// 3
gen_normal_isr	// 4
gen_normal_isr	// 5
gen_normal_isr	// 6
gen_normal_isr	// 7
gen_error_isr	// 8
gen_normal_isr	// 9
gen_error_isr	// 10
gen_error_isr	// 11
gen_error_isr	// 12
gen_error_isr	// 13
gen_error_isr	// 14
gen_normal_isr	// 15
gen_normal_isr	// 16
gen_error_isr 	// 17
gen_normal_isr	// 18
gen_normal_isr	// 19
gen_normal_isr	// 20
gen_normal_isr	// 21
gen_normal_isr	// 22
gen_normal_isr	// 23
gen_normal_isr	// 24
gen_normal_isr	// 25
gen_normal_isr	// 26
gen_normal_isr	// 27
gen_normal_isr	// 28
gen_normal_isr	// 29
gen_error_isr	// 30
gen_normal_isr	// 31

// Hard- and software interrupts
.rept				ISR_WRAPPER_COUNT - EXCEPTION_COUNT
	gen_normal_isr
.endr

isrCaller:
	cld												// Clear the direction bit in accordance
													// with C calling convention
	
	// TODO: what registers do we have to save?
	// (Probably the volatile ones, except the FPU registers
	// since we don't use it in the kernel???)
	pushl		%ebp
	pushl		%edi
	pushl		%esi
	pushl		%gs
	pushl		%fs
	pushl		%es
	pushl		%edx
	pushl		%ecx
	pushl		%ebx
	pushl		%eax
	
	pushl		%esp
	call		isrHandler
	addl		$4, %esp
	
	# Restore the registers
	popl		%eax
	popl		%ebx
	popl		%ecx
	popl		%edx
	popl		%es
	popl		%fs
	popl		%gs
	popl		%esi
	popl		%edi
	popl		%ebp
	
	addl		$8, %esp					# Remove error code and interrupt number from stack
	
	iret

