/**
 * boot/gdt.cc
 *
 * Contains the global descriptor table and related methods.
 * For any change to take effect, reload() has to be called!
**/

#include "gdt.h"
#include "main/kernel.h"
#include "libk/math.h"

// Creates a null descriptor
GlobalDescriptorTable::Entry::Entry()
    : lowerLimit(0), lowerBase(0),
        middleBase(0), accessed(0),
        readWritable(0), conforming(0),
        executable(0), ignored1(1),
        privilege(Privilege::LEVEL_0), present(0),
        upperLimit(0), ignored2(0),
        mode64(0), size32(0),
        pageGranular(0), upperBase(0) {
}

GlobalDescriptorTable::Entry::Entry(uint32_t base, uint32_t limit, Privilege privilege,
                bool readWritable, bool conforming, bool executable,
                bool size32, bool pageGranular)
    : lowerLimit(limit & 0xFFFF), lowerBase(base & 0xFFFF),
        middleBase((base >> 16) & 0xFF), accessed(0),
        readWritable(readWritable), conforming(conforming),
        executable(executable), ignored1(1),
        privilege(privilege), present(1),
        upperLimit((limit >> 16) & 0xF), ignored2(0),
        mode64(0), size32(size32), pageGranular(pageGranular),
        upperBase((base >> 24) && 0xFF) {
}

GlobalDescriptorTable::Entry& GlobalDescriptorTable::Entry::operator=(const Entry &entry) {
    // Simply copy everything
    lowerLimit = entry.lowerLimit;
    lowerBase = entry.lowerBase;
    middleBase = entry.middleBase;
    accessed = entry.accessed;
    readWritable = entry.readWritable;
    conforming = entry.conforming;
    executable = entry.executable;
    privilege = entry.privilege;
    present = entry.present;
    upperLimit = entry.upperLimit;
    mode64 = entry.mode64;
    size32 = entry.size32;
    pageGranular = entry.pageGranular;
    upperBase = entry.upperBase;

    return *this;
}

GlobalDescriptorTable::GlobalDescriptorTable(uint16_t entries)
    : capacity(math::max(entries + 1, 1)), // at least 1 entry, also take care of overflow
      currentIndex(1),
      table(new Entry[capacity]),
      descriptor{0, reinterpret_cast<uint32_t>(table)} {
    // TODO: wtf shouldn't this be the linear address?
    // TODO: limit capacity to uint16_t limit - 1!

    // The first entry has to be the null descriptor
    // But since we're initializing all of them will be made
    // 'null' at first (doesn't really matter)
    for(size_t i = 0; i < capacity; i++) {
        table[i] = Entry();
    }
}

GlobalDescriptorTable::~GlobalDescriptorTable() {
    // TODO: any more clean-up?
    delete[] table;
}

void GlobalDescriptorTable::addEntry(Entry entry) {
    if(currentIndex >= capacity) {
        // TODO: copy
        // Until we implement copy, panic when we attempt to add too many entries
        Kernel::panic("Ran out of GDT capacity: {} >= {}", currentIndex, capacity);
    }

    // Set the entry and increase the current counter
    table[currentIndex++] = entry;
}

void GlobalDescriptorTable::clearEntry(uint16_t index) {
    // TODO: assert?
    // Add one due to the null descriptor
    index = math::max<uint16_t>(1, index + 1);
    if(index < capacity) {
        // Make the selected entry a null segment
        table[index] = Entry();
    }
}

void GlobalDescriptorTable::setEntry(uint16_t index, Entry entry) {
    // TODO: assert?
    // Add one due to the null descriptor
    index = math::max<uint16_t>(1, index + 1);
    if(index < capacity) {
        table[index] = entry;
    }
}

GlobalDescriptorTable::Selector GlobalDescriptorTable::getSelector(uint16_t index) {
    // TODO: assert!
    // TODO: check index overflow
    if (index >= capacity) {
        Kernel::panic("GDT entry beyond capacity: {} >= {}", index, capacity);
    }
    return Selector{table[index].privilege, 0, static_cast<uint16_t>(index + 1)};
}

void GlobalDescriptorTable::reload() {
    extern void loadGdt(Descriptor descriptor) __asm__("_loadGdt");

    descriptor.size = static_cast<uint16_t>(sizeof(Entry) * currentIndex - 1);
    // TODO: disable interrupts!
    loadGdt(descriptor);
    Kernel::console.log(KernelConsole::LogLevel::STATUS, "Initialized GDT with {} entries!",
                        currentIndex);
}