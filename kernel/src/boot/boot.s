/**
 * boot/boot.s
 * 
 * Contains the startup code for the kernel. Function '_start' is the entry point for the linker.
 * The bootloader currently has to be multiboot compliant and put the machine into protected mode.
 * Furthermore, the memory for the kernel stack is reserved.
**/

#define ASM

.global			_entry, _start
.global			_pageDirectory

// Multiboot header constants
.set			ALIGN,					1 << 0				// align loaded modules on page boundaries
.set			MEMINFO,				1 << 1				// provide memory map
.set			FLAGS,					ALIGN | MEMINFO		// this is the Multiboot 'flag' field
.set			MAGIC,					0x1BADB002			// 'magic number' lets bootloader find the header
.set			CHECKSUM,				-(MAGIC + FLAGS)	// checksum of above, to prove we are multiboot

// Kernel constants
.set			KERNEL_STACK_SIZE,		32768
.set			KERNEL_VIRTUAL_ADDR,	0xF0000000
.set			KERNEL_PAGE_INDEX,		KERNEL_VIRTUAL_ADDR >> 22

// Paging constants
.set			PAGING_DIR_ENTRIES,		1024
.set			PAGING_PRESENT_RW_4MB,	0x83
.set			PAGING_PRESENT_RW_4KB,	0x03
.set			PAGING_CR0_PG,			0x80000000
.set			PAGING_CR4_PSE,			0x10

// Set the linker entry point address to the actual PHYSICAL address of the kernel
// so that the execution starts at the right address
.set			_entry,					_start

// Multiboot section
.align			4
.section .multiboot
.long			MAGIC
.long			FLAGS
.long			CHECKSUM

// Kernel entry point
.section .text

_start:
	cli			// TODO: are interrupts already disabled?
	// Load physical address of page directory
	movl		$(_pageDirectory - KERNEL_VIRTUAL_ADDR), %ecx
	movl		%ecx, %cr3

	// Enable PSE (needed for 4MB pages)
	movl		%cr4, %ecx
	orl			$PAGING_CR4_PSE, %ecx
	movl		%ecx, %cr4
	// Enable paging
	movl		%cr0, %ecx
	orl			$PAGING_CR0_PG, %ecx
	movl		%ecx, %cr0
	
	// Perform long-jump to reload segment registers (TODO: necessary?)
	movl		$startHigherHalf, %ecx
	jmp			*%ecx

startHigherHalf:
	// Setup initial kernel stack
	movl		$kernel_stack + KERNEL_STACK_SIZE, %esp

	// Push the parameters for the main kernel function here already, since
	// '_init' may overwrite them
	pushl		%ebx					// Multiboot structure
	pushl		%eax					// Magic number
	
	// Let gcc handle the constructor calls
	call		_init
	
	// Call kernel main function with multiboot info
	call		kernelMain
	
	// Let gcc handle the desctructor calls 
	call		_fini
	
	// Disable interrupts and halt machine
	cli
.hang:
	hlt
	jmp			.hang

// Data segment
.section .data
// Initial page directory. It does two things:
// 1. Map 0x00000000-0x00400000 virtual to 0-4MB physical
// 2. Map 0xF0000000-0xF0400000 virtual to 0-4MB physical
.align			0x1000
_pageDirectory:
	.int		PAGING_PRESENT_RW_4MB
	.skip		4 * (KERNEL_PAGE_INDEX - 1), 0
	.int		PAGING_PRESENT_RW_4MB
	.skip		4 * (PAGING_DIR_ENTRIES - KERNEL_PAGE_INDEX - 2), 0
	.int		PAGING_PRESENT_RW_4KB | 0x107000

// Read-only data segment
.section .rodata

// Uninitialized data segment
.section .bss
// Reserve space for initial kernel stack
	.align		32
	.comm		kernel_stack, KERNEL_STACK_SIZE
