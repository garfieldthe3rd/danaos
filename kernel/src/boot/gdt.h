/**
 * boot/gdt.h
 *
 * The global descriptor table defines the segments that
 * the CPU will use for everything with the correct
 * privilege. For per-program segments, the local descriptor
 * table needs to be used.
 * Since DANAOS uses paging for IA-32, the only segments
 * to be defined should cover the entirety of the address
 * space for code, data and level 0 and 3 privileges.
 * Also, the very first entry has to be a null descriptor.
 * This is taken care of by the class; it cannot be modified
 * or removed and doesn't count as a descriptor for the
 * purpose of indexing.
**/

#ifndef DANAOS_BOOT_GDT_H_
#define DANAOS_BOOT_GDT_H_

#include <stdint.h>
#include <stddef.h>

class GlobalDescriptorTable {
public:
    enum class Privilege : uint8_t {
        LEVEL_0 = 0,
        LEVEL_1 = 1,
        LEVEL_2 = 2,
        LEVEL_3 = 3
    };

    // Describes the entries of the GDT
    struct Entry {
        uint16_t lowerLimit;
        uint16_t lowerBase;
        uint8_t middleBase;
        uint8_t accessed        : 1;
        uint8_t readWritable    : 1;
        uint8_t conforming      : 1;
        uint8_t executable      : 1;
        const uint8_t ignored1  : 1;
        Privilege privilege     : 2;
        uint8_t present         : 1;
        uint8_t upperLimit      : 4;
        const uint8_t ignored2  : 1;
        uint8_t mode64          : 1;    // Must not be used in x32!
        uint8_t size32          : 1;
        uint8_t pageGranular    : 1;
        uint8_t upperBase;

        Entry();
        Entry(uint32_t base, uint32_t limit, Privilege privilege,
                bool readWritable, bool conforming, bool executable,
                bool size32, bool pageGranular);
        Entry& operator=(const Entry &entry);
    } __attribute__((packed));

    struct Selector {
        Privilege privilege : 2;
        uint8_t localTable : 1;
        uint16_t index : 13;
    } __attribute__((packed));

private:
    struct Descriptor {
        uint16_t size;
        uint32_t address;
    } __attribute__((packed));

    uint16_t capacity, currentIndex;
    Entry *table;
    Descriptor descriptor;

public:
    GlobalDescriptorTable(uint16_t entries);
    ~GlobalDescriptorTable();

    void addEntry(Entry entry);
    void clearEntry(uint16_t index);
    void setEntry(uint16_t index, Entry entry);

    Selector getSelector(uint16_t index);

    void reload();
};

#endif //DANAOS_BOOT_GDT_H_