/**
 * boot/idt.h
**/

#ifndef DANAOS_BOOT_IDT_H_
#define DANAOS_BOOT_IDT_H_

#include "gdt.h"
#include <stdint.h>

class InterruptDescriptorTable {
public:
    enum class Privilege : uint8_t { LEVEL_0 = 0, LEVEL_1 = 1, LEVEL_2 = 2, LEVEL_3 = 3 };

    enum class GateType : uint8_t { TASK = 5, INTERRUPT = 6, TRAP = 7 };

    struct Gate {
        uint16_t lowerOffset;
        GlobalDescriptorTable::Selector segmentSelector;
        const uint8_t reserved1;
        GateType type : 3;
        uint8_t size32 : 1;
        const uint8_t reserved2 : 1;
        Privilege privilege : 2;
        uint8_t present : 1;
        uint16_t upperOffset;

        Gate();
        Gate(uint32_t address, GlobalDescriptorTable::Selector segmentSelector, GateType type,
             Privilege privilege, bool size32);
        Gate &operator=(const Gate &gate);
    } __attribute__((packed));

    static constexpr uint16_t GATE_COUNT = 256;

private:
    struct Descriptor {
        uint16_t size;
        uint32_t address;
    } __attribute__((packed));
    static const uint32_t ISR_WRAPPER_SIZE;

    uint16_t entries;
    // TODO: keep on heap or on stack (or pre-allocate)?
    Gate *table;
    Descriptor descriptor;

public:
    InterruptDescriptorTable();
    ~InterruptDescriptorTable();

    void setGate(uint8_t index, Gate gate);

    void reload();
};

#endif // DANAOS_BOOT_IDT_H_