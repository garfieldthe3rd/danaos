/**
 * main/kernel.h
**/

#ifndef DANAOS_MAIN_KERNEL_H_
#define DANAOS_MAIN_KERNEL_H_

#include "boot/gdt.h"
#include "boot/idt.h"
#include "boot/kernelstruct.h"
#include "boot/multiboot.h"
#include "machine/devices/cga.h"
#include "machine/devices/cpu.h"
#include "machine/devices/pic.h"
#include "machine/devices/ps2/ps2.h"
#include "machine/devices/ps2/ps2keyboard.h"
#include "machine/devices/timer/cmos.h"
#include "machine/interrupts/isr.h"
#include "machine/io/serial.h"
#include "machine/memory/heap.h"
#include "machine/memory/memmanager.h"
#include "main/console.h"

class Kernel {
private:
    static KernelStructure structure;

    Kernel() = delete;

public:
    static SerialPort com1;
    static Cmos cmos;
    static CgaScreen cga;
    static KernelConsole console;
    static MemoryManager memManager;
    static Heap *heap;
    static GlobalDescriptorTable *gdt;
    static InterruptDescriptorTable *idt;
    static IsrPlugbox *isrPlugbox;
    static InterruptController *pic;
    static Ps2Controller *ps2;
    static Ps2Keyboard *keyboard;

    static void run(const multiboot_info_t *info);

    template <class... Args>
    static void __attribute__((noreturn)) panic(const char *msg, Args... args) {
        // Until we have something like a bluescreen and proper logging, a panic
        // simply prints some message, disables interrupts and halts the machine
        // TODO: also disable NMIs?
        console.log(KernelConsole::LogLevel::ERROR, "Kernel panic: ");
        // Perfect forwarding manually, since we don't have std::forward (yet)
        console.log(KernelConsole::LogLevel::ERROR, msg, static_cast<Args &&>(args)...);

        while (true) {
            cpu::disableInterrupts();
            cpu::halt();
        }
    }
};

#endif // DANAOS_MAIN_KERNEL_H_
