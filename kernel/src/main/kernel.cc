/**
 * main/kernel.cc
 *
 * Contains the entry point for the 'high-level', C++ part of the kernel.
 * Every global object needed is defined here in order of initialization, as
 * well.
**/

#include <stddef.h>
#include <stdint.h>

#include "boot/multiboot.h"
#include "libk/new.h"
#include "libk/string.h"
#include "machine/devices/timer/pit.h"
#include "machine/devices/timer/rtc.h"
#include "main/kernel.h"

// The symbols declared in the linker script can be used to determine kernel
// size
extern uintptr_t KERNEL_VIRT_OFFSET;
extern uintptr_t KERNEL_START;
extern uintptr_t KERNEL_CODE;
extern uintptr_t KERNEL_RODATA;
extern uintptr_t KERNEL_DATA;
extern uintptr_t KERNEL_BSS;
extern uintptr_t KERNEL_END;

KernelStructure Kernel::structure{
    reinterpret_cast<uintptr_t>(&KERNEL_VIRT_OFFSET),
    reinterpret_cast<uintptr_t>(&KERNEL_START),
    reinterpret_cast<uintptr_t>(&KERNEL_END),
    reinterpret_cast<uintptr_t>(&KERNEL_END) - reinterpret_cast<uintptr_t>(&KERNEL_START),
    reinterpret_cast<uintptr_t>(&KERNEL_CODE),
    reinterpret_cast<uintptr_t>(&KERNEL_RODATA),
    reinterpret_cast<uintptr_t>(&KERNEL_RODATA),
    reinterpret_cast<uintptr_t>(&KERNEL_DATA),
    reinterpret_cast<uintptr_t>(&KERNEL_DATA),
    reinterpret_cast<uintptr_t>(&KERNEL_BSS),
    reinterpret_cast<uintptr_t>(&KERNEL_BSS),
    reinterpret_cast<uintptr_t>(&KERNEL_END)};

// TODO: check if COM port is correct?
SerialPort Kernel::com1(SerialPort::COM_1);
Cmos Kernel::cmos;
// The fixed address is a temporary band-aid until we can query the BIOS for the
// real location
CgaScreen Kernel::cga(0xB8000 + structure.virtualAddrOffset);
KernelConsole Kernel::console;
MemoryManager Kernel::memManager;
Heap *Kernel::heap = nullptr;
GlobalDescriptorTable *Kernel::gdt = nullptr;
InterruptDescriptorTable *Kernel::idt = nullptr;
IsrPlugbox *Kernel::isrPlugbox        = nullptr;
InterruptController *Kernel::pic      = nullptr;
Ps2Controller *Kernel::ps2            = nullptr;
Ps2Keyboard *Kernel::keyboard         = nullptr;

void Kernel::run(const multiboot_info_t *info) {
    // TODO: theoretically, the size of the heap object could be larger than a page
    uintptr_t heapAddr =
        Kernel::memManager.init(reinterpret_cast<multiboot_memory_map_t *>(info->mmap_addr),
                                info->mmap_length, Kernel::structure);
    Kernel::memManager.allocPage(heapAddr, true, false);
    Kernel::heap = new (reinterpret_cast<void *>(heapAddr)) Heap(heapAddr, 10, true);

    // Initialize the IDT
    Kernel::gdt = new GlobalDescriptorTable(4);
    // Kernel code segment
    Kernel::gdt->addEntry(GlobalDescriptorTable::Entry(
        0, 0xFFFFFFFF, GlobalDescriptorTable::Privilege::LEVEL_0, true, false, true, true, true));
    // Kernel data segment
    Kernel::gdt->addEntry(GlobalDescriptorTable::Entry(
        0, 0xFFFFFFFF, GlobalDescriptorTable::Privilege::LEVEL_0, true, false, false, true, true));
    // User code segment
    Kernel::gdt->addEntry(GlobalDescriptorTable::Entry(
        0, 0xFFFFFFFF, GlobalDescriptorTable::Privilege::LEVEL_3, true, false, true, true, true));
    // User data segment
    Kernel::gdt->addEntry(GlobalDescriptorTable::Entry(
        0, 0xFFFFFFFF, GlobalDescriptorTable::Privilege::LEVEL_3, true, false, false, true, true));
    Kernel::gdt->reload();

    // Initialize the IDT
    Kernel::idt = new InterruptDescriptorTable();
    Kernel::idt->reload();

    // Initialize the ISR plugbox
    Kernel::isrPlugbox = new IsrPlugbox();

    // Initialize the PIC and enable interrupts
    Kernel::pic = new InterruptController();
    Kernel::pic->enable();
    cpu::enableInterrupts();

    // Test the IRQs by enabling the timer
    IntervalTimer timer;
    timer.enable();
    RealTimeClock rtc;
    rtc.setFrequency(RealTimeClock::Frequency::HERTZ_4);
    rtc.enable();

    // Ready the PS2 controller
    Kernel::ps2      = new Ps2Controller();
    Kernel::keyboard = new Ps2Keyboard();
    // Kernel::keyboard->enable();

    Kernel::console.log(KernelConsole::LogLevel::WARNING, "Kernel start/size:\t\t{} / {} KB",
                        reinterpret_cast<void *>(Kernel::structure.kernelStart),
                        (Kernel::structure.kernelEnd - Kernel::structure.kernelStart) / 1024);
    Kernel::console.log(
        KernelConsole::LogLevel::WARNING, "Memory available/total:\t\t{} KB / {} KB",
        (Kernel::memManager.getAvailablePageFrames() * MemoryManager::PAGE_SIZE) / 1024,
        (Kernel::memManager.getTotalPageFrames() * MemoryManager::PAGE_SIZE) / 1024);
    Kernel::console.log(KernelConsole::LogLevel::WARNING, "Highest available page address:\t{}",
                        reinterpret_cast<void *>(Kernel::memManager.getHighestPageAddress()));

    // Keep the machine busy
    // Optionally, let it shut down; nothing else can currently happen, anyway
    // (read: no interrupts/user tasks)
    while (true) {
        //Kernel::console.log(KernelConsole::LogLevel::STATUS, "Time: {}",
        //                    RealTimeClock::formatDateTime(rtc.getDateTime()).data());
    }
}

extern "C" void kernelMain(uint32_t magic, const multiboot_info_t *info) {
    if (magic != MULTIBOOT_BOOTLOADER_MAGIC) {
        Kernel::panic("Error: The kernel was not loaded by a multiboot loader!");
    }

    Kernel::run(info);
}
