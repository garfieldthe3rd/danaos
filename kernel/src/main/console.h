/**
 * main/console.h
**/

#ifndef DANAOS_MAIN_CONSOLE_H_
#define DANAOS_MAIN_CONSOLE_H_

#include "machine/devices/cga.h"

class KernelConsole {
public:
    enum class Base : uint8_t { BIN = 2, DEC = 10, HEX = 16 };

    enum class LogLevel : uint8_t {
        STATUS      = 0,
        LOG         = 1,
        WARNING     = 2,
        ERROR       = 3,
        LEVEL_COUNT = ERROR + 1
    };

private:
    // TODO: necessary?
    friend class Kernel;

    KernelConsole();
    KernelConsole(const KernelConsole &) = delete;

    void put(char c);
    void putCom(char c);
    void putCga(char c);

    void print(const char *msg);
    void print(bool val);
    void print(char val);
    void print(unsigned char val);
    void print(short val);
    void print(unsigned short val);
    void print(int val);
    void print(unsigned int val);
    void print(long val);
    void print(unsigned long val);
    void print(void *ptr);

    static const CgaScreen::CharAttribute ATTRIBUTES[static_cast<uint8_t>(LogLevel::LEVEL_COUNT)];
    static constexpr LogLevel DEFAULT_LEVEL = LogLevel::LOG;

    LogLevel currentLevel;
    uint8_t screenX, screenY;
    Base base;

public:
    void setBase(Base base);

    void log(const char *msg);
    void log(LogLevel level, const char *msg);

    template <class T, class... Args>
    void log(const char *msg, T val, Args... args) {
        // Perfect forwarding manually, since we don't have std::forward (yet)
        log(KernelConsole::DEFAULT_LEVEL, msg, val, static_cast<Args &&>(args)...);
    }

    template <class T, class... Args>
    void log(LogLevel level, const char *msg, T val, Args... args) {
        // TODO: this seems pretty bad...
        // TODO: especially for multi-threading!
        this->currentLevel = level;

        while (*msg != '\0') {
            if (*msg == '{') {
                if (*(msg + 1) == '}') {
                    print(val);
                    log(level, msg + 2, args...);
                    return;
                }
            } else if (*msg == '[') {
                if (*(msg + 1) == ']') {
                    Base temp = base;
                    base = Base::HEX;
                    print(val);
                    base = temp;
                    log(level, msg + 2, args...);
                    return;
                }
            }

            put(*(msg++));
        }

        put('\n');
    }
};

#endif // DANAOS_MAIN_CONSOLE_H_