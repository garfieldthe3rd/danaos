#include "main/console.h"
#include "libk/math.h"
#include "main/kernel.h"

const CgaScreen::CharAttribute KernelConsole::ATTRIBUTES[] = {
    CgaScreen::CharAttribute(CgaScreen::FgColors::WHITE, CgaScreen::BgColors::BLACK, false),
    CgaScreen::CharAttribute(CgaScreen::FgColors::WHITE, CgaScreen::BgColors::BLACK, false),
    CgaScreen::CharAttribute(CgaScreen::FgColors::YELLOW, CgaScreen::BgColors::BLACK, false),
    CgaScreen::CharAttribute(CgaScreen::FgColors::RED, CgaScreen::BgColors::BLACK, false)};

KernelConsole::KernelConsole()
    : currentLevel(LogLevel::STATUS), screenX(0), screenY(0), base(Base::DEC) {
}

void KernelConsole::put(char c) {
    // Everything will be logged to COM
    this->putCom(c);

    // NOT everything (namely logging output) will be put on screen
    if (currentLevel != LogLevel::LOG) {
        this->putCga(c);
    }
}

void KernelConsole::putCom(char c) {
    switch (c) {
        case '\t':
            Kernel::com1.send('\t');
            break;
        case '\n':
            Kernel::com1.send('\n');
            Kernel::com1.send('\r');
            break;
        default:
            Kernel::com1.send(c);
    }
}

void KernelConsole::putCga(char c) {
    // Determine the attribute to be used based on the
    // currently set logging level
    const CgaScreen::CharAttribute attrib = ATTRIBUTES[static_cast<uint8_t>(currentLevel)];

    switch (c) {
        case '\t':
            screenX = ((screenX / 8) + 1) * 8;
            break;
        case '\n':
            screenX = 0;
            screenY++;
            break;
        default:
            Kernel::cga.print(screenX++, screenY, c, attrib);
    }

    while (screenX >= 80) {
        screenX -= 80;
        screenY++;
    }
    while (screenY >= 25) {
        Kernel::cga.scrollDown();
        screenY--;
    }
}

void KernelConsole::setBase(Base base) {
    this->base = base;
}

void KernelConsole::log(const char *msg) {
    log(KernelConsole::DEFAULT_LEVEL, msg);
}

void KernelConsole::log(LogLevel level, const char *msg) {
    this->currentLevel = level;

    print(msg);
    put('\n');
}

void KernelConsole::print(const char *msg) {
    while (*msg != '\0') {
        put(*(msg++));
    }
}

void KernelConsole::print(bool val) {
    if (val) {
        print("true");
    } else {
        print("false");
    }
}

void KernelConsole::print(char val) {
    put(val);
}

void KernelConsole::print(unsigned char val) {
    print(static_cast<unsigned long>(val));
}

void KernelConsole::print(short val) {
    print(static_cast<long>(val));
}

void KernelConsole::print(unsigned short val) {
    print(static_cast<unsigned long>(val));
}

void KernelConsole::print(int val) {
    print(static_cast<long>(val));
}

void KernelConsole::print(unsigned int val) {
    print(static_cast<unsigned long>(val));
}

void KernelConsole::print(long val) {
    if (val < 0) {
        put('-');
        val *= -1;
    }
    print(static_cast<unsigned long>(val));
}

void KernelConsole::print(unsigned long val) {
    unsigned long currFig, currDigit;

    switch (base) {
        case Base::BIN:
            print("0b");
            break;
        case Base::HEX:
            print("0x");
            break;
        default:
            break;
    }

    for (long i = math::logl(static_cast<uint8_t>(base), val); i >= 0; i--) {
        currFig   = math::powl(static_cast<uint8_t>(base), i);
        currDigit = val / currFig;

        // If currDigit > 9, continue with capital letters (only for hex base)
        put((char) (currDigit + 48 + (currDigit > 9 ? 7 : 0)));
        val -= currDigit * currFig;
    }
}

void KernelConsole::print(void *ptr) {
    Base oldBase = base;
    setBase(Base::HEX);
    print(reinterpret_cast<uintptr_t>(ptr));
    setBase(oldBase);
}