/**
 * machine/locks/spinlock.cc
**/

#include "machine/locks/spinlock.h"
#include "machine/devices/cpu.h"

extern void _aquireSpinlock(int *lock) __asm__("_aquireSpinlock");
extern void _releaseSpinlock(int *lock) __asm__("_releaseSpinlock");

SpinLock::SpinLock(bool locked) : locked(false), intState() {
    if (locked) {
        this->lock();
    }
}

SpinLock::~SpinLock() {
    // TODO: is it really smart to risk a possible deadlock?
    this->unlock();
}

void SpinLock::lock() {
    // TODO: store interrupt state!
    intState = cpu::disableInterrupts();
    _aquireSpinlock(&this->locked);
}

void SpinLock::unlock() {
    _releaseSpinlock(&this->locked);
    if (intState) {
        cpu::enableInterrupts();
        intState = true;
    }
}