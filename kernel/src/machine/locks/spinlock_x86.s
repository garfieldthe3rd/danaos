/**
 * machine/locks/spinlock_x86.s
 *
 * Implements the 'aquire' and 'release' methods for spinlocks.
**/

.global _aquireSpinlock, _releaseSpinlock

// Check whether the lock is lockable; if not, busy-wait until it is
_aquireSpinlock:
	push		%ebp
	movl		%esp, %ebp
	movl		8(%ebp), %eax
.aquire:
	lock btsl	$0, (%eax)
	jc			.spin
	
	pop			%ebp
	ret

.spin:
	pause
	testl		$1, (%eax)
	jnz			.spin
	jmp			.aquire

// Release a (locked or not) spinlock
_releaseSpinlock:
	push		%ebp
	movl		%esp, %ebp
	movl		8(%ebp), %eax
	
	movl		$0, (%eax)
	
	pop			%ebp
	ret
