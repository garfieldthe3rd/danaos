/**
 * machine/locks/spinlock.cc
**/

#ifndef DANAOS_MACHINE_LOCKS_SPINLOCK_H_
#define DANAOS_MACHINE_LOCKS_SPINLOCK_H_

class SpinLock {
private:
    int locked;
    bool intState;

public:
    SpinLock(bool locked       = false);
    SpinLock(const SpinLock &) = delete;
    SpinLock(SpinLock &&lock)  = delete;
    ~SpinLock();

    SpinLock &operator=(const SpinLock &) = delete;
    SpinLock &operator=(SpinLock &&) = delete;

    virtual void lock();
    virtual void unlock();
};

#endif // DANAOS_MACHINE_LOCKS_SPINLOCK_H_