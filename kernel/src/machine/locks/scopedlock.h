/**
 * machine/locks/scopedlock.h
 *
 * Lock built on top of a regular lock which is bound by scope.
 * The lock requires that its 'host' lock will not be destroyed within its
 *lifespan!
**/

#ifndef DANAOS_MACHINE_LOCKS_SCOPEDLOCK_H_
#define DANAOS_MACHINE_LOCKS_SCOPEDLOCK_H_

template <class T>
class ScopedLock {
  private:
    T &lock;

  public:
    ScopedLock(T &lock) : lock(lock) {
        this->lock.lock();
    }

    ~ScopedLock() {
        this->lock.unlock();
    }
};

#endif // DANAOS_MACHINE_LOCKS_SCOPEDLOCK_H_