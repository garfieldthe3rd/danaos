/**
 * machine/devices/cpu.h
**/

#ifndef DANAOS_MACHINE_DEVICES_CPU_H
#define DANAOS_MACHINE_DEVICES_CPU_H

namespace cpu {

// TODO: this won't work for multiple CPU's/cores/w.e
bool enableInterrupts();
bool disableInterrupts();

void halt();

} // namespace cpu

#endif // DANAOS_MACHINE_DEVICES_CPU_H