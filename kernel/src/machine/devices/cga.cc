/**
 * machine/devices/cga.cc
**/

#include "machine/devices/cga.h"
#include "machine/locks/scopedlock.h"
#include "main/kernel.h"

CgaScreen::CharAttribute::CharAttribute()
    : fgColor(FgColors::DEFAULT), bgColor(BgColors::DEFAULT), blink() {
}

CgaScreen::CharAttribute::CharAttribute(uint8_t raw)
    : fgColor(static_cast<FgColors>((raw >> 0) & 0x0F)),
      bgColor(static_cast<BgColors>((raw >> 4) & 0x07)),
      blink((raw >> 7) & 0x01) {
}

CgaScreen::CharAttribute::CharAttribute(FgColors fgColor)
    : fgColor(fgColor), bgColor(BgColors::DEFAULT), blink() {
}

CgaScreen::CharAttribute::CharAttribute(BgColors bgColor)
    : fgColor(FgColors::DEFAULT), bgColor(bgColor), blink() {
}

CgaScreen::CharAttribute::CharAttribute(FgColors fgColor, BgColors bgColor)
    : fgColor(fgColor), bgColor(bgColor), blink() {
}

CgaScreen::CharAttribute::CharAttribute(BgColors bgColor, bool blink)
    : fgColor(FgColors::DEFAULT), bgColor(bgColor), blink(blink) {
}

CgaScreen::CharAttribute::CharAttribute(FgColors fgColor, bool blink)
    : fgColor(fgColor), bgColor(BgColors::DEFAULT), blink(blink) {
}

CgaScreen::CharAttribute::CharAttribute(FgColors fgColor, BgColors bgColor, bool blink)
    : fgColor(fgColor), bgColor(bgColor), blink(blink) {
}

CgaScreen::ScreenBlock::ScreenBlock() : character(' '), attributes() {
}

CgaScreen::ScreenBlock::ScreenBlock(uint16_t raw)
    : character(raw & 0xFF), attributes((raw >> 8) & 0xFF) {
}

CgaScreen::ScreenBlock::ScreenBlock(char character, CharAttribute attributes)
    : character(character), attributes(attributes) {
}

CgaScreen::ScreenBlock::ScreenBlock(char character, FgColors fgColor, BgColors bgColor, bool blink)
    : character(character), attributes(fgColor, bgColor, blink) {
}

CgaScreen::CgaScreen(uintptr_t memAddr)
    : MEM_ADDRESS(memAddr),
      MIN_X(0),
      MAX_X(79),
      MIN_Y(0),
      MAX_Y(24),
      screen(reinterpret_cast<ScreenBlock *>(memAddr)),
      screenLock() {
    this->clear();
    // TODO: screen boundaries, mark memory area as unavailable etc...
}

void CgaScreen::setBlock(size_t x, size_t y, ScreenBlock block) {
    ScopedLock<SpinLock> lock(screenLock);
    screen[x + (MAX_X - MIN_X + 1) * y] = block;
}

CgaScreen::ScreenBlock CgaScreen::getBlock(size_t x, size_t y) {
    ScopedLock<SpinLock> lock(screenLock);
    return screen[x + (MAX_X - MIN_X + 1) * y];
}

void CgaScreen::print(size_t x, size_t y, char c, CharAttribute attributes) {
    // TODO: screen boundaries!
    // TODO: different attributes
    this->setBlock(x, y, ScreenBlock(c, attributes));
}

void CgaScreen::clear() {
    // TODO: screen boundaries!
    for (size_t x = MIN_X; x <= MAX_X; x++) {
        for (size_t y = MIN_Y; y <= MAX_Y; y++) {
            // TODO: is the function call inlined?
            print(x, y, ' ');
        }
    }
}

void CgaScreen::scrollDown() {
    for (size_t y = MIN_Y; y < MAX_Y; y++) {
        for (size_t x = MIN_X; x <= MAX_X; x++) {
            this->setBlock(x, y, this->getBlock(x, y + 1));
        }
    }

    for (size_t x = MIN_X; x <= MAX_X; x++) {
        print(x, MAX_Y, ' ');
    }
}

size_t CgaScreen::size() const {
    return (MAX_X - MIN_X + 1) * (MAX_Y - MIN_Y + 1) * sizeof(ScreenBlock);
}