/**
 * machine/devices/timer/pit.cc
**/

#include "pit.h"
#include "libk/math.h"
#include "libk/string.h"
#include "machine/devices/cpu.h"
#include "main/kernel.h"

IntervalTimer::IntervalTimer()
    : control(BASE_ADDRESS + 3),
      channel0(BASE_ADDRESS),
      channel1(BASE_ADDRESS + 1),
      channel2(BASE_ADDRESS + 2),
      currentDivisor(DIVISOR_MILLISECOND),
      currentTicks(0) {
    setFrequencyDivisor(currentDivisor);
}

IntervalTimer::~IntervalTimer() {
    this->disable();
}

void IntervalTimer::enable() {
    Kernel::pic->enable(InterruptController::Irq::PIT, this);
}

void IntervalTimer::disable() {
    Kernel::pic->disable(InterruptController::Irq::PIT);
}

void IntervalTimer::trigger(InterruptState *state) {
    // TODO: feels too slow (caused by emulator)
    currentTicks++;

    uint32_t milliseconds = currentTicks; // * 1000 / (BASE_FREQUENCY / currentDivisor);
    std::string time      = std::string("PIT: ") + std::to_string(milliseconds) + "ms";

    for (size_t i = 0; i < time.length(); i++) {
        Kernel::cga.print(80 - time.length() + i, 1, time[i]);
    }
}

uint16_t IntervalTimer::getFrequencyDivisor() const {
    return currentDivisor;
}

void IntervalTimer::setFrequencyDivisor(uint16_t divisor) {
    divisor = math::max(math::min(divisor, MAX_DIVISOR), MIN_DIVISOR);

    cpu::disableInterrupts();
    control.write(Command{0, OperatingMode::MODE_2, AccessMode::LOW_HIGH, Channel::CHANNEL_0});
    channel0.write(static_cast<uint8_t>(divisor & 0xFF));
    channel0.write(static_cast<uint8_t>((divisor >> 8) & 0xFF));

    // TODO: save the current interrupt state instead of blindly
    // enabling them!
    cpu::enableInterrupts();
}

IntervalTimer::Command::operator uint8_t() const {
    return bcd | (static_cast<uint8_t>(opMode) << 1) | (static_cast<uint8_t>(accessMode) << 4) |
           (static_cast<uint8_t>(channel) << 6);
}