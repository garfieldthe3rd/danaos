/**
 * machine/devices/timer/rtc.cc
**/

#include "rtc.h"
#include "main/kernel.h"

constexpr const char* RealTimeClock::WEEKDAYS[];
constexpr const char* RealTimeClock::MONTHS[];

RealTimeClock::RealTimeClock() : currentTicks(0), currentFrequency(Frequency::HERTZ_1024) {
    setFrequency(Frequency::HERTZ_1024);
}

RealTimeClock::~RealTimeClock() {
    this->disable();
}

void RealTimeClock::enable() {
    // Plug it into the PIC
    Kernel::pic->enable(InterruptController::Irq::RTC, this);

    // Tell the RTC to trigger periodic interrupts with the set frequency
    auto srb              = Kernel::cmos.read<StatusRegisterB>(Cmos::Register::STATUS_B);
    srb.periodicInterrupt = 1;
    srb.updateInterrupt   = 1;
    Kernel::cmos.write(Cmos::Register::STATUS_B, srb);
    srb = Kernel::cmos.read<StatusRegisterB>(Cmos::Register::STATUS_B);
}

void RealTimeClock::disable() {
    // Disable the IRQ in the PIC
    Kernel::pic->disable(InterruptController::Irq::RTC);

    // Tell the RTC to stop triggering periodic interrupts
    auto srb              = Kernel::cmos.read<StatusRegisterB>(Cmos::Register::STATUS_B);
    srb.periodicInterrupt = 0;
    Kernel::cmos.write(Cmos::Register::STATUS_B, srb);
}

void RealTimeClock::trigger(InterruptState *state) {
    // TODO: seems too slow!

    // Distinguish the interrupt source
    // Also needed to get more interrupts (reading SR C, that is)
    auto src = Kernel::cmos.read<StatusRegisterC>(Cmos::Register::STATUS_C);

    if (src.irqIsUpdate) {
        std::string datetime = formatDateTime(getDateTime()).data();
        // Print it to the very right
        for (size_t i = 0; i < datetime.length(); i++) {
            Kernel::cga.print(80 - datetime.length() + i, 24, datetime[i]);
        }
    }
    if (src.irqIsPeriodic) {
        currentTicks++;
        uint32_t milliseconds = (1000 * currentTicks) / getHertz(currentFrequency);
        std::string time      = std::string("RTC: ") + std::to_string(milliseconds) + "ms";

        for (size_t i = 0; i < time.length(); i++) {
            Kernel::cga.print(80 - time.length() + i, 0, time[i]);
        }
    }
}

void RealTimeClock::setFrequency(Frequency freq) {
    // Write the frequency into the status register A
    auto sba      = Kernel::cmos.read<StatusRegisterA>(Cmos::Register::STATUS_A);
    sba.frequency = freq;
    Kernel::cmos.write(Cmos::Register::STATUS_A, sba);
    this->currentFrequency = freq;
}

RealTimeClock::Datetime RealTimeClock::getDateTime() {
    Datetime time{
        bcdToBinary(Kernel::cmos.read<uint8_t>(Cmos::Register::SECOND)),
        bcdToBinary(Kernel::cmos.read<uint8_t>(Cmos::Register::MINUTE)),
        bcdToBinary(Kernel::cmos.read<uint8_t>(Cmos::Register::HOUR)),
        static_cast<Weekday>(bcdToBinary(Kernel::cmos.read<uint8_t>(Cmos::Register::WEEKDAY))),
        bcdToBinary(Kernel::cmos.read<uint8_t>(Cmos::Register::MONTHDAY)),
        static_cast<Month>(bcdToBinary(Kernel::cmos.read<uint8_t>(Cmos::Register::MONTH))),
        bcdToBinary(Kernel::cmos.read<uint8_t>(Cmos::Register::YEAR)) +
            100u * bcdToBinary(Kernel::cmos.read<uint8_t>(Cmos::Register::CENTURY))};

    // Check for summer time
    auto srb = Kernel::cmos.read<StatusRegisterB>(Cmos::Register::STATUS_B);
    if (srb.summerTime) {
        // Check if we passed a day and adopt the other fields accordingly
        if (time.hour == 0) {
            time.hour = 23;
            if (time.monthday == 0) {
                // TODO: check for going over into new month (also requires leap year!)
            } else {
                time.monthday--;
            }
            if (time.weekday == Weekday::MONDAY) {
                time.weekday = Weekday::SUNDAY;
            } else {
                time.weekday = static_cast<Weekday>(static_cast<uint8_t>(time.weekday) - 1);
            }

        } else {
            // time.hour--;
        }
    }

    return time;
}

std::string RealTimeClock::formatDateTime(Datetime time) {
    std::string str;

    // Reserve the necessary space
    str.reserve(29);

    // RFC 1123 datetime format: Thu, 02 Mar 2017 01:49:35 GMT
    str.append(WEEKDAYS[static_cast<uint8_t>(time.weekday)], 3);
    str += ", ";

    if (time.monthday < 10) {
        str += "0";
    }
    str += std::to_string(time.monthday);
    str += " ";

    str.append(MONTHS[static_cast<uint8_t>(time.month)], 3);
    str += " ";

    str += std::to_string(time.year);
    str += " ";

    if (time.hour < 10) {
        str += "0";
    }
    str += std::to_string(time.hour);
    str += ":";

    if (time.minute < 10) {
        str += "0";
    }
    str += std::to_string(time.minute);
    str += ":";

    if (time.second < 10) {
        str += "0";
    }
    str += std::to_string(time.second);

    str += " UTC";

    return str;
}

/**
 * Methods for the status registers
**/

RealTimeClock::StatusRegisterA::StatusRegisterA(uint8_t data)
    : frequency(static_cast<Frequency>(data & 0x0F)),
      basis((data >> 4) & 0x07),
      updating((data >> 7) & 0x01) {
}

RealTimeClock::StatusRegisterA::StatusRegisterA(Frequency frequency)
    : frequency(frequency), basis(2), updating(0) {
}

RealTimeClock::StatusRegisterA &RealTimeClock::StatusRegisterA::
operator=(const StatusRegisterA &reg) {
    this->frequency = reg.frequency;
    this->updating  = reg.updating;

    return *this;
}

RealTimeClock::StatusRegisterA::operator uint8_t() const {
    return static_cast<uint8_t>(frequency) | (basis << 4) | (updating << 7);
}

RealTimeClock::StatusRegisterB::StatusRegisterB(uint8_t data)
    : summerTime(data & 0x01),
      format24Hours((data >> 1) & 0x01),
      binaryData((data >> 2) & 0x01),
      rectFrequency((data >> 3) & 0x01),
      updateInterrupt((data >> 4) & 0x01),
      alarmInterrupt((data >> 5) & 0x01),
      periodicInterrupt((data >> 6) & 0x01),
      updateTime((data >> 7) & 0x01) {
}

RealTimeClock::StatusRegisterB::operator uint8_t() const {
    return summerTime | (format24Hours << 1) | (binaryData << 2) | (rectFrequency << 3) |
           (updateInterrupt << 4) | (alarmInterrupt << 5) | (periodicInterrupt << 6) |
           (updateTime << 7);
}

RealTimeClock::StatusRegisterC::StatusRegisterC(bool useUpdate, bool useAlarm, bool usePeriodic)
    : reserved(0),
      irqIsUpdate(useUpdate),
      irqIsAlarm(useAlarm),
      irqIsPeriodic(usePeriodic),
      interruptRequested(0) {
}

RealTimeClock::StatusRegisterC::StatusRegisterC(uint8_t data)
    : reserved(0),
      irqIsUpdate((data >> 4) & 0x01),
      irqIsAlarm((data >> 5) & 0x01),
      irqIsPeriodic((data >> 6) & 0x01),
      interruptRequested((data >> 7) & 0x01) {
}

RealTimeClock::StatusRegisterC &RealTimeClock::StatusRegisterC::
operator=(const StatusRegisterC &reg) {
    this->irqIsUpdate        = reg.irqIsUpdate;
    this->irqIsAlarm         = reg.irqIsAlarm;
    this->irqIsPeriodic      = reg.irqIsPeriodic;
    this->interruptRequested = reg.interruptRequested;

    return *this;
}

RealTimeClock::StatusRegisterC::operator uint8_t() const {
    return reserved | (irqIsUpdate << 4) | (irqIsAlarm << 5) | (irqIsPeriodic << 6) |
           (interruptRequested << 7);
}

RealTimeClock::StatusRegisterD::StatusRegisterD(bool dataValid)
    : reserved(0), dataValid(dataValid) {
}

RealTimeClock::StatusRegisterD &RealTimeClock::StatusRegisterD::
operator=(const StatusRegisterD &reg) {
    this->dataValid = reg.dataValid;

    return *this;
}

RealTimeClock::StatusRegisterD::operator uint8_t() const {
    return reserved | (dataValid << 7);
}
