/**
 * machine/devices/timer/cmos.h
**/

#ifndef DANAOS_MACHINE_DEVICES_TIMER_CMOS_H_
#define DANAOS_MACHINE_DEVICES_TIMER_CMOS_H_

#include "machine/devices/cpu.h"
#include "machine/io/port.h"
#include <stdint.h>

// TODO: add the rest of the registers
// TODO: current solution sucks; more typesafety!
class Cmos {
private:
    static constexpr uint16_t SELECTOR_PORT = 0x70;
    static constexpr uint16_t DATA_PORT     = 0x71;
    static constexpr uint8_t NMI_BIT        = 7;

    // TODO: factor in NMI
    IoPort<uint8_t> selector;
    IoPort<uint8_t> data;

public:
    // TODO: the century register is not necessarily present or
    // at a different port; use ACPI to check for it!
    enum class Register : uint8_t {
        SECOND   = 0,
        MINUTE   = 2,
        HOUR     = 4,
        WEEKDAY  = 6,
        MONTHDAY = 7,
        MONTH    = 8,
        YEAR     = 9,
        CENTURY  = 50,
        STATUS_A = 10,
        STATUS_B = 11,
        STATUS_C = 12,
        STATUS_D = 13
    };

    template <class T>
    void write(Register reg, T val) {
        // Disable interrupts
        cpu::disableInterrupts();

        // Our selection handedly disables NMIs
        selector.write(static_cast<uint8_t>(reg) | (1 << NMI_BIT));
        data.write((uint8_t) val);

        // TODO: restore previous state instead!
        cpu::enableInterrupts();
        // selector.write(selector.read() & ~(1 << NMI_BIT));
        // TODO: Meaningless read mandated by hardware spec
        // data.read();
    }

    template <class T>
    T read(Register reg) {
        // Disable interrupts
        cpu::disableInterrupts();

        // Our selection handedly disables NMIs
        selector.write(static_cast<uint8_t>(reg) | (1 << NMI_BIT));
        uint8_t val = data.read();

        // TODO: restore previous state instead!
        cpu::enableInterrupts();
        selector.write(selector.read() & ~(1 << NMI_BIT));
        // TODO: Meaningless read mandated by hardware spec
        // data.read();
        return T(val);
    }

    Cmos();
    // TODO: what would there need to be destructed?
};

#endif // DANAOS_MACHINE_DEVICES_TIMER_CMOS_H_