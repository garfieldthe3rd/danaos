/**
 * machine/devices/timer/rtc.h
**/

#ifndef DANAOS_MACHINE_DEVICES_TIMER_RTC_H_
#define DANAOS_MACHINE_DEVICES_TIMER_RTC_H_

#include "libk/string.h"
#include "machine/devices/timer/cmos.h"
#include "machine/interrupts/isr.h"
#include "machine/io/port.h"

// TODO: const-correctness
// TODO: this is somehow linked with the CMOS
class RealTimeClock : public InterruptGate {
public:
    enum class Frequency : uint8_t {
        HERTZ_2    = 15,
        HERTZ_4    = 14,
        HERTZ_8    = 13,
        HERTZ_16   = 12,
        HERTZ_32   = 11,
        HERTZ_64   = 10,
        HERTZ_128  = 9,
        HERTZ_256  = 8,
        HERTZ_512  = 7,
        HERTZ_1024 = 6,
        HERTZ_2048 = 5,
        HERTZ_4096 = 4,
        HERTZ_8192 = 3
    };

private:
    struct StatusRegisterA {
        Frequency frequency : 4;
        const uint8_t basis : 3;
        uint8_t updating : 1;

        StatusRegisterA(uint8_t data);
        StatusRegisterA(Frequency frequency);
        StatusRegisterA &operator=(const StatusRegisterA &);
        operator uint8_t() const;
    } __attribute__((packed));

    struct StatusRegisterB {
        uint8_t summerTime : 1;
        uint8_t format24Hours : 1;
        uint8_t binaryData : 1;
        uint8_t rectFrequency : 1;
        uint8_t updateInterrupt : 1;
        uint8_t alarmInterrupt : 1;
        uint8_t periodicInterrupt : 1;
        uint8_t updateTime : 1;

        StatusRegisterB(uint8_t data);
        operator uint8_t() const;
    } __attribute__((packed));

    struct StatusRegisterC {
        const uint8_t reserved : 4;
        uint8_t irqIsUpdate : 1;
        uint8_t irqIsAlarm : 1;
        uint8_t irqIsPeriodic : 1;
        uint8_t interruptRequested : 1;

        StatusRegisterC(uint8_t data);
        StatusRegisterC(bool useUpdate, bool useAlarm, bool usePeriodic);
        StatusRegisterC &operator=(const StatusRegisterC &);
        operator uint8_t() const;
    } __attribute__((packed));

    struct StatusRegisterD {
        const uint8_t reserved : 7;
        uint8_t dataValid : 1;

        StatusRegisterD(bool dataValid);
        StatusRegisterD &operator=(const StatusRegisterD &);
        operator uint8_t() const;
    } __attribute__((packed));

    static constexpr uint8_t bcdToBinary(uint8_t bcd) {
        return (bcd & 0x0F) + 10 * ((bcd >> 4) & 0x0F);
    }

    static constexpr const char* WEEKDAYS[7] = {
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
        "Sunday"
    };

    static constexpr const char* MONTHS[12] = {
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
    };

    static constexpr uint32_t getHertz(Frequency freq) {
        return 1 << (16 - static_cast<uint32_t>(freq));
    }

    uint32_t currentTicks;
    Frequency currentFrequency;

public:
    enum class Weekday : uint8_t {
        MONDAY    = 0,
        TUESDAY   = 1,
        WEDNESDAY = 2,
        THURSDAY  = 3,
        FRIDAY    = 4,
        SATURDAY  = 5,
        SUNDAY    = 6
    };

    enum class Month : uint8_t {
        JANUARY   = 0,
        FEBRUARY  = 1,
        MARCH     = 2,
        APRIL     = 3,
        MAY       = 4,
        JUNE      = 5,
        JULY      = 6,
        AUGUST    = 7,
        SEPTEMBER = 8,
        OCTOBER   = 9,
        NOVEMBER  = 10,
        DECEMBER  = 11
    };

    struct Datetime {
        unsigned char second;
        unsigned char minute;
        unsigned char hour;
        Weekday weekday;
        unsigned char monthday;
        Month month;
        unsigned int year;
    };

    RealTimeClock();
    ~RealTimeClock();

    virtual void enable();
    virtual void disable();
    virtual void trigger(InterruptState *state);

    void setFrequency(Frequency freq);

    Datetime getDateTime();
    static std::string formatDateTime(Datetime time);
};

#endif // DANAOS_MACHINE_DEVICES_TIMER_RTC_H_