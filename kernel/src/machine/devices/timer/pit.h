/**
 * machine/device/timer/pit.h
**/

#ifndef DANAOS_MACHINE_DEVICE_TIMER_PIT_H_
#define DANAOS_MACHINE_DEVICE_TIMER_PIT_H_

#include "machine/interrupts/isr.h"
#include "machine/io/port.h"

// TODO: const-correctness?
class IntervalTimer : public InterruptGate {
private:
    // TODO: proper description?
    enum class OperatingMode : uint8_t {
        MODE_0 = 0,
        MODE_1 = 1,
        MODE_2 = 2,
        MODE_3 = 3,
        MODE_4 = 4,
        MODE_5 = 5
    };

    enum class AccessMode : uint8_t {
        LATCH_COUNT_VALUE = 0,
        LOW_ONLY          = 1,
        HIGH_ONLY         = 2,
        LOW_HIGH          = 3
    };

    enum class Channel : uint8_t {
        CHANNEL_0 = 0,
        CHANNEL_1 = 1,
        CHANNEL_2 = 2
        // READ_BACK_CMD = 3     // TODO: enable this?
    };

    struct Command {
        uint8_t bcd : 1;
        OperatingMode opMode : 3;
        AccessMode accessMode : 2;
        Channel channel : 2;

        operator uint8_t() const;
    } __attribute__((packed));

    static constexpr uint16_t BASE_ADDRESS   = 0x40;
    static constexpr uint32_t BASE_FREQUENCY = 1193182;
    static constexpr uint16_t MIN_DIVISOR    = 2;
    static constexpr uint16_t MAX_DIVISOR    = 65535;

    OutPort<Command> control;
    OutPort<uint8_t> channel0;
    OutPort<uint8_t> channel1;
    OutPort<uint8_t> channel2;
    uint16_t currentDivisor;
    uint32_t currentTicks;

public:
    static constexpr uint16_t DIVISOR_MILLISECOND = static_cast<uint16_t>(BASE_FREQUENCY / 1000);
    static constexpr uint16_t DIVISOR_CENTISECOND = static_cast<uint16_t>(BASE_FREQUENCY / 100);

    IntervalTimer();
    ~IntervalTimer();

    virtual void enable();
    virtual void disable();
    virtual void trigger(InterruptState *state);

    uint16_t getFrequencyDivisor() const;
    void setFrequencyDivisor(uint16_t);
};

#endif // DANAOS_MACHINE_DEVICE_TIMER_PIT_H_