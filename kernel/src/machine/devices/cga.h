/**
 * machine/devices/cga.h
 *
 * Provides a (temporary) interface for the 80x25 'DOS' screen.
**/

#ifndef DANAOS_MACHINE_DEVICES_CGA_H_
#define DANAOS_MACHINE_DEVICES_CGA_H_

#include "machine/locks/spinlock.h"
#include <stddef.h>
#include <stdint.h>

class CgaScreen {
public:
    const uintptr_t MEM_ADDRESS;
    const uint8_t MIN_X;
    const uint8_t MAX_X;
    const uint8_t MIN_Y;
    const uint8_t MAX_Y;

    // Color of the letter/symbol
    enum class FgColors : uint8_t {
        BLACK         = 0,
        BLUE          = 1,
        GREEN         = 2,
        CYAN          = 3,
        RED           = 4,
        MAGENTA       = 5,
        BROWN         = 6,
        LIGHT_GREY    = 7,
        DARK_GREY     = 8,
        LIGHT_BLUE    = 9,
        LIGHT_GREEN   = 10,
        LIGHT_CYAN    = 11,
        LIGHT_RED     = 12,
        LIGHT_MAGENTA = 13,
        YELLOW        = 14,
        WHITE         = 15,
        DEFAULT       = WHITE
    };

    // Color of the screen area behind the letter/symbol
    enum class BgColors : uint8_t {
        BLACK      = 0,
        BLUE       = 1,
        GREEN      = 2,
        CYAN       = 3,
        RED        = 4,
        MAGENTA    = 5,
        BROWN      = 6,
        LIGHT_GREY = 7,
        DEFAULT    = BLACK
    };

    struct CharAttribute {
        // The bitfielded enum's here trigger a warning in gcc; ignore it
        FgColors fgColor : 4;
        BgColors bgColor : 3;
        uint8_t blink : 1;

        CharAttribute();
        CharAttribute(uint8_t raw);
        CharAttribute(FgColors fgColor);
        CharAttribute(BgColors bgColor);
        CharAttribute(FgColors fgColor, BgColors bgColor);
        CharAttribute(FgColors fgColor, bool blink);
        CharAttribute(BgColors bgColor, bool blink);
        CharAttribute(FgColors fgColor, BgColors bgColor, bool blink);
    } __attribute__((packed));

private:
    struct ScreenBlock {
        char character;
        CharAttribute attributes;

        ScreenBlock();
        ScreenBlock(uint16_t raw);
        ScreenBlock(char character, CharAttribute attributes);
        ScreenBlock(char character, FgColors fgColor, BgColors bgColor, bool blink);
    } __attribute__((packed));

    ScreenBlock *screen;
    SpinLock screenLock;

    CgaScreen(uintptr_t memAddr);
    CgaScreen(const CgaScreen &) = delete;
    void init();
    void setBlock(size_t x, size_t y, ScreenBlock block);
    ScreenBlock getBlock(size_t x, size_t y);

    friend class Kernel;

public:
    void print(size_t x, size_t y, char c, CharAttribute attributes = CharAttribute());
    void clear();

    void scrollDown();

    size_t size() const;
};

#endif //DANAOS_MACHINE_DEVICES_CGA_H_
