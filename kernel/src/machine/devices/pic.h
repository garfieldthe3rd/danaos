/**
 * machine/devices/pic.h
**/

#ifndef DANAOS_MACHINE_DEVICES_PIC_H_
#define DANAOS_MACHINE_DEVICES_PIC_H_

#include "machine/interrupts/isr.h"
#include "machine/io/port.h"

// TODO: const-correctness?
class InterruptController : public InterruptGate {
public:
    enum class Irq : uint8_t {
        PIT       = 0,
        PS2_1     = 1,
        SLAVE_IRQ = 2,
        RS232_2   = 3,
        RS232_1   = 4,
        LPT_2     = 5,
        FLOPPY    = 6,
        LPT_1     = 7,
        RTC       = 8,
        ATA_4     = 10,
        ATA_3     = 11,
        PS2_2     = 12,
        FPU       = 13,
        ATA_1     = 14,
        ATA_2     = 15
    };

private:
    enum class InternalRegister : uint8_t { ISR = 2, IRR = 3, NONE = 0 };

    enum class SpecialMaskAction : uint8_t { SET = 2, RESET = 3, NONE = 0 };

    struct InitControlWord1 {
        uint8_t expectCw4 : 1;
        uint8_t singlePic : 1;
        uint8_t reducedCallInterval : 1;
        uint8_t levelTriggered : 1;
        uint8_t initialization : 1;
        uint8_t ivAddress : 3;

        InitControlWord1(bool expectCw4, bool singlePic, bool reducedCallInterval,
                         bool levelTriggered, bool startInitialization, uint8_t ivAddress = 0);
        InitControlWord1 &operator=(const InitControlWord1 &);
        operator uint8_t() const;
    } __attribute__((packed));

    struct InitControlWord2 {
        uint8_t idtOffset;

        InitControlWord2(uint8_t idtOffset);
        operator uint8_t() const;
    } __attribute__((packed));

    struct InitControlWord3 {
        uint8_t slaveIrq;

        InitControlWord3(uint8_t irqNum, bool slave);
        operator uint8_t() const;
    } __attribute__((packed));

    struct InitControlWord4 {
        uint8_t x86Mode : 1;
        uint8_t autoEoi : 1;
        uint8_t selectBufferMaster : 1;
        uint8_t bufferedMode : 1;
        uint8_t fullyNested : 1;
        const uint8_t reserved : 3;

        InitControlWord4(bool x86Mode, bool autoEoi, bool selectBufferMaster, bool bufferedMode,
                         bool fullyNested);
        InitControlWord4 &operator=(const InitControlWord4 &);
        operator uint8_t() const;
    } __attribute__((packed));

    struct OpCommandWord1 {
        uint8_t irq1 : 1;
        uint8_t irq2 : 1;
        uint8_t irq3 : 1;
        uint8_t irq4 : 1;
        uint8_t irq5 : 1;
        uint8_t irq6 : 1;
        uint8_t irq7 : 1;
        uint8_t irq8 : 1;

        operator uint8_t() const;
    } __attribute__((packed));

    struct OpCommandWord2 {
        uint8_t affectedIrq : 3;
        const uint8_t reserved : 2;
        uint8_t eoi : 1;
        uint8_t selection : 1;
        uint8_t rotation : 1;

        OpCommandWord2(uint8_t affectedIrq, bool eoi, bool selection, bool rotation);
        OpCommandWord2 &operator=(const OpCommandWord2 &);
        operator uint8_t() const;
    } __attribute__((packed));

    struct OpCommandWord3 {
        InternalRegister internal : 2;
        uint8_t pollCommand : 1;
        const uint8_t reserved1 : 2;
        SpecialMaskAction mask : 2;
        const uint8_t reserved2 : 1;

        OpCommandWord3(InternalRegister reg, bool issuePoll, SpecialMaskAction maskAction);
        OpCommandWord3 &operator=(const OpCommandWord3 &);
        operator uint8_t() const;
    } __attribute__((packed));

    static constexpr uint16_t MASTER_PORT      = 0x20;
    static constexpr uint16_t SLAVE_PORT       = 0xA0;
    static constexpr uint8_t MASTER_IDT_OFFSET = 32;
    static constexpr uint8_t SLAVE_IDT_OFFSET  = MASTER_IDT_OFFSET + 8;

    // Used for initialization!
    static const InitControlWord1 ICW1_MASTER;
    static const InitControlWord1 ICW1_SLAVE;
    static const InitControlWord2 ICW2_MASTER;
    static const InitControlWord2 ICW2_SLAVE;
    static const InitControlWord3 ICW3_MASTER;
    static const InitControlWord3 ICW3_SLAVE;
    // Will only be used if specified by ICW1
    static const InitControlWord4 ICW4_MASTER;
    static const InitControlWord4 ICW4_SLAVE;

    OutPort<uint8_t> commandMaster;
    OutPort<uint8_t> commandSlave;
    IoPort<uint8_t> dataMaster;
    IoPort<uint8_t> dataSlave;

    InterruptGate *irqGates[16];

    void acknowledge(Irq irq);
    bool isServiced(Irq irq);

public:
    InterruptController();
    ~InterruptController();

    void enable(Irq irq, InterruptGate *gate);
    void disable(Irq irq);
    bool isEnabled(Irq irq);

    virtual void enable();
    virtual void disable();
    virtual void trigger(InterruptState *state);
};

#endif // DANAOS_MACHINE_DEVICES_PIC_H_