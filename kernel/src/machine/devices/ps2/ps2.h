/**
 * machine/devices/ps2/ps2.h
**/

#ifndef DANAOS_MACHINE_DEVICES_PS2_PS2CONTROLLER_H_
#define DANAOS_MACHINE_DEVICES_PS2_PS2CONTROLLER_H_

#include "machine/io/port.h"

// TODO: general model of a PS2 controller!
class Ps2Controller {
private:
    enum class Command1Byte : uint8_t {
        DISABLE_PORT_2   = 0xA7,
        ENABLE_PORT_2    = 0xA8,
        DISABLE_PORT_1   = 0xAD,
        ENABLE_PORT_1    = 0xAE,
        PULSE_LINES_0000 = 0xF0,
        PULSE_LINES_0001 = 0xF1,
        PULSE_LINES_0010 = 0xF2,
        PULSE_LINES_0011 = 0xF3,
        PULSE_LINES_0100 = 0xF4,
        PULSE_LINES_0101 = 0xF5,
        PULSE_LINES_0110 = 0xF6,
        PULSE_LINES_0111 = 0xF7,
        PULSE_LINES_1000 = 0xF8,
        PULSE_LINES_1001 = 0xF9,
        PULSE_LINES_1010 = 0xFA,
        PULSE_LINES_1011 = 0xFB,
        PULSE_LINES_1100 = 0xFC,
        PULSE_LINES_1101 = 0xFD,
        PULSE_LINES_1110 = 0xFE,
        PULSE_LINES_1111 = 0xFF,
    };

    enum class Command1ByteResponse : uint8_t {
        READ_BYTE_0            = 0x20,
        READ_BYTE_1            = 0x21,
        READ_BYTE_2            = 0x22,
        READ_BYTE_3            = 0x23,
        READ_BYTE_4            = 0x24,
        READ_BYTE_5            = 0x25,
        READ_BYTE_6            = 0x26,
        READ_BYTE_7            = 0x27,
        READ_BYTE_8            = 0x28,
        READ_BYTE_9            = 0x29,
        READ_BYTE_10           = 0x2A,
        READ_BYTE_11           = 0x2B,
        READ_BYTE_12           = 0x2C,
        READ_BYTE_13           = 0x2D,
        READ_BYTE_14           = 0x2E,
        READ_BYTE_15           = 0x2F,
        READ_BYTE_16           = 0x30,
        READ_BYTE_17           = 0x31,
        READ_BYTE_18           = 0x32,
        READ_BYTE_19           = 0x33,
        READ_BYTE_20           = 0x34,
        READ_BYTE_21           = 0x35,
        READ_BYTE_22           = 0x36,
        READ_BYTE_23           = 0x37,
        READ_BYTE_24           = 0x38,
        READ_BYTE_25           = 0x39,
        READ_BYTE_26           = 0x3A,
        READ_BYTE_27           = 0x3B,
        READ_BYTE_28           = 0x3C,
        READ_BYTE_29           = 0x3D,
        READ_BYTE_30           = 0x3E,
        READ_BYTE_31           = 0x3F,
        TEST_PORT_2            = 0xA9,
        TEST_CONTROLLER        = 0xAA,
        TEST_PORT_1            = 0xAB,
        DIAGNOSTICS            = 0xAC,
        READ_CONTROLLER_INPUT  = 0xC0,
        READ_CONTROLLER_OUTPUT = 0xD0
    };

    enum class Command2Bytes : uint8_t {
        WRITE_BYTE_0            = 0x60,
        WRITE_BYTE_1            = 0x61,
        WRITE_BYTE_2            = 0x62,
        WRITE_BYTE_3            = 0x63,
        WRITE_BYTE_4            = 0x64,
        WRITE_BYTE_5            = 0x65,
        WRITE_BYTE_6            = 0x66,
        WRITE_BYTE_7            = 0x67,
        WRITE_BYTE_8            = 0x68,
        WRITE_BYTE_9            = 0x69,
        WRITE_BYTE_10           = 0x6A,
        WRITE_BYTE_11           = 0x6B,
        WRITE_BYTE_12           = 0x6C,
        WRITE_BYTE_13           = 0x6D,
        WRITE_BYTE_14           = 0x6E,
        WRITE_BYTE_15           = 0x6F,
        WRITE_BYTE_16           = 0x70,
        WRITE_BYTE_17           = 0x71,
        WRITE_BYTE_18           = 0x72,
        WRITE_BYTE_19           = 0x73,
        WRITE_BYTE_20           = 0x74,
        WRITE_BYTE_21           = 0x75,
        WRITE_BYTE_22           = 0x76,
        WRITE_BYTE_23           = 0x77,
        WRITE_BYTE_24           = 0x78,
        WRITE_BYTE_25           = 0x79,
        WRITE_BYTE_26           = 0x7A,
        WRITE_BYTE_27           = 0x7B,
        WRITE_BYTE_28           = 0x7C,
        WRITE_BYTE_29           = 0x7D,
        WRITE_BYTE_30           = 0x7E,
        WRITE_BYTE_31           = 0x7F,
        SET_STATUS_0_3          = 0xC1,
        SET_STATUS_4_7          = 0xC2,
        WRITE_CONTROLLER_OUTPUT = 0xD1,
        WRITE_PORT_1_OUTPUT     = 0xD2,
        WRITE_PORT_2_OUTPUT     = 0xD3,
    };

    enum class Command2BytesResponse : uint8_t {};

    enum class SelfTestResponse : uint8_t { PASSED = 0x55, FAILED = 0xFC };

    enum class PortTestResponse : uint8_t {
        PASSED           = 0x00,
        CLOCK_STUCK_LOW  = 0x01,
        CLOCK_STUCK_HIGH = 0x02,
        DATA_STUCK_LOW   = 0x03,
        DATA_STUCK_HIGH  = 0x04
    };

    struct Status {
        uint8_t outputEmpty : 1;
        uint8_t inputEmpty : 1;
        uint8_t systemFlag : 1;
        uint8_t commandFlag : 1;
        uint8_t chipsetSpecific1 : 1;
        uint8_t chipsetSpecific2 : 1;
        uint8_t timeoutError : 1;
        uint8_t parityError : 1;

        Status(uint8_t data);
    } __attribute__((packed));

    struct Configuration {
        uint8_t port1Interrupt : 1;
        uint8_t port2Interrupt : 1;
        uint8_t postPassed : 1;
        const uint8_t reserved1 : 1;
        uint8_t port1Clock : 1;
        uint8_t port2Clock : 1;
        uint8_t port1Translation : 1;
        const uint8_t reserved2 : 1;

        Configuration(uint8_t data);
        Configuration(bool port1Interrupt, bool port2Interrupt, bool postPassed, bool port1Clock,
                      bool port2Clock, bool port1Translation);
        Configuration &operator=(const Configuration &);
        operator uint8_t() const;
    } __attribute__((packed));

    struct ControllerOutput {
        const uint8_t systemReset : 1;
        uint8_t a20Gate : 1;
        uint8_t port2ClockOut : 1;
        uint8_t port2DataOut : 1;
        uint8_t port1BufferFull : 1;
        uint8_t port2BufferFull : 1;
        uint8_t port1ClockOut : 1;
        uint8_t port1DataOut : 1;

        ControllerOutput(uint8_t data);
        ControllerOutput(bool a20Gate, bool port2ClockOut, bool port2DataOut, bool port1BufferFull,
                         bool port2BufferFull, bool port1ClockOut, bool port1DataOut);
        ControllerOutput &operator=(const ControllerOutput &);
        operator uint8_t() const;
    } __attribute__((packed));

    static constexpr uint16_t DATA_PORT    = 0x60;
    static constexpr uint16_t STATUS_PORT  = 0x64;
    static constexpr uint16_t COMMAND_PORT = 0x64;

    IoPort<uint8_t> data;
    InPort<Status> status;
    OutPort<uint8_t> command;

public:
    Ps2Controller();
    // TODO: destructor?

    bool readReady();
    bool writeReady();

    template <class T>
    T readData() {
        return T(data.read());
    }

    template <class T>
    void writeData(T byte) {
        data.write((uint8_t) byte);
    }

    void sendCommand(Command1Byte cmd);

    template <class T>
    T sendCommand(Command1ByteResponse cmd) {
        command.write(static_cast<uint8_t>(cmd));

        while (!readReady()) {
        }
        return readData<T>();
    }

    template <class T>
    void sendCommand(Command2Bytes cmd, T byte) {
        command.write(static_cast<uint8_t>(cmd));

        while (!writeReady()) {
        }
        writeData(byte);
    }

    template <class U, class V>
    U sendCommand(Command2BytesResponse cmd, V byte) {
        command.write(static_cast<uint8_t>(cmd));

        while (!writeReady()) {
        }
        writeData(byte);

        while (!readReady()) {
        }
        return readData<U>();
    }
};

#endif // DANAOS_MACHINE_DEVICES_PS2_PS2CONTROLLER_H_