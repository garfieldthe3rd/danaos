/**
 * machine/devices/ps2/ps2keyboard.cc
**/

#include "ps2keyboard.h"
#include "libk/string.h"
#include "main/kernel.h"

Ps2Keyboard::Ps2Keyboard()
    : Ps2Device(),
      device(DeviceType::ERROR),
      ledStatus(0),
      currRepeatDelay(RepeatDelay::MS_250),
      currRepeatRate(0) {
    // Enable the device to get the interrupts to work
    this->enable();

    // Reset device and flush output
    if (!this->reset()) {
    }
    while (Kernel::ps2->readReady()) {
        Kernel::ps2->readData<uint8_t>();
    }

    // Identify the keyboard
    // device = this->identifyDeviceType();
    Kernel::console.log(KernelConsole::LogLevel::STATUS, "Keyboard type: {}",
                        static_cast<uint8_t>(device));

    // Disable all and any LEDs
    this->setLedStatus(Led::SCROLL_LOCK, false);
    this->setLedStatus(Led::NUM_LOCK, false);
    this->setLedStatus(Led::CAPS_LOCK, false);

    // Set the repeat rate
    this->setRepeatDelay(currRepeatDelay);
    this->setRepeatRate(currRepeatRate);

    Kernel::console.log(KernelConsole::LogLevel::STATUS, "Scan code set: {}",
                        static_cast<uint8_t>(this->getScanCodeSet()));
    // Set scancode 1
    if (!this->setScanCodeSet(ScanCodeSet::SCANCODE_1)) {
        Kernel::panic("Failed to set keyboard scan code!");
    }
    Kernel::console.log(KernelConsole::LogLevel::STATUS, "Scan code set: {}",
                        static_cast<uint8_t>(this->getScanCodeSet()));

    if (!this->setScanCodeSet(ScanCodeSet::SCANCODE_2)) {
        Kernel::panic("Failed to set keyboard scan code!");
    }
    Kernel::console.log(KernelConsole::LogLevel::STATUS, "Scan code set: {}",
                        static_cast<uint8_t>(this->getScanCodeSet()));

    // TODO: check supported scancode sets by reading them back?
}

bool Ps2Keyboard::setScanCodeSet(ScanCodeSet set) {
    if (set == ScanCodeSet::ERROR) {
        // TODO: proper way of telling the person to fuck off?
        return false;
    }

    // Send get/set command, then the desired set number
    sendCommand(static_cast<uint8_t>(Command::GET_SET_SCANCODE_SET));
    if (buffer[0] == ACKNOWLEDGE) {
        sendCommand(static_cast<uint8_t>(set));
        return buffer[0] == ACKNOWLEDGE;
    }

    return false;
}

Ps2Keyboard::ScanCodeSet Ps2Keyboard::getScanCodeSet() {
    // Send get/set, then 0; response should be the current set
    sendCommand(static_cast<uint8_t>(Command::GET_SET_SCANCODE_SET));
    if (buffer[0] == ACKNOWLEDGE) {
        sendCommand(0);
        return static_cast<ScanCodeSet>(buffer[0]);
    }

    return ScanCodeSet::ERROR;
}

bool Ps2Keyboard::setLedStatus(Led led, bool enabled) {
    bool newLedStatus = ledStatus;
    if (enabled) {
        newLedStatus |= 1 << static_cast<uint8_t>(led);
    } else {
        newLedStatus &= ~(1 << static_cast<uint8_t>(led));
    }

    sendCommand(static_cast<uint8_t>(Command::SET_LEDS));
    if (buffer[0] == ACKNOWLEDGE) {
        sendCommand(newLedStatus);
        if (buffer[0] == ACKNOWLEDGE) {
            ledStatus = newLedStatus;
            return true;
        }
    }

    return false;
}

bool Ps2Keyboard::getLedStatus(Led led) const {
    return ledStatus & (1 << static_cast<uint8_t>(led));
}

Ps2Keyboard::DeviceType Ps2Keyboard::identifyDeviceType() {
    // Disable scanning
    sendCommand(static_cast<uint8_t>(Command::DISABLE_SCANNING));
    if (buffer[0] != ACKNOWLEDGE) {
        return DeviceType::ERROR;
    }

    DeviceType type = DeviceType::ERROR;

    /*for (size_t i = 0; i < COMMAND_RETRIES; i++) {
        while (!Kernel::ps2->writeReady()) {
        }
        // Prepare the counters for timeout and expected response bytes
        responses.clear();
        execBytes = 4;
        // Send the command
        Kernel::ps2->writeData(Command::IDENTIFY);

        // TODO: proper timeout!
        for (size_t i = 0; i < 1000000; i++) {
            math::logl(10, i);
        }

        // TODO: uhhhh... we may need to send commands y'know...
        execBytes = 0;

        // Get the initial response
        uint8_t response = this->responses.front();
        this->responses.pop();

        if (response != static_cast<uint8_t>(Response::RESEND)) {
            if (response == static_cast<uint8_t>(Response::ACKNOWLEDGE)) {
                // If it was acknowledged, we can now identify the device
                if (this->responses.empty()) {
                    type = DeviceType::AT_KEYBOARD_TRANSLATED;
                } else {
                    uint8_t byte = this->responses.front();
                    this->responses.pop();

                    // TODO: remove magic numbers
                    if (byte == 0x00) {
                        type = DeviceType::MOUSE;
                    } else if (byte == 0x03) {
                        type = DeviceType::MOUSE_SCROLL_WHEEL;
                    } else if (byte == 0x04) {
                        type = DeviceType::MOUSE_SCROLL_WHEEL;
                    } else if (byte == 0xAB && !this->responses.empty()) {
                        byte = this->responses.front();
                        this->responses.pop();

                        if ((byte == 0x41 || byte == 0xAB) && !this->responses.empty()) {
                            byte = this->responses.front();
                            this->responses.pop();
                            if (byte == 0xC1 && this->responses.empty()) {
                                type = DeviceType::MF2_KEYBOARD_TRANSLATED;
                            }
                        } else if (byte == 0x83 && this->responses.empty()) {
                            type = DeviceType::MF2_KEYBOARD;
                        }
                    }
                }
            }

            // If it was neither acknowledged nor resend requested,
            // an error occurred; we'll break anyway
            break;
        }
    }

    this->responses.clear();*/

    // Re-enable scanning
    sendCommand(static_cast<uint8_t>(Command::ENABLE_SCANNING));
    if (buffer[0] != ACKNOWLEDGE) {
        // TODO: proper error communication! Trigger reset?
        return DeviceType::ERROR;
    }
    return type;
}

bool Ps2Keyboard::setRepeatDelay(RepeatDelay delay) {
    sendCommand(static_cast<uint8_t>(Command::SET_TYPEMATIC_BYTE));
    if (buffer[0] == ACKNOWLEDGE) {
        uint8_t typematic = (currRepeatRate & 0x1F) | (static_cast<uint8_t>(delay) << 5);
        sendCommand(typematic);
        if (buffer[0] == ACKNOWLEDGE) {
            currRepeatDelay = delay;
            return true;
        }
    }

    return false;
}

bool Ps2Keyboard::setRepeatRate(uint8_t rate) {
    rate = math::min(math::max(rate, MIN_REPEAT_RATE), MAX_REPEAT_RATE);

    sendCommand(static_cast<uint8_t>(Command::SET_TYPEMATIC_BYTE));
    if (buffer[0] == ACKNOWLEDGE) {
        uint8_t typematic = (rate & 0x1F) | (static_cast<uint8_t>(currRepeatDelay) << 5);
        sendCommand(typematic);

        if (buffer[0] == ACKNOWLEDGE) {
            currRepeatRate = rate;
            return true;
        }
    }

    return false;
}

Ps2Keyboard::RepeatDelay Ps2Keyboard::getRepeatDelay() const {
    return currRepeatDelay;
}

uint8_t Ps2Keyboard::getRepeatRate() const {
    return currRepeatRate;
}

void Ps2Keyboard::enable() {
    Kernel::pic->enable(InterruptController::Irq::PS2_1, this);
}

void Ps2Keyboard::disable() {
    Kernel::pic->disable(InterruptController::Irq::PS2_1);
}

void Ps2Keyboard::handle(uint8_t data) {
    std::string key(16, ' ');
    for (size_t i = 0; i < key.length(); i++) {
        Kernel::cga.print(80 - key.length() + i, 20, key[i]);
    }
    key = "Key pressed: " + std::to_string(data);

    for (size_t i = 0; i < key.length(); i++) {
        Kernel::cga.print(80 - key.length() + i, 20, key[i]);
    }
}