/**
 * machine/device/ps2/ps2device.h
**/

#ifndef DANAOS_MACHINE_PS2_PS2DEVICE_H_
#define DANAOS_MACHINE_PS2_PS2DEVICE_H_

#include "libk/buffer.h"
#include "machine/interrupts/isr.h"

class Ps2Device : public InterruptGate {
protected:
    // TODO: this needs to be atomic!
    Buffer<uint8_t, 8> buffer;
    uint8_t execBytes;

protected:
    enum class DeviceType : uint8_t {
        MOUSE,
        MOUSE_SCROLL_WHEEL,
        MOUSE_5_BUTTONS,
        AT_KEYBOARD_TRANSLATED,
        MF2_KEYBOARD,
        MF2_KEYBOARD_TRANSLATED,
        ERROR
    };

    static constexpr size_t COMMAND_RETRIES   = 3;
    static constexpr uint8_t SELF_TEST_PASSED = 0xAA;
    static constexpr uint8_t ACKNOWLEDGE      = 0xFA;
    static constexpr uint8_t RESEND           = 0xFE;
    static constexpr uint8_t RESET            = 0xFF;

    void sendCommand(uint8_t cmd, size_t additionalResponseBytes = 0);
    void awaitResponse();

public:
    // TODO: input device type
    Ps2Device();
    virtual ~Ps2Device();
    virtual void trigger(InterruptState *state);
    virtual void handle(uint8_t byte) = 0;

    bool reset();
};

#endif // DANAOS_MACHINE_PS2_PS2DEVICE_H_