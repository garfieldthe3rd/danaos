/**
 * machine/devices/ps2/ps2.cc
**/

#include "ps2.h"
#include "main/kernel.h"

Ps2Controller::Ps2Controller() : data(DATA_PORT), status(STATUS_PORT), command(COMMAND_PORT) {
    // TODO: init USB controller and disable legacy!
    // TODO: check if we even exist!

    // Disable both ports
    this->sendCommand(Command1Byte::DISABLE_PORT_1);
    this->sendCommand(Command1Byte::DISABLE_PORT_2);

    // Flush output buffer
    while (readReady()) {
        data.read();
    }

    // Disable all interrupts and translation
    auto conf             = this->sendCommand<Configuration>(Command1ByteResponse::READ_BYTE_0);
    conf.port1Interrupt   = 0;
    conf.port2Interrupt   = 0;
    conf.port1Translation = 0;
    this->sendCommand(Command2Bytes::WRITE_BYTE_0, conf);

    // Test the controller
    auto selfTest = this->sendCommand<SelfTestResponse>(Command1ByteResponse::TEST_CONTROLLER);
    if (selfTest != SelfTestResponse::PASSED) {
        Kernel::panic("PS/2 controller failed self-test!");
    }

    // TODO: check if two ports are present!

    // Test the ports themselves
    // TODO: test both ports
    auto port1Test = this->sendCommand<PortTestResponse>(Command1ByteResponse::TEST_PORT_1);
    if (port1Test != PortTestResponse::PASSED) {
        Kernel::panic("Port 1 failed test with code {}!", static_cast<uint8_t>(port1Test));
    }

    // Enable the ports again
    // TODO: enable port 2 if present
    this->sendCommand(Command1Byte::ENABLE_PORT_1);
    // Enable IRQs
    // TODO: also enable for port 2
    conf                = this->sendCommand<Configuration>(Command1ByteResponse::READ_BYTE_0);
    conf.port1Interrupt = 1;
    this->sendCommand(Command2Bytes::WRITE_BYTE_0, conf);
}

bool Ps2Controller::readReady() {
    return status.read().outputEmpty;
}

bool Ps2Controller::writeReady() {
    return !status.read().inputEmpty;
}

void Ps2Controller::sendCommand(Command1Byte cmd) {
    command.write(static_cast<uint8_t>(cmd));
}

/*******************************************/

Ps2Controller::Status::Status(uint8_t data)
    : outputEmpty(data & 0x01),
      inputEmpty((data >> 1) & 0x01),
      systemFlag((data >> 2) & 0x01),
      commandFlag((data >> 3) & 0x01),
      chipsetSpecific1((data >> 4) & 0x01),
      chipsetSpecific2((data >> 5) & 0x01),
      timeoutError((data >> 6) & 0x01),
      parityError((data >> 7) & 0x01) {
}

Ps2Controller::Configuration::Configuration(uint8_t data)
    : port1Interrupt(data & 0x01),
      port2Interrupt((data >> 1) & 0x01),
      postPassed((data >> 2) & 0x01),
      reserved1(0),
      port1Clock((data >> 4) & 0x01),
      port2Clock((data >> 5) & 0x01),
      port1Translation((data >> 6) & 0x01),
      reserved2(0) {
}

Ps2Controller::Configuration::Configuration(bool port1Interrupt, bool port2Interrupt,
                                            bool postPassed, bool port1Clock, bool port2Clock,
                                            bool port1Translation)
    : port1Interrupt(port1Interrupt),
      port2Interrupt(port2Interrupt),
      postPassed(postPassed),
      reserved1(0),
      port1Clock(port1Clock),
      port2Clock(port2Clock),
      port1Translation(port1Translation),
      reserved2(0) {
}

Ps2Controller::Configuration &Ps2Controller::Configuration::operator=(const Configuration &conf) {
    this->port1Interrupt   = conf.port1Interrupt;
    this->port2Interrupt   = conf.port2Interrupt;
    this->postPassed       = conf.postPassed;
    this->port1Clock       = conf.port1Clock;
    this->port2Clock       = conf.port2Clock;
    this->port1Translation = conf.port1Translation;

    return *this;
}

Ps2Controller::Configuration::operator uint8_t() const {
    return port1Interrupt | (port2Interrupt << 1) | (postPassed << 2) | (reserved1 << 3) |
           (port1Clock << 4) | (port2Clock << 5) | (port1Translation << 6) | (reserved2 << 7);
}

Ps2Controller::ControllerOutput::ControllerOutput(uint8_t data)
    : systemReset(1),
      a20Gate((data >> 1) & 0x01),
      port2ClockOut((data >> 2) & 0x01),
      port2DataOut((data >> 3) & 0x01),
      port1BufferFull((data >> 4) & 0x01),
      port2BufferFull((data >> 5) & 0x01),
      port1ClockOut((data >> 6) & 0x01),
      port1DataOut((data >> 7) & 0x01) {
}

Ps2Controller::ControllerOutput::ControllerOutput(bool a20Gate, bool port2ClockOut,
                                                  bool port2DataOut, bool port1BufferFull,
                                                  bool port2BufferFull, bool port1ClockOut,
                                                  bool port1DataOut)
    : systemReset(1),
      a20Gate(a20Gate),
      port2ClockOut(port2ClockOut),
      port2DataOut(port2DataOut),
      port1BufferFull(port1BufferFull),
      port2BufferFull(port2BufferFull),
      port1ClockOut(port1ClockOut),
      port1DataOut(port1DataOut) {
}

Ps2Controller::ControllerOutput &Ps2Controller::ControllerOutput::
operator=(const ControllerOutput &out) {
    this->a20Gate         = out.a20Gate;
    this->port2ClockOut   = out.port2ClockOut;
    this->port2DataOut    = out.port2DataOut;
    this->port1BufferFull = out.port1BufferFull;
    this->port2BufferFull = out.port2BufferFull;
    this->port1ClockOut   = out.port1ClockOut;
    this->port1DataOut    = out.port1DataOut;

    return *this;
}

Ps2Controller::ControllerOutput::operator uint8_t() const {
    return systemReset | (a20Gate << 1) | (port2ClockOut << 2) | (port2DataOut << 3) |
           (port1BufferFull << 4) | (port2BufferFull << 5) | (port1ClockOut << 6) |
           (port1DataOut << 7);
}