/**
 * machine/device/ps2/ps2device.h
**/

#include "ps2device.h"
#include "main/kernel.h"

Ps2Device::Ps2Device() : buffer(), execBytes() {
    // TODO: detect type here?
}

Ps2Device::~Ps2Device() {
    // TODO
}

void Ps2Device::sendCommand(uint8_t cmd, size_t additionalResponseBytes) {
    // Try to send the command to the devices only so many times
    for (size_t i = 0; i < COMMAND_RETRIES; i++) {
        // Wait for the controller to be ready
        while (!Kernel::ps2->writeReady()) {
        }

        // Wipe the response buffer
        buffer.clear();

        execBytes = additionalResponseBytes + 1;
        // Send the command
        Kernel::ps2->writeData(cmd);

        // Wait for the device's response
        awaitResponse();

        uint8_t response = buffer[0];
        if (response != RESEND) {
            return;
        }
    }
}

void Ps2Device::awaitResponse() {
    while (execBytes) {
    }
}

bool Ps2Device::reset() {
    // Special command; returns both ACKNOWLEDGE and
    // then a status code, thus needs different sendings
    sendCommand(RESET, 1);
    if (buffer[0] == ACKNOWLEDGE) {
        return buffer[1] == SELF_TEST_PASSED;
    }

    return false;
}

void Ps2Device::trigger(InterruptState *state) {
    // Read the data
    uint8_t data = Kernel::ps2->readData<uint8_t>();

    if (execBytes > 0) {
        buffer.push(data);
        execBytes--;

        if (data == RESEND) {
            execBytes = 0;
        }
    } else {
        this->handle(data);
    }
}