/**
 * machine/devices/ps2/ps2keyboard.h
**/

#ifndef DANAOS_MACHINE_DEVICES_PS2_PS2KEYBOARD_H_
#define DANAOS_MACHINE_DEVICES_PS2_PS2KEYBOARD_H_

#include "libk/queue.h"
#include "machine/devices/ps2/ps2device.h"

// TODO: command queue!

class Ps2Keyboard : public Ps2Device {
private:
    enum class ScanCodeSet : uint8_t {
        ERROR      = 0x00,
        SCANCODE_1 = 0x01,
        SCANCODE_2 = 0x02,
        SCANCODE_3 = 0x03
    };

    enum class Led : uint8_t { SCROLL_LOCK = 0, NUM_LOCK = 1, CAPS_LOCK = 2 };

    enum class RepeatDelay : uint8_t { MS_250 = 0, MS_500 = 1, MS_750 = 2, MS_1000 = 3 };

    enum class Response : uint8_t {
        ERROR_1            = 0x00,
        SELF_TEST_PASSED   = 0xAA,
        TIMEOUT            = 0xBB,
        ECHO               = 0xEE,
        SELF_TEST_FAILED_1 = 0xFC,
        SELF_TEST_FAILED_2 = 0xFD,
        ACKNOWLEDGE        = 0xFA,
        RESEND             = 0xFE,
        ERROR_2            = 0xFF
    };

    enum class Command : uint8_t {
        SET_LEDS             = 0xED,
        ECHO                 = 0xEE,
        GET_SET_SCANCODE_SET = 0xF0,
        IDENTIFY             = 0xF2,
        SET_TYPEMATIC_BYTE   = 0xF3,
        ENABLE_SCANNING      = 0xF4,
        DISABLE_SCANNING     = 0xF5,
        // TODO: autorepeat, make, release etc.!
        RESEND = 0xFE,
        RESET  = 0xFF
    };

    enum class Command1Byte : uint8_t { RESEND = 0xFE, RESET = 0xFF };

    enum class Command2Bytes : uint8_t { SET_LEDS = 0xED, ECHO = 0xEE };

    // Scancode set is special since it return depending on getting or setting
    static constexpr size_t COMMAND_RETRIES  = 3;
    static constexpr uint8_t MIN_REPEAT_RATE = 0;
    static constexpr uint8_t MAX_REPEAT_RATE = 31;

    bool setScanCodeSet(ScanCodeSet set);
    ScanCodeSet getScanCodeSet();

    bool setLedStatus(Led led, bool enabled);
    bool getLedStatus(Led led) const;

    DeviceType identifyDeviceType();

    DeviceType device;

    // Current state of Ps2keyboard
    uint8_t ledStatus;
    RepeatDelay currRepeatDelay;
    uint8_t currRepeatRate;

public:
    Ps2Keyboard();

    bool setRepeatDelay(RepeatDelay delay);
    bool setRepeatRate(uint8_t rate);
    RepeatDelay getRepeatDelay() const;
    uint8_t getRepeatRate() const;

    virtual void enable();
    virtual void disable();
    virtual void handle(uint8_t byte);
};

#endif // DANAOS_MACHINE_DEVICES_PS2_PS2KEYBOARD_H_