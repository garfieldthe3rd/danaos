/**
 * machine/devices/pic.cc
**/

#include "pic.h"
#include "main/kernel.h"

// TODO: is auto EOI a good thing? So far it's off...
const InterruptController::InitControlWord1 InterruptController::ICW1_MASTER(true, false, false,
                                                                             false, true);
const InterruptController::InitControlWord1 InterruptController::ICW1_SLAVE(true, false, false,
                                                                            false, true);
const InterruptController::InitControlWord2
    InterruptController::ICW2_MASTER(InterruptController::MASTER_IDT_OFFSET);
const InterruptController::InitControlWord2
    InterruptController::ICW2_SLAVE(InterruptController::MASTER_IDT_OFFSET + 8);
const InterruptController::InitControlWord3
    InterruptController::ICW3_MASTER(static_cast<uint8_t>(Irq::SLAVE_IRQ), false);
const InterruptController::InitControlWord3
    InterruptController::ICW3_SLAVE(static_cast<uint8_t>(Irq::SLAVE_IRQ), true);
const InterruptController::InitControlWord4 InterruptController::ICW4_MASTER(true, false, false,
                                                                             false, false);
const InterruptController::InitControlWord4 InterruptController::ICW4_SLAVE(true, false, false,
                                                                            false, false);

InterruptController::InterruptController()
    : commandMaster(MASTER_PORT),
      commandSlave(SLAVE_PORT),
      dataMaster(MASTER_PORT + 1),
      dataSlave(SLAVE_PORT + 1) {
    for (size_t i = 0; i < sizeof(irqGates) / sizeof(InterruptGate *); i++) {
        irqGates[i] = nullptr;
    }
    // Iniitalize both PICs!
    commandMaster.write((uint8_t) InterruptController::ICW1_MASTER);
    commandSlave.write((uint8_t) InterruptController::ICW1_SLAVE);

    // Send the ICW2 (the IDT offsets) to the respective data registers
    dataMaster.write((uint8_t) InterruptController::ICW2_MASTER);
    dataSlave.write((uint8_t) InterruptController::ICW2_SLAVE);

    // Set the slave's IRQ in both PICs
    dataMaster.write((uint8_t) InterruptController::ICW3_MASTER);
    dataSlave.write((uint8_t) InterruptController::ICW3_SLAVE);

    // If specified, write ICW4 too (e.g. for setting x86)
    if (InterruptController::ICW1_MASTER.expectCw4) {
        dataMaster.write((uint8_t) InterruptController::ICW4_MASTER);
    }
    if (InterruptController::ICW1_SLAVE.expectCw4) {
        dataSlave.write((uint8_t) InterruptController::ICW4_SLAVE);
    }

    // Clear the data registers to disallow any IRQ
    dataMaster.write(0xFF);
    dataSlave.write(0xFF);

    // Allow slave interrupts to go through to master
    this->enable(Irq::SLAVE_IRQ, this);
}

InterruptController::~InterruptController() {
    // TODO: disable PIC itself somehow?
    this->disable();
}

void InterruptController::enable(Irq irq, InterruptGate *gate) {
    // TODO: use OCW1 struct
    // TODO: check gate!

    // Set the interrupt gate
    irqGates[static_cast<uint8_t>(irq)] = gate;

    // Distinguish between master and slave
    if (static_cast<uint8_t>(irq) < 8) {
        // Disable the bit in the mask to allow IRQ
        uint8_t mask = 0xFF & ~(1 << static_cast<uint8_t>(irq));
        mask &= dataMaster.read();
        dataMaster.write(mask);
    } else {
        // Disable the bit in the mask to allow IRQ
        uint8_t mask = 0xFF & ~(1 << (static_cast<uint8_t>(irq) - 8));
        mask &= dataSlave.read();
        dataSlave.write(mask);
    }
}

void InterruptController::disable(Irq irq) {
    // TODO: use OCW1 struct
    if (irq == Irq::SLAVE_IRQ) {
        // We do not allow the slave IRQ to be masked!
        return;
    }

    // Remove the interrupt gate
    irqGates[static_cast<uint8_t>(irq)] = nullptr;

    // Distinguish between master and slave
    if (static_cast<uint8_t>(irq) < 8) {
        // Enable the bit in the mask to disallow IRQ
        uint8_t mask = 1 << static_cast<uint8_t>(irq);
        mask |= dataMaster.read();
        dataMaster.write(mask);
    } else {
        // Disable the bit in the mask to allow IRQ
        uint8_t mask = 1 << (static_cast<uint8_t>(irq) - 8);
        mask |= dataSlave.read();
        dataSlave.write(mask);
    }
}

bool InterruptController::isEnabled(Irq irq) {
    // TODO: use OCW1 struct
    // Distinguish between master and slave
    if (static_cast<uint8_t>(irq) < 8) {
        // Check if bit is disabled
        uint8_t mask = dataMaster.read();
        return !(mask & (1 << static_cast<uint8_t>(irq)));
    } else {
        // Check if bit is disabled
        uint8_t mask = dataSlave.read();
        return !(mask & (1 << (static_cast<uint8_t>(irq) - 8)));
    }
}

void InterruptController::acknowledge(Irq irq) {
    // TODO: selection necessary/good?
    uint8_t irqNum = static_cast<uint8_t>(irq);
    OpCommandWord2 ocw2(irqNum, true, true, false);
    if (irqNum >= 8) {
        ocw2.affectedIrq -= 8;
        commandSlave.write((uint8_t) ocw2);
        // If the slave got the IRQ, the master must have also
        // gotten the interrupt for the slave, so EOI it, too
        ocw2.affectedIrq = static_cast<uint8_t>(Irq::SLAVE_IRQ);
        commandMaster.write((uint8_t) ocw2);
    } else {
        commandMaster.write((uint8_t) ocw2);
    }
}

bool InterruptController::isServiced(Irq irq) {
    OpCommandWord3 ocw3(InternalRegister::ISR, false, SpecialMaskAction::NONE);

    uint8_t index = static_cast<uint8_t>(irq);
    if (index >= 8) {
        index -= 8;
        commandSlave.write(ocw3);
        // Check if the IRQ is set in the ISR
        return dataSlave.read() & (1 << index);
    } else {
        commandMaster.write(ocw3);
        // Check if the IRQ is set in the ISR
        return dataMaster.read() & (1 << index);
    }
}

void InterruptController::enable() {
    // Set the gates for all 8 IRQs for both PICs to this
    for (size_t i = 0; i < 8; i++) {
        Kernel::isrPlugbox->assignGate(MASTER_IDT_OFFSET + i, this);
        Kernel::isrPlugbox->assignGate(SLAVE_IDT_OFFSET + i, this);
    }
}

void InterruptController::disable() {
    // Remove the gates for all 8 IRQs for both PICs
    for (size_t i = 0; i < 8; i++) {
        Kernel::isrPlugbox->releaseGate(MASTER_IDT_OFFSET + i);
        Kernel::isrPlugbox->releaseGate(SLAVE_IDT_OFFSET + i);
    }
}

void InterruptController::trigger(InterruptState *state) {
    uint32_t index = state->interruptIndex - MASTER_IDT_OFFSET;
    if (index >= 8) {
        // Master/slave do not have to be consecutive in IDT
        index = state->interruptIndex - SLAVE_IDT_OFFSET + 8;
    }

    // Check for spurious interrupts
    if (index == 7 || index == 15) {
        if (!this->isServiced(static_cast<Irq>(index))) {
            // Not set in the ISR -> spurious!
            return;
        }
    }

    if (this->irqGates[index] == nullptr) {
        // TODO: proper panic ^.^
        Kernel::panic("IRQ without gate triggered: {}/{}!", state->interruptIndex, index);
    } else {
        this->irqGates[index]->trigger(state);
        // Acknowledge the IRQ
        acknowledge(static_cast<Irq>(index));
    }
}

/**
 * Init and Op Control Word methods
**/

InterruptController::InitControlWord1::InitControlWord1(bool expectCw4, bool singlePic,
                                                        bool reducedCallInterval,
                                                        bool levelTriggered,
                                                        bool startInitialization, uint8_t ivAddress)
    : expectCw4(expectCw4),
      singlePic(singlePic),
      reducedCallInterval(reducedCallInterval),
      levelTriggered(levelTriggered),
      initialization(startInitialization),
      ivAddress(ivAddress) {
}

InterruptController::InitControlWord1 &InterruptController::InitControlWord1::
operator=(const InitControlWord1 &cw1) {
    this->expectCw4           = cw1.expectCw4;
    this->singlePic           = cw1.singlePic;
    this->reducedCallInterval = cw1.reducedCallInterval;
    this->levelTriggered      = cw1.levelTriggered;
    this->initialization      = cw1.initialization;
    this->ivAddress           = cw1.ivAddress;

    return *this;
}

InterruptController::InitControlWord1::operator uint8_t() const {
    return expectCw4 | (singlePic << 1) | (reducedCallInterval << 2) | (levelTriggered << 3) |
           (initialization << 4);
}

InterruptController::InitControlWord2::InitControlWord2(uint8_t idtOffset) : idtOffset(idtOffset) {
}

InterruptController::InitControlWord2::operator uint8_t() const {
    return idtOffset;
}

InterruptController::InitControlWord3::InitControlWord3(uint8_t irqNum, bool slave) {
    // If the word is to be send to the master, the IRQ number
    // isn't specified directly but instead the bit of it
    if (slave) {
        this->slaveIrq = irqNum;
    } else {
        this->slaveIrq = 1 << (irqNum & 0x07);
    }
}

InterruptController::InitControlWord3::operator uint8_t() const {
    return slaveIrq;
}

InterruptController::InitControlWord4::InitControlWord4(bool x86Mode, bool autoEoi,
                                                        bool selectBufferMaster, bool bufferedMode,
                                                        bool fullyNested)
    : x86Mode(x86Mode),
      autoEoi(autoEoi),
      selectBufferMaster(selectBufferMaster),
      bufferedMode(bufferedMode),
      fullyNested(fullyNested),
      reserved(0) {
    // Ensure that only supported bits are set
    if (x86Mode) {
        fullyNested = 0;
    }
    if (!bufferedMode) {
        selectBufferMaster = 0;
    }
}

InterruptController::InitControlWord4 &InterruptController::InitControlWord4::
operator=(const InitControlWord4 &cw4) {
    this->x86Mode            = cw4.x86Mode;
    this->autoEoi            = cw4.autoEoi;
    this->selectBufferMaster = cw4.selectBufferMaster;
    this->bufferedMode       = cw4.bufferedMode;
    this->fullyNested        = cw4.fullyNested;

    // Ensure that only supported bits are set
    if (x86Mode) {
        fullyNested = 0;
    }
    if (!bufferedMode) {
        selectBufferMaster = 0;
    }

    return *this;
}
InterruptController::InitControlWord4::operator uint8_t() const {
    return x86Mode | (autoEoi << 1) | (selectBufferMaster << 2) | (bufferedMode << 3) |
           (fullyNested << 4);
}

InterruptController::OpCommandWord1::operator uint8_t() const {
    return irq1 | (irq2 << 1) | (irq3 << 1) | (irq4 << 1) | (irq5 << 1) | (irq6 << 1) |
           (irq7 << 1) | (irq8 << 1);
}

InterruptController::OpCommandWord2::OpCommandWord2(uint8_t affectedIrq, bool eoi, bool selection,
                                                    bool rotation)
    : affectedIrq(affectedIrq & 0x07),
      reserved(0),
      eoi(eoi),
      selection(selection),
      rotation(rotation) {
}

InterruptController::OpCommandWord2 &InterruptController::OpCommandWord2::
operator=(const OpCommandWord2 &oc2) {
    this->affectedIrq = oc2.affectedIrq;
    this->eoi         = oc2.eoi;
    this->selection   = oc2.selection;
    this->rotation    = oc2.rotation;

    return *this;
}

InterruptController::OpCommandWord2::operator uint8_t() const {
    return affectedIrq | (eoi << 5) | (selection << 6) | (rotation << 7);
}

InterruptController::OpCommandWord3::OpCommandWord3(InternalRegister reg, bool issuePoll,
                                                    SpecialMaskAction maskAction)
    : internal(reg), pollCommand(issuePoll), reserved1(0x01), mask(maskAction), reserved2(0) {
}

InterruptController::OpCommandWord3 &InterruptController::OpCommandWord3::
operator=(const OpCommandWord3 &cw3) {
    this->internal    = cw3.internal;
    this->pollCommand = cw3.pollCommand;
    this->mask        = cw3.mask;

    return *this;
}

InterruptController::OpCommandWord3::operator uint8_t() const {
    return static_cast<uint8_t>(internal) | (pollCommand << 2) | (reserved1 << 3) |
           (static_cast<uint8_t>(mask) << 5);
}