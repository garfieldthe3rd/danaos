/**
 * machine/devices/cpu.cc
**/

#include "machine/devices/cpu.h"
#include <stdint.h>

namespace cpu {

// TODO: this won't work for multiple CPU's/cores/w.e
bool enableInterrupts() {
    uint16_t volatile flags = 0;
    __asm__ volatile("pushf\n\t"
                     "sti\n\t"
                     "movw (%%esp), %0\n\t"
                     "add $4, %%esp\n\t"
                     : "=r"(flags));
    return flags & (1 << 9);
}

bool disableInterrupts() {
    uint16_t volatile flags = 0;
    __asm__ volatile("pushf\n\t"
                     "cli\n\t"
                     "movw (%%esp), %0\n\t"
                     "add $4, %%esp\n\t"
                     : "=r"(flags));
    return flags & (1 << 9);
}

void halt() {
    __asm__ volatile("hlt");
}

} // namespace cpu