/**
 * machine/io/serial.cc
**/

#include "serial.h"

SerialPort::SerialPort(uint16_t baseAddress)
    : transmitter(baseAddress),
      receiver(baseAddress),
      divisorLowerByte(baseAddress),
      interruptEnable(baseAddress + 1),
      divisorUpperByte(baseAddress + 1),
      fifoControl(baseAddress + 2),
      interruptIdentification(baseAddress + 2),
      lineControl(baseAddress + 3),
      modemControl(baseAddress + 4),
      lineStatus(baseAddress + 5),
      modemStatus(baseAddress + 6),
      scratch(baseAddress + 7) {
    // Disable interrupts by default
    interruptEnable.write(InterruptEnableRegister(0));

    // TODO: move this to methods
    // Set the default line configuration
    LineControlRegister lc = lineControl.read();
    lc.length              = WordLength::BITS_8;
    lc.extendedStop        = 0;
    lc.parity              = ParityType::NONE;
    lc.breakEnabled        = false;
    lineControl.write(lc);

    // Set the default FIFO configuration
    FifoControlRegister fc(0);
    fc.enableFifos       = 1;
    fc.clearReceiveFifo  = 1;
    fc.clearTransmitFifo = 1;
    fc.triggerLevel      = InterruptTriggerLevel::BYTES_14;
    fifoControl.write(fc);

    // Set the default modem configuration
    ModemControlRegister mc = modemControl.read();
    mc.dataTerminalReady    = 1;
    mc.requestToSend        = 1;
    mc.auxOut2              = 1;
    modemControl.write(mc);
}

SerialPort::~SerialPort() {
    // TODO: shut down connection?
}

bool SerialPort::receiveReady() const {
    return lineStatus.read().dataReady != 0;
}

bool SerialPort::sendReady() const {
    return lineStatus.read().emptyTransmitter != 0;
}

uint8_t SerialPort::receive() const {
    // TODO: make this not blocking?
    while (!this->receiveReady())
        ;
    return receiver.read();
}

void SerialPort::send(uint8_t data) const {
    // TODO: make this not blocking?
    while (!this->sendReady())
        ;
    transmitter.write(data);
}

void SerialPort::send(const char *str) const {
    // Send an entire string (that means null-terminated!)
    while ((*str) != 0) {
        this->send(*(str++));
    }
}

uint16_t SerialPort::getClockDivisor() const {
    // Tell the port that we want to access the divisor
    LineControlRegister line = lineControl.read();
    line.divisorLatchAccess  = 1;
    lineControl.write(line);

    // The divisor is composed of upper and lower byte -> OR
    uint16_t divisor = (divisorUpperByte.read() << 8) | divisorLowerByte.read();

    // Disable divisor access again
    line.divisorLatchAccess = 0;
    lineControl.write(line);

    return divisor;
}

void SerialPort::setClockDivisor(uint16_t divisor) const {
    // Make sure that the divisor isn't zero
    if (divisor == 0) {
        divisor = 1;
    }

    // Tell the port that we want to access the divisor
    LineControlRegister line = lineControl.read();
    line.divisorLatchAccess  = 1;
    lineControl.write(line);

    // The divisor is composed of upper and lower byte
    divisorLowerByte.write(divisor & 0xFF);
    divisorUpperByte.write((divisor >> 8) & 0xFF);

    // Disable divisor access again
    line.divisorLatchAccess = 0;
    lineControl.write(line);
}

/*
 * Constructors and conversions for the register struct's
**/

SerialPort::InterruptEnableRegister::InterruptEnableRegister(uint8_t data)
    : receivedDataAvailable(data & 0x01),
      transmitterEmpty((data >> 1) & 0x01),
      receiverLineStatus((data >> 2) & 0x01),
      modemStatus((data >> 3) & 0x01),
      sleepMode((data >> 4) & 0x01),
      lowPowerMode((data >> 5) & 0x01),
      reserved(0) {
}

SerialPort::InterruptEnableRegister::operator uint8_t() const {
    return receivedDataAvailable | (transmitterEmpty << 1) | (receiverLineStatus << 2) |
           (modemStatus << 3) | (sleepMode << 4) | (lowPowerMode << 5);
}

SerialPort::InterruptIdentificationRegister::InterruptIdentificationRegister(uint8_t data)
    : interruptPending(data & 0x01),
      identifier(static_cast<InterruptIdentification>((data >> 1) & 0x07)),
      reserved(0),
      fifo64Byte((data >> 5) & 0x01),
      fifoStatus(static_cast<FifoStatus>((data >> 6) & 0x03)) {
}

SerialPort::InterruptIdentificationRegister::operator uint8_t() const {
    return interruptPending | (static_cast<uint8_t>(identifier) << 1) | (fifo64Byte << 5) |
           (static_cast<uint8_t>(fifoStatus) << 6);
}

SerialPort::FifoControlRegister::FifoControlRegister(uint8_t data)
    : enableFifos(data & 0x01),
      clearReceiveFifo((data >> 1) & 0x01),
      clearTransmitFifo((data >> 2) & 0x01),
      dmaModeSelect((data >> 3) & 0x01),
      reserved(0),
      enable64ByteFifo((data >> 5) & 0x01),
      triggerLevel(static_cast<InterruptTriggerLevel>((data >> 6) & 0x03)) {
}

SerialPort::FifoControlRegister::operator uint8_t() const {
    return enableFifos | (clearReceiveFifo << 1) | (clearTransmitFifo << 2) | (dmaModeSelect << 3) |
           (enable64ByteFifo << 5) | (static_cast<uint8_t>(triggerLevel) << 6);
}

SerialPort::LineControlRegister::LineControlRegister(uint8_t data)
    : length(static_cast<WordLength>(data & 0x03)),
      extendedStop((data >> 2) & 0x01),
      parity(static_cast<ParityType>((data >> 3) & 0x07)),
      breakEnabled((data >> 6) & 0x01),
      divisorLatchAccess((data >> 7) & 0x01) {
}

SerialPort::LineControlRegister::operator uint8_t() const {
    return static_cast<uint8_t>(length) | (extendedStop << 2) |
           (static_cast<uint8_t>(parity) << 3) | (breakEnabled << 6) | (divisorLatchAccess << 7);
}

SerialPort::ModemControlRegister::ModemControlRegister(uint8_t data)
    : dataTerminalReady(data & 0x01),
      requestToSend((data >> 1) & 0x01),
      auxOut1((data >> 2) & 0x01),
      auxOut2((data >> 3) & 0x01),
      loopbackMode((data >> 4) & 0x01),
      autoflowControlEnabled((data >> 5) & 0x01),
      reserved(0) {
}

SerialPort::ModemControlRegister::operator uint8_t() const {
    return dataTerminalReady | (requestToSend << 1) | (auxOut1 << 2) | (auxOut2 << 3) |
           (loopbackMode << 4) | (autoflowControlEnabled << 5);
}

SerialPort::LineStatusRegister::LineStatusRegister(uint8_t data)
    : dataReady(data & 0x01),
      overrunError((data >> 1) & 0x01),
      parityError((data >> 2) & 0x01),
      framingError((data >> 3) & 0x01),
      breakInterrupt((data >> 4) & 0x01),
      emptyTransmitter((data >> 5) & 0x01),
      emptyDataRegisters((data >> 6) & 0x01),
      errorInReceivedFifo((data >> 7) & 0x01) {
}

SerialPort::LineStatusRegister::operator uint8_t() const {
    return dataReady | (overrunError << 1) | (parityError << 2) | (framingError << 3) |
           (breakInterrupt << 4) | (emptyTransmitter << 5) | (emptyDataRegisters << 6) |
           (errorInReceivedFifo << 7);
}

SerialPort::ModemStatusRegister::ModemStatusRegister(uint8_t data)
    : deltaClearToSend(data & 0x01),
      deltaDataReady((data >> 1) & 0x01),
      trailingEdgeRingIndicator((data >> 2) & 0x01),
      deltaDataCarrier((data >> 3) & 0x01),
      clearToSend((data >> 4) & 0x01),
      dataReady((data >> 5) & 0x01),
      ringIndicator((data >> 6) & 0x01),
      carrierDetect((data >> 7) & 0x01) {
}

SerialPort::ModemStatusRegister::operator uint8_t() const {
    return deltaClearToSend | (deltaDataReady << 1) | (trailingEdgeRingIndicator << 2) |
           (deltaDataCarrier << 3) | (clearToSend << 4) | (dataReady << 5) | (ringIndicator << 6) |
           (carrierDetect << 7);
}