/**
 * machine/io/serial.h
**/

#ifndef DANAOS_MACHINE_IO_SERIAL_H_
#define DANAOS_MACHINE_IO_SERIAL_H_

#include "port.h"

// TODO: replace reserved bits with unnamed fields?
// TODO: identification of UART by trying to set FIFO enabled;
// if it works check the bits 5, 6 and 7, if not try writing
// to the scratch registers
// TODO: what about const?
// TODO: proper default constructors?
// TODO: initial configuration

class SerialPort {
private:
    enum class InterruptIdentification : uint8_t {
        MODEM_STATUS            = 0,
        TRANSMITTER_EMPTY       = 1,
        RECEIVED_DATA_AVAILABLE = 2,
        RECEIVER_LINE_STATUS    = 3,
        TIMEOUT_PENDING         = 6
    };

    enum class FifoStatus : uint8_t { NO_FIFO = 0, FIFO_ENABLED_NOT_WORKING = 2, FIFO_ENABLED = 3 };

    enum class InterruptTriggerLevel : uint8_t {
        BYTES_1  = 0,
        BYTES_4  = 1,
        BYTES_8  = 2,
        BYTES_14 = 3,
        BYTES_16 = BYTES_4,
        BYTES_32 = BYTES_8,
        BYTES_56 = BYTES_14,
    };

    enum class ParityType : uint8_t { NONE = 0, ODD = 1, EVEN = 3, MARK = 5, SPACE = 7 };

    enum class WordLength : uint8_t { BITS_5 = 0, BITS_6 = 1, BITS_7 = 2, BITS_8 = 3 };

    enum class UartType { UART_8250, UART_16450, UART_16550, UART_16550A, UART_16750 };

    struct InterruptEnableRegister {
        uint8_t receivedDataAvailable : 1;
        uint8_t transmitterEmpty : 1;
        uint8_t receiverLineStatus : 1;
        uint8_t modemStatus : 1;
        uint8_t sleepMode : 1;    // 16750 only?
        uint8_t lowPowerMode : 1; // 16750 only?
        uint8_t reserved : 2;

        InterruptEnableRegister(uint8_t data);
        operator uint8_t() const;
    } __attribute__((packed));

    struct InterruptIdentificationRegister {
        uint8_t interruptPending : 1;
        InterruptIdentification identifier : 3;
        uint8_t reserved : 1;
        uint8_t fifo64Byte : 1; // 16750 only
        FifoStatus fifoStatus : 2;

        InterruptIdentificationRegister(uint8_t data);
        operator uint8_t() const;
    } __attribute__((packed));

    struct FifoControlRegister {
        uint8_t enableFifos : 1;
        uint8_t clearReceiveFifo : 1;
        uint8_t clearTransmitFifo : 1;
        uint8_t dmaModeSelect : 1;
        uint8_t reserved : 1;
        uint8_t enable64ByteFifo : 1; // 16750 only
        InterruptTriggerLevel triggerLevel : 2;

        FifoControlRegister(uint8_t data);
        operator uint8_t() const;
    } __attribute__((packed));

    struct LineControlRegister {
        WordLength length : 2;
        uint8_t extendedStop : 1;
        ParityType parity : 3;
        uint8_t breakEnabled : 1;
        uint8_t divisorLatchAccess : 1;

        LineControlRegister(uint8_t data);
        operator uint8_t() const;
    } __attribute__((packed));

    struct ModemControlRegister {
        uint8_t dataTerminalReady : 1;
        uint8_t requestToSend : 1;
        uint8_t auxOut1 : 1;
        uint8_t auxOut2 : 1;
        uint8_t loopbackMode : 1;
        uint8_t autoflowControlEnabled : 1; // 16750 only
        uint8_t reserved : 2;

        ModemControlRegister(uint8_t data);
        operator uint8_t() const;
    } __attribute__((packed));

    struct LineStatusRegister {
        uint8_t dataReady : 1;
        uint8_t overrunError : 1;
        uint8_t parityError : 1;
        uint8_t framingError : 1;
        uint8_t breakInterrupt : 1;
        uint8_t emptyTransmitter : 1;
        uint8_t emptyDataRegisters : 1;
        uint8_t errorInReceivedFifo : 1;

        LineStatusRegister(uint8_t data);
        operator uint8_t() const;
    } __attribute__((packed));

    struct ModemStatusRegister {
        uint8_t deltaClearToSend : 1;
        uint8_t deltaDataReady : 1;
        uint8_t trailingEdgeRingIndicator : 1;
        uint8_t deltaDataCarrier : 1;
        uint8_t clearToSend : 1;
        uint8_t dataReady : 1;
        uint8_t ringIndicator : 1;
        uint8_t carrierDetect : 1;

        ModemStatusRegister(uint8_t data);
        operator uint8_t() const;
    } __attribute__((packed));

    // Offset 0 (doubles as divisor latch lower byte)
    const OutPort<uint8_t> transmitter;
    const InPort<uint8_t> receiver;
    const IoPort<uint8_t> divisorLowerByte;
    // Offset 1 (doubles as divisor latch upper byte)
    const IoPort<InterruptEnableRegister> interruptEnable;
    const IoPort<uint8_t> divisorUpperByte;
    // Offset 2
    const OutPort<FifoControlRegister> fifoControl;
    const InPort<InterruptIdentificationRegister> interruptIdentification;
    // Offset 3
    const IoPort<LineControlRegister> lineControl;
    // Offset 4
    const IoPort<ModemControlRegister> modemControl;
    // Offset 5
    const InPort<LineStatusRegister> lineStatus;
    // Offset 6
    const InPort<ModemStatusRegister> modemStatus;
    // Offset 7
    const IoPort<uint8_t> scratch;

public:
    static constexpr uint16_t COM_1 = 0x3f8;

    SerialPort(uint16_t baseAddress);
    ~SerialPort();

    bool receiveReady() const;
    bool sendReady() const;

    uint8_t receive() const;
    void send(uint8_t data) const;
    void send(const char *str) const;

    uint16_t getClockDivisor() const;
    void setClockDivisor(uint16_t divisor) const;
};

#endif // DANAOS_MACHINE_IO_SERIAL_H_