/**
 * machine/interrupts/isr.cc
**/

#include "isr.h"
#include "main/kernel.h"
#include <stddef.h>

extern "C" void isrHandler(InterruptState *state) {
    // TODO: state must not be null! ('ensure' that with assert?)
    if (state == nullptr) {
        Kernel::panic("Interrupt state must not be null!");
    }
    if (state->interruptIndex >= InterruptDescriptorTable::GATE_COUNT) {
        Kernel::panic("Interrupt index {} out of bounds!", state->interruptIndex);
    }
    Kernel::isrPlugbox->triggerGate(state);
    /*Kernel::log("Called handler {}!", intState->interruptIndex);
    Kernel::log("Error code: []; EFLAGS: []!", intState->errorCode, intState->eflags);
    Kernel::log("EAX:[] EBX:[] ECX:[] EDX:[]", intState->cpuState.eax, intState->cpuState.ebx,
                intState->cpuState.ecx, intState->cpuState.edx);*/
}

InterruptGate::~InterruptGate() {
    // TODO: what clean-up is necessary?
    // this->disable();
    // TODO: how to trigger release???
    Kernel::isrPlugbox->releaseGate(this);
}

void IsrPlugbox::panic(InterruptState *state) const {
    Kernel::panic("Attempted execution of missing interrupt gate {}!", state->interruptIndex);
}

IsrPlugbox::IsrPlugbox() : panicGate(nullptr) {
    for (size_t i = 0; i < GATE_COUNT; i++) {
        this->gates[i] = nullptr;
    }
}

IsrPlugbox::~IsrPlugbox() {
    // TODO: what clean-up?
    for (size_t i = 0; i < GATE_COUNT; i++) {
        this->releaseGate(i);
    }
}

void IsrPlugbox::setPanicGate(InterruptGate *gate) {
    this->panicGate = gate;
}

void IsrPlugbox::assignGate(uint8_t index, InterruptGate *gate) {
    this->gates[index] = gate;
}

void IsrPlugbox::releaseGate(uint8_t index) {
    // TODO: also disable the interrupt?
    this->gates[index] = nullptr;
}

void IsrPlugbox::releaseGate(InterruptGate *gate) {
    for (size_t i = 0; i < GATE_COUNT; i++) {
        if (this->gates[i] == gate) {
            // TODO: also disable the interrupt?
            this->gates[i] = nullptr;
        }
    }
}

void IsrPlugbox::triggerGate(InterruptState *state) const {
    // TODO: assert?
    if (this->gates[state->interruptIndex] == nullptr) {
        if (this->panicGate != nullptr) {
            this->panicGate->trigger(state);
        } else {
            panic(state);
        }
    } else {
        this->gates[state->interruptIndex]->trigger(state);
    }
}