/**
 * machine/interrupts/isr.h
 *
 * Contains struct and method declaration for the basic ISR.
**/

#ifndef DANAOS_MACHINE_INTERRUPTS_ISR_H_
#define DANAOS_MACHINE_INTERRUPTS_ISR_H_

#include "boot/idt.h"
#include <stdint.h>

// TODO: this is very platform dependent and may also be needed
// for other stuff; relocate to cpu.h?
struct CpuState {
    uint32_t eax;
    uint32_t ebx;
    uint32_t ecx;
    uint32_t edx;
    uint32_t es;
    uint32_t fs;
    uint32_t gs;
    uint32_t esi;
    uint32_t edi;
    uint32_t ebp;
} __attribute__((packed));

// TODO: in case of a privilege change, ESP and segment selector
// are also saved!
struct InterruptState {
    // Manually saved registers
    // TODO: this isn't exactly something most interrupts should see
    CpuState cpuState;

    uint32_t interruptIndex;
    // Error code (0 if not given by the interrupt/exception)
    uint32_t errorCode;

    // Always saved by the CPU
    uint32_t eip;
    uint32_t cs;
    uint32_t eflags;
} __attribute__((packed));

class InterruptGate {
public:
    virtual ~InterruptGate(); // TODO: needed?

    virtual void enable()                       = 0;
    virtual void disable()                      = 0;
    virtual void trigger(InterruptState *state) = 0; // TODO: what parameters does this take?
};

// TODO: is usage of uint8_t etc. in API bad?
class IsrPlugbox {
private:
    // TODO: stack or heap (or pre-allocated)?
    // TODO: pull interrupt count into header somewhere
    static constexpr uint32_t GATE_COUNT = InterruptDescriptorTable::GATE_COUNT;
    InterruptGate *gates[GATE_COUNT];
    InterruptGate *panicGate; // TODO: better solution necessary (record index and so on)!

    // TODO: remove and replace with default gate
    void panic(InterruptState *state) const;

public:
    IsrPlugbox();
    ~IsrPlugbox();

    void setPanicGate(InterruptGate *gate);

    void assignGate(uint8_t index, InterruptGate *gate);
    void releaseGate(uint8_t index);
    void releaseGate(InterruptGate *gate);

    void triggerGate(InterruptState *state) const;
};

extern "C" void isrHandler(InterruptState *state);

#endif // DANAOS_MACHINE_INTERRUPTS_ISR_H_