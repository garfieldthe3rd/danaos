#include "machine/memory/memmanager.h"
#include "libk/math.h"
#include "libk/string.h"
#include "main/kernel.h"

const size_t MemoryManager::PAGE_SIZE           = 4096;
const size_t MemoryManager::PAGE_DIRECTORY_SIZE = 1024;
const size_t MemoryManager::PAGE_TABLE_SIZE     = 1024;
const size_t MemoryManager::MAX_PAGES           = 0x10000;

constexpr size_t MemoryManager::getPageNumber(uintptr_t address) {
    return address / PAGE_SIZE;
}

constexpr uintptr_t MemoryManager::getPageAddress(size_t pageNumber) {
    return pageNumber * PAGE_SIZE;
}

constexpr size_t MemoryManager::getPageDirIndex(uintptr_t address) {
    return address / (PAGE_SIZE * PAGE_TABLE_SIZE);
}

constexpr uintptr_t MemoryManager::getPageAddress(size_t dirIndex, size_t tableIndex) {
    return dirIndex * PAGE_SIZE * PAGE_TABLE_SIZE + tableIndex * PAGE_SIZE;
}

constexpr size_t MemoryManager::getPageTableIndex(uintptr_t address) {
    // TODO: optimize?
    return (address % (PAGE_SIZE * PAGE_TABLE_SIZE)) / PAGE_SIZE;
}

constexpr uint32_t MemoryManager::pageAlignUp(uint32_t address) {
    return (address % PAGE_SIZE != 0) ? ((address / PAGE_SIZE) + 1) * PAGE_SIZE : address;
}

constexpr uint32_t MemoryManager::pageAlignDown(uint32_t address) {
    return (address / PAGE_SIZE) * PAGE_SIZE;
}

MemoryManager::MemoryManager()
    : memBitmap(0, 0),
      totalPageFrames(0),
      availablePageFrames(0),
      highestPageAddress(0),
      pageDirectory(nullptr),
      pageTables(
          reinterpret_cast<PageTable *>(PAGE_SIZE * PAGE_TABLE_SIZE * (PAGE_DIRECTORY_SIZE - 1))) {
}

MemoryManager::~MemoryManager() {
    // TODO: this should only happen when the kernel shuts down, so add smthing for hybernation
    // etc.?
}

uintptr_t MemoryManager::init(const multiboot_memory_map_t *memMap, uint32_t length,
                              const KernelStructure &structure) {
    // Kernel::log("Initializing Memory Manager...");
    uintptr_t bitmapAddress = 0;

    // Loop through the memory area structure provided by the multiboot loader to
    // find a suitable place for our bitmap
    Kernel::console.log("Memory map:");
    uintptr_t currAddress = reinterpret_cast<uintptr_t>(memMap);
    while (currAddress < (reinterpret_cast<uintptr_t>(memMap) + length)) {
        multiboot_mmap_entry *currEntry = reinterpret_cast<multiboot_mmap_entry *>(currAddress);

        Kernel::console.log("\t[ {} | {} | {} | {} ]", reinterpret_cast<void *>(currEntry->addrLow),
                            currEntry->addrHigh != 0, currEntry->lenLow, currEntry->type);

        // Our 32bit kernel can't handle memory beyond the 4GB mark
        // TODO: what about areas with higher lengths from below 4GB?
        if ((currEntry->addrHigh == 0) && (currEntry->lenHigh == 0) &&
            (currEntry->type == MULTIBOOT_MEMORY_AVAILABLE)) {
            // TODO: this is only valid for a 32-bit machine!!!
            if (pageAlignDown(currEntry->addrLow + currEntry->lenLow - 1) > highestPageAddress) {
                highestPageAddress = pageAlignDown(currEntry->addrLow + currEntry->lenLow - 1);
            }

            uintptr_t freeAfterKernel = currEntry->lenLow;
            if (structure.kernelEnd >= currEntry->addrLow + currEntry->lenLow) {
                // The area is entirely consumed by the kernel
                freeAfterKernel = 0;
            } else if (structure.kernelEnd >= currEntry->addrLow) {
                // The kernel ends somewhere in this area and we need to adjust the free
                // space accordingly
                freeAfterKernel -= structure.kernelEnd - currEntry->addrLow;
            }

            // TODO: actually we don't need the full possible space, only up to the
            // highest available page frame
            if ((bitmapAddress == 0) && (freeAfterKernel >= MAX_PAGES / 8)) {
                // If the area is large enough to provide one bit per page frame
                // (2^32 memory size / (size of each page * bits per byte))

                // and the area does not intersect with the kernel, we may use it
                bitmapAddress = currEntry->addrLow + currEntry->lenLow - freeAfterKernel;
            }
        }

        // Go on to the next entry in the memory map
        currAddress += currEntry->size + sizeof(currEntry->size);
    }

    if (bitmapAddress == 0) {
        Kernel::panic("Error: Could not find a suitable location for the pmm bitmap!");
    }

    // Locate the bitmap to the location we found, but WITH the virtual offset
    // TODO: make sure that the virtual memory is mapped
    memBitmap.relocate(reinterpret_cast<uintptr_t>(bitmapAddress) + structure.virtualAddrOffset,
                       getPageNumber(highestPageAddress));
    Kernel::console.log("Memory bitmap relocated to {} ({} bytes / {} page frames)!",
                        reinterpret_cast<void *>(bitmapAddress), memBitmap.getSizeInBytes(),
                        math::ceilDiv(memBitmap.getSizeInBytes(), PAGE_SIZE));

    // Now that we have our bitmap structure, we need to loop over the memory map
    // again to fill it with the correct values!
    memBitmap.clear();

    currAddress = reinterpret_cast<uintptr_t>(memMap);
    while (currAddress < (reinterpret_cast<uintptr_t>(memMap) + length)) {
        multiboot_mmap_entry *currEntry = reinterpret_cast<multiboot_mmap_entry *>(currAddress);

        // Our 32bit kernel can't handle memory beyond the 4GB mark
        if ((currEntry->addrHigh == 0) && (currEntry->lenHigh == 0) &&
            (currEntry->type == MULTIBOOT_MEMORY_AVAILABLE)) {
            // If the memory is available, we can all page frames in that area
            // TODO: this code assumes that addrLow is always aligned to a page
            // boundary. Is that
            // true? (Prolly not)
            for (uintptr_t base = pageAlignUp(currEntry->addrLow);
                 base < currEntry->addrLow + currEntry->lenLow; base += PAGE_SIZE) {
                totalPageFrames++;
                freePageFrame(base);
            }
        }

        // Go on to the next entry in the memory map
        currAddress += currEntry->size + sizeof(currEntry->size);
    }

    // Now we actually free'd too much memory; the kernel and the bitmap are
    // already in use, so we
    // have to allocate those frames
    if (allocPageFrames(structure.kernelStart,
                        pageAlignUp(structure.kernelSize + structure.kernelStart -
                                    pageAlignDown(structure.kernelStart)) /
                            PAGE_SIZE) == 0) {
        Kernel::panic("The kernel memory is already in use!");
    }

    // Allocate bitmap
    // The first bitmap page might overlap with the kernel!
    allocPageFrames(bitmapAddress, pageAlignUp(memBitmap.getSizeInBytes() + bitmapAddress -
                                               pageAlignDown(bitmapAddress)) /
                                       PAGE_SIZE);

    // Everything below the 1MB mark is (usually) reserved for some special stuff.
    // At the very least the first page frame has to be unusable if we want to
    // continue to identify page frames by their first address.
    // Otherwise, we may return an accidental nullptr
    for (size_t i = 0; i < 0x100000; i += PAGE_SIZE) {
        allocPageFrame(i);
    }

    // We need access to the boot page directory
    extern PageDirectoryEntry asmPageDir __asm__("_pageDirectory");
    PageDirectoryEntry *bootPageDir = &asmPageDir;
    uintptr_t bootPageDirPhys =
        reinterpret_cast<uintptr_t>(bootPageDir) - structure.virtualAddrOffset;

    // Create a "fake" page directory which we'll use to create the necessary entries
    uintptr_t pageDirPhys = reinterpret_cast<uintptr_t>(this->allocPageFrame());
    // Manually map the virtual addresses for the boot page directory
    pageDirectory =
        reinterpret_cast<PageDirectoryEntry *>(pageDirPhys + structure.virtualAddrOffset);
    size_t pageDirIndex = getPageDirIndex(reinterpret_cast<uintptr_t>(pageDirectory));
    if (!bootPageDir[pageDirIndex].present) {
        bootPageDir[pageDirIndex] =
            PageDirectoryEntry(true, true, false, true, true, true, pageDirPhys);
    }

    if (pageDirectory == nullptr) {
        Kernel::panic("Could not allocate memory for page directory!");
    }

    // Initialize the page directory entries to empty
    for (size_t i = 0; i < PAGE_TABLE_SIZE; i++) {
        pageDirectory[i] = PageDirectoryEntry();
    }

    // Map the last page directory entry to the page directory
    // This allows us to modify page tables by accessing 0xFFC0000 - 0xFFFFFFFF!
    pageDirectory[PAGE_TABLE_SIZE - 1] =
        PageDirectoryEntry(true, true, false, true, true, false, pageDirPhys);
    // And since we need to work with them, also add this mapping to the current page directory in
    // the boot section
    bootPageDir[PAGE_TABLE_SIZE - 1] =
        PageDirectoryEntry(true, true, false, true, true, false, pageDirPhys);

    // TODO: make these pages global?
    // Identity-map the kernel regions with corresponding read/write access
    // Code (read-only)
    for (uintptr_t code = structure.codeStart; code < structure.codeEnd; code += PAGE_SIZE) {
        this->mapPage(code + structure.virtualAddrOffset, code, true, true);
    }
    // RoData (read-only)
    for (uintptr_t rodata = structure.rodataStart; rodata < structure.rodataEnd;
         rodata += PAGE_SIZE) {
        this->mapPage(rodata + structure.virtualAddrOffset, rodata, true, true);
    }
    // Data (read-write)
    for (uintptr_t data = structure.dataStart; data < structure.dataEnd; data += PAGE_SIZE) {
        this->mapPage(data + structure.virtualAddrOffset, data, true, false);
    }
    // Bss (read-write)
    for (uintptr_t bss = structure.bssStart; bss < structure.bssEnd; bss += PAGE_SIZE) {
        this->mapPage(bss + structure.virtualAddrOffset, bss, true, false);
    }
    // Identity-map the pmm bitmap
    for (uintptr_t bitmap = pageAlignDown(bitmapAddress);
         bitmap < pageAlignDown(bitmapAddress) + memBitmap.getSizeInBytes(); bitmap += PAGE_SIZE) {
        this->mapPage(bitmap + structure.virtualAddrOffset, bitmap, true, false);
    }
    // TODO: We also need to modify the bitmap's interior pointer
    // TODO: remove this when we don't need the console anymore
    // Identity-map the cga screen memory
    for (uintptr_t mem = Kernel::cga.MEM_ADDRESS;
         mem < Kernel::cga.MEM_ADDRESS + Kernel::cga.size(); mem += PAGE_SIZE) {
        this->mapPage(mem, mem - structure.virtualAddrOffset, true, false);
    }
    // Finally, map the page directory in the temporary page directory also
    this->mapPage(reinterpret_cast<uintptr_t>(pageDirectory), pageDirPhys, true, false);

    // Exchange the used page directory with the new one we've build
    __asm__ volatile("movl %%eax, %%cr3" : : "a"(pageDirPhys) :);

    // Copy the content of the (new used) page directory to the boot page dir
    std::memcpy(bootPageDir, pageDirectory, PAGE_TABLE_SIZE * sizeof(PageDirectoryEntry));
    // Perform the good ol' switcheroo and use the (now overwritten) boot page directory again
    __asm__ volatile("movl %%eax, %%cr3" : : "a"(bootPageDirPhys) :);

    // Since we now use a different physical address for the page directory, we
    // need to correct its last entry
    uintptr_t tempPageAddr = reinterpret_cast<uintptr_t>(pageDirectory);
    pageDirectory          = bootPageDir;
    pageDirectory[PAGE_DIRECTORY_SIZE - 1] = PageDirectoryEntry(true, true, false, true, true, false, bootPageDirPhys);

    // Now that that's clear, deallocate the temporary page directory
    this->freePage(tempPageAddr);
    Kernel::console.log(KernelConsole::LogLevel::STATUS, "Initialized memory manager!");

    // Return a possible virtual address for the heap
    return tempPageAddr;
}

uintptr_t MemoryManager::allocPageFrame() {
    size_t pageNumber = memBitmap.getFirstSet();
    if (pageNumber == memBitmap.INVALID_INDEX) {
        return 0;
    }
    memBitmap.clear(pageNumber);
    availablePageFrames--;
    return getPageAddress(pageNumber);
}

uintptr_t MemoryManager::allocPageFrame(uintptr_t physicalAddr) {
    size_t pageNumber = getPageNumber(physicalAddr);
    if (memBitmap.getAndClear(pageNumber)) {
        availablePageFrames--;
        return getPageAddress(pageNumber);
    } else {
        return 0;
    }
}

uintptr_t MemoryManager::allocPageFrames(size_t count) {
    // TODO: very inefficient!!
    for(size_t frame = 1; frame < memBitmap.getSizeInBits(); frame++) {
        size_t countFrame = frame;
        bool foundFreeArea = true;
        for(; countFrame < frame + count; countFrame++) {
            if (!memBitmap.getAndClear(countFrame)) {
                // If page isn't available, deallocate all previously allocated pages
                for (size_t dealloc = frame; dealloc < countFrame; dealloc++) {
                    memBitmap.set(dealloc);
                }
                foundFreeArea = false;
                break;
            }
        }
        // If we found our area, we can stop
        if(foundFreeArea) {
            availablePageFrames -= count;
            return getPageAddress(frame);
        } else {
            // Otherwise we can move the frame counter forward since we know there's not enough 
            // space between frame and countFrame
            frame = countFrame;
        }
    }
    return 0;
}

uintptr_t MemoryManager::allocPageFrames(uintptr_t physicalAddr, size_t count) {
    size_t firstPage = getPageNumber(physicalAddr);

    for (size_t page = firstPage; page < firstPage + count; page++) {
        // Check if page is available and mark as used
        if (!memBitmap.getAndClear(page)) {
            // If page isn't available, deallocate all previously allocated pages
            for (size_t dealloc = firstPage; dealloc < page; dealloc++) {
                memBitmap.set(dealloc);
            }
            return 0;
        }
    }

    availablePageFrames -= count;
    return getPageAddress(firstPage);
}

void MemoryManager::freePageFrame(uintptr_t physicalAddr) {
    size_t pageNumber = getPageNumber(physicalAddr);
    if (!memBitmap.get(pageNumber)) {
        availablePageFrames++;
        memBitmap.set(pageNumber);
    }
}

void MemoryManager::freePageFrames(uintptr_t address, size_t count) {
    for (size_t page = getPageNumber(address); page < getPageNumber(address) + count; page++) {
        if (!memBitmap.get(page)) {
            availablePageFrames++;
            memBitmap.set(page);
        }
    }
}

uintptr_t MemoryManager::mapPage(uintptr_t virtualAddr, uintptr_t physicalAddr, bool kernel,
                             bool readOnly) {
    size_t dirIndex   = getPageDirIndex(virtualAddr);
    size_t tableIndex = getPageTableIndex(virtualAddr);
    if (!pageDirectory[dirIndex].present) {
        pageDirectory[dirIndex] =
            PageDirectoryEntry(true, true, true, true, false, false,
                               this->allocPageFrame());
        pageTables[dirIndex] = PageTable();
    }

    if (!pageTables[dirIndex].getPage(tableIndex).present) {
        pageTables[dirIndex].getPage(tableIndex) =
            Page(true, !readOnly, !kernel, true, false, false, physicalAddr);
    }
    return virtualAddr;
}

void MemoryManager::unmapPage(uintptr_t virtualAddr) {
    size_t dirIndex   = getPageDirIndex(virtualAddr);
    size_t tableIndex = getPageTableIndex(virtualAddr);
    // TODO: clear and free page directory entry when there are no more pages
    if (pageDirectory[dirIndex].present) {
        pageTables[dirIndex].getPage(tableIndex) = Page();
    }
    // TODO: virtual or physical address?
    invalidateTlb(virtualAddr);
}

void MemoryManager::invalidateTlb(uintptr_t address) {
    __asm__ volatile("invlpg (%0)" : : "b"(address) : "memory");
}

uintptr_t MemoryManager::allocPage(bool kernel, bool readOnly) {
    uintptr_t physicalAddr = this->allocPageFrame();
    if (physicalAddr == 0) {
        Kernel::panic("No more memory for paging! Swap not yet implemented!");
    }
    // TODO: more efficient search!
    for(size_t frame = 1; frame < PAGE_DIRECTORY_SIZE * PAGE_TABLE_SIZE; frame++) {
        if(mapPage(frame * PAGE_SIZE, physicalAddr, kernel, readOnly) != 0) {
            return frame * PAGE_SIZE;
        }
    }
    // If no virtual address has been found, we can (have to!) free the page frame
    this->freePageFrame(physicalAddr);
    return 0;
}

uintptr_t MemoryManager::allocPages(size_t count, bool kernel, bool readOnly) {
    // TODO: more efficient search!
    // TODO: change API so that we don't need a dummy physical address...
    count = math::min(count, PAGE_DIRECTORY_SIZE * PAGE_TABLE_SIZE);
    for(size_t frame = 1; frame < PAGE_DIRECTORY_SIZE * PAGE_TABLE_SIZE - count + 1; frame++) {
        size_t currFrame = frame;
        bool foundArea = true;
        for(; currFrame < frame + count; currFrame++) {
            if(mapPage(frame * PAGE_SIZE, 0, kernel, readOnly) == 0) {
                // Unmap all the pages
                for(size_t dealloc = frame; dealloc <= currFrame; dealloc++) {
                    unmapPage(frame * PAGE_SIZE);
                }
                break;
            }
        }
        
        if(foundArea) {
            for(size_t pageFrame = frame; pageFrame < frame + count; pageFrame++) {
                mapPage(pageFrame * PAGE_SIZE, this->allocPageFrame(), kernel, readOnly);
            }
            return frame * PAGE_SIZE;
        } else {
            frame = currFrame;
        }
    }
    return 0;
}

uintptr_t MemoryManager::allocPage(uintptr_t virtualAddr, bool kernel, bool readOnly) {
    uintptr_t physicalAddr = this->allocPageFrame();
    if (physicalAddr == 0) {
        Kernel::panic("No more memory for paging! Swap not yet implemented!");
    }
    if(mapPage(virtualAddr, physicalAddr, kernel, readOnly) == 0) {
        this->freePageFrame(physicalAddr);
        return 0;
    } else {
        return virtualAddr;
    }
}

uintptr_t MemoryManager::allocPages(uintptr_t virtualAddr, size_t count, bool kernel, bool readOnly) {
    // Allocate the pages consecutively
    for(uintptr_t address = virtualAddr; address < virtualAddr + count*PAGE_SIZE; address += PAGE_SIZE) {
        // If there is a page already allocated, reverse what we've aligned so far
        if(allocPage(address, kernel, readOnly) == 0) {
            for(uintptr_t free = virtualAddr; free < address; free += PAGE_SIZE) {
                freePage(free);
            }
            return 0;
        }
    }
    return virtualAddr;
}

void MemoryManager::freePage(uintptr_t virtualAddr) {
    size_t dirIndex   = getPageDirIndex(virtualAddr);
    size_t tableIndex = getPageTableIndex(virtualAddr);

    if (pageDirectory[dirIndex].present) {
        if (pageTables[dirIndex].getPage(tableIndex).present) {
            this->freePageFrame(pageTables[dirIndex].getPage(tableIndex).pageAddress << 12);
            pageTables[dirIndex].getPage(tableIndex) = Page();
            // TODO: virtual or physical address?
            invalidateTlb(virtualAddr);
        }
    }
}

uintptr_t MemoryManager::getPhysicalAddress(uintptr_t virtualAddr) const {
    size_t dirIndex   = getPageDirIndex(virtualAddr);
    size_t tableIndex = getPageTableIndex(virtualAddr);
    uintptr_t offset = virtualAddr - getPageAddress(dirIndex, tableIndex);

    if (pageDirectory[dirIndex].present) {
        if (pageTables[dirIndex].getPage(tableIndex).present) {
            return (pageTables[dirIndex].getPage(tableIndex).pageAddress << 12)
                    + offset;
        }
    }
    return 0;
}

size_t MemoryManager::getAvailablePageFrames() const {
    return availablePageFrames;
}

size_t MemoryManager::getTotalPageFrames() const {
    return totalPageFrames;
}

uintptr_t MemoryManager::getHighestPageAddress() const {
    return highestPageAddress;
}
