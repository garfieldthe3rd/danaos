/*
 * machine/memory/memmanager.h
**/

#ifndef DANAOS_MACHINE_MEMORY_MEMMANAGER_H_
#define DANAOS_MACHINE_MEMORY_MEMMANAGER_H_

#include "boot/kernelstruct.h"
#include "boot/multiboot.h"
#include "machine/memory/paging.h"
#include "util/bitmap.h"
#include <stddef.h>
#include <stdint.h>

// TODO: ensure thread safety!

class MemoryManager {
private:
    static const size_t PAGE_SIZE;
    static const size_t PAGE_DIRECTORY_SIZE;
    static const size_t PAGE_TABLE_SIZE;
    static const size_t MAX_PAGES;

    Bitmap<size_t> memBitmap;
    size_t totalPageFrames;
    size_t availablePageFrames;
    uintptr_t highestPageAddress;

    PageDirectoryEntry *pageDirectory;
    PageTable *pageTables;

    static constexpr size_t getPageNumber(uintptr_t address);
    static constexpr uintptr_t getPageAddress(size_t pageNumber);
    static constexpr size_t getPageDirIndex(uintptr_t address);
    static constexpr uintptr_t getPageAddress(size_t dirIndex, size_t tableIndex);
    static constexpr size_t getPageTableIndex(uintptr_t address);
    static constexpr uint32_t pageAlignUp(uint32_t address);
    static constexpr uint32_t pageAlignDown(uint32_t address);

    MemoryManager();
    MemoryManager(const MemoryManager &) = delete;
    void operator=(MemoryManager) = delete;
    ~MemoryManager();

    uintptr_t init(const multiboot_memory_map_t *memMap, uint32_t length,
                   const KernelStructure &structure);
    uintptr_t allocPageFrame();
    uintptr_t allocPageFrame(uintptr_t physicalAddr);
    uintptr_t allocPageFrames(size_t count);
    uintptr_t allocPageFrames(uintptr_t physicalAddr, size_t count);
    void freePageFrame(uintptr_t physicalAddr);
    void freePageFrames(uintptr_t physicalAddr, size_t count);
    uintptr_t mapPage(uintptr_t virtualAddr, uintptr_t physicalAddr, bool kernel = false,
                  bool readOnly = false);
    void unmapPage(uintptr_t virtualAddr);
    void invalidateTlb(uintptr_t address);

    friend class Kernel;
    friend class Heap;

public:
    uintptr_t allocPage(bool kernel = false, bool readOnly = false);
    uintptr_t allocPage(uintptr_t virtualAddr, bool kernel = false, bool readOnly = false);
    uintptr_t allocPages(size_t count, bool kernel = false, bool readOnly = false);
    uintptr_t allocPages(uintptr_t virtualAddr, size_t count, bool kernel = false, bool readOnly = false);
    void freePage(uintptr_t virtualAddr);
    // TODO: alloc/free multiple consecutive pages at once

    uintptr_t getPhysicalAddress(uintptr_t virtualAddr) const;

    size_t getAvailablePageFrames() const;
    size_t getTotalPageFrames() const;
    uintptr_t getHighestPageAddress() const;
};

#endif // DANAOS_MACHINE_MEMORY_MEMMANAGER_H_