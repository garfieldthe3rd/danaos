#ifndef DANAOS_MACHINE_MEMORY_PAGING_H_
#define DANAOS_MACHINE_MEMORY_PAGING_H_

#include <stddef.h>
#include <stdint.h>

struct PageDirectoryEntry {
    uint8_t present : 1;
    uint8_t readWrite : 1;
    uint8_t userSupervisor : 1;
    uint8_t writeThrough : 1;
    uint8_t cacheDisabled : 1;
    uint8_t accessed : 1;
    uint8_t reserved : 1;
    uint8_t size4Mb : 1;
    uint8_t ignored : 4;
    uintptr_t tableAddress : 20;

    PageDirectoryEntry();
    PageDirectoryEntry(uintptr_t raw);
    PageDirectoryEntry(bool present, bool readWrite, bool userSupervisor, bool writeThrough,
                       bool cacheDisabled, bool size4Mb, uintptr_t tableAddres);
};

struct Page {
    uint8_t present : 1;
    uint8_t readWrite : 1;
    uint8_t userSupervisor : 1;
    uint8_t writeThrough : 1;
    uint8_t cacheDisabled : 1;
    uint8_t accessed : 1;
    uint8_t dirty : 1;
    uint8_t reserved : 1;
    uint8_t global : 1;
    uint8_t ignored : 3;
    uintptr_t pageAddress : 20;

    Page();
    Page(uintptr_t raw);
    Page(bool present, bool readWrite, bool userSupervisor, bool writeThrough, bool cacheDisabled,
         bool global, uintptr_t pageAddress);
};

struct PageTable {
private:
    Page pages[1024];

public:
    PageTable();

    Page &getPage(size_t num);
    const Page &getPage(size_t num) const;
};

#endif // DANAOS_MACHINE_MEMORY_PAGING_H_