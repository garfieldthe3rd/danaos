#include "machine/memory/heap.h"
#include "libk/new.h"
#include "machine/locks/scopedlock.h"
#include "main/kernel.h"

Heap::Header::Header(uintptr_t address, size_t length, Header *prev, Header *next, bool free)
    : address(address), length(length), prev(prev), next(next), free(free) {
}

Heap::Heap(uintptr_t start, size_t initPages, bool isKernel)
    : firstHeader(nullptr),
      lastHeader(nullptr),
      heapLock(),
      IS_KERNEL(isKernel) {
    // TODO: When does the heap ever shrink? Sometime we will have to give pages back...

    // We have to initialize the heap with a page size of at least one
    // (the one where the heap object is already placed)
    initPages = (initPages < 1) ? 1 : initPages;
    Kernel::console.log(KernelConsole::LogLevel::STATUS, "Initialized heap to []!", start);
    if(Kernel::memManager.allocPages(start, initPages - 1, isKernel, false) == 0) {
        Kernel::panic("Could not allocate initial heap pages!");
    }
    firstHeader = new (reinterpret_cast<void *>(start + sizeof(Heap))) Header(start + sizeof(Heap), MemoryManager::PAGE_SIZE * initPages, nullptr, nullptr, true);
    lastHeader = firstHeader;
}

Heap::~Heap() {
    // TODO: give back all the pages etc.
}

void Heap::expand(size_t pages) {
    // TODO: force consecutive memory?
    // get the current end of the heap and add as many consecutive pages to it as necessary
    uintptr_t nextPage = lastHeader->address + lastHeader->length;
    if (Kernel::memManager.allocPages(nextPage, pages, IS_KERNEL, false) == 0) {
        // TODO: can we work around that by having e.g. reserved as a block type?
        // TODO: is it even necessary to work around?
        // TODO: add max. heap size
        Kernel::panic("Could not allocate page at next location for heap!");
        return ;
    }
    lastHeader->next = new (reinterpret_cast<void *>(nextPage))
            Header(nextPage, pages * MemoryManager::PAGE_SIZE, lastHeader, nullptr, true);
    lastHeader = lastHeader->next;

    // Since we should have gotten consecutive pages, it is advisable to merge these if possible
    // TODO: currently we may stop at the first disjoint block; but there may be more isolated,
    // yet split ones -> scan whole heap?
    mergeBackwards(lastHeader);
}

void *Heap::alloc(size_t size, bool pageAligned) {
    ScopedLock<SpinLock> lock(heapLock);
    // TODO: clear the memory?
    // TODO: allow page alignment
    if (pageAligned) {
        Kernel::panic("Page aligned memory allocation is not supported yet!");
        return nullptr;
    }

    // Find a free block of memory in the linked list
    Header *currBlock = firstHeader;
    while (currBlock != nullptr) {
        if (currBlock->free && (currBlock->length >= size + sizeof(Header))) {
            break;
        }
        currBlock = currBlock->next;
    }

    // We haven't found a block large enough, so we need to expand the heap
    if (currBlock == nullptr) {
        expand((size % MemoryManager::PAGE_SIZE != 0) ? (size / MemoryManager::PAGE_SIZE + 1) : (size / MemoryManager::PAGE_SIZE));
        currBlock = lastHeader;
    }

    // Reserve the necessary space and add a new block if space is left over
    currBlock->free = false;
    if (currBlock->length >= 2 * sizeof(Header) + size) {
        Header *old         = currBlock->next;
        uintptr_t blockAddr = currBlock->address + sizeof(Header) + size;
        currBlock->next     = new (reinterpret_cast<void *>(blockAddr))
            Header(blockAddr, currBlock->length - sizeof(Header) - size, currBlock, old, true);
        // Set the list links to incorporate the block
        if(old == nullptr) {
            lastHeader = currBlock->next;
        } else {
            old->prev = currBlock->next;
        }
        currBlock->length = sizeof(Header) + size;
    }

    return reinterpret_cast<void *>(currBlock->address + sizeof(Header));
}

void Heap::free(void *ptr) {
    ScopedLock<SpinLock> lock(heapLock);
    uintptr_t addr    = reinterpret_cast<uintptr_t>(ptr);
    Header *currBlock = firstHeader;

    // Look through the list to find the block that belongs to the freed pointer
    // TODO: while more secure, this is also slow...
    while ((currBlock != nullptr) && (currBlock->address <= addr)) {
        if ((currBlock->address + sizeof(Header) == addr) && !currBlock->free) {
            currBlock->free = true;

            // Make sure that consecutive free blocks get merged
            mergeForwards(currBlock);
            mergeBackwards(currBlock);

            return;
        }
        currBlock = currBlock->next;
    }
}

void Heap::mergeForwards(Header *header) {
    if (header == nullptr) {
        // TODO: replace by assert/simple check?
        Kernel::panic("Tried to merge a nullptr header!");
        return;
    }

    // As long as our current header has a free successor, merge them by
    // adapting the header's length and changing the links of the elements
    while ((header->next != nullptr) && header->next->free &&
           (header->next->address == header->address + header->length)) {
        header->length += header->next->length;
        header->next       = header->next->next;
        if(header->next != nullptr) {
            header->next->prev = header;
        } else {
            // If there's no next we're the last header
            lastHeader = header;
        }
    }
}

void Heap::mergeBackwards(Header *header) {
    if (header == nullptr) {
        // TODO: replace by assert/simple check?
        Kernel::panic("Tried to merge a nullptr header!");
        return;
    }

    // As long as our current header has a free predecessor, merge them by
    // adapting the header's length and changing the links of the elements
    while ((header->prev != nullptr) && header->prev->free &&
           (header->address == header->prev->address + header->prev->length)) {
        header->prev->length += header->length;
        header->prev->next = header->next;
        if(header->next != nullptr) {
            header->next->prev = header->prev;
        }
        header = header->prev;
    }
}
