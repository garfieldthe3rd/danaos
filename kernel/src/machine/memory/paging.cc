#include "machine/memory/paging.h"

PageDirectoryEntry::PageDirectoryEntry()
    : present(0),
      readWrite(0),
      userSupervisor(0),
      writeThrough(0),
      cacheDisabled(0),
      accessed(0),
      reserved(0),
      size4Mb(0),
      ignored(0),
      tableAddress(0) {
}

PageDirectoryEntry::PageDirectoryEntry(uintptr_t raw)
    : present((raw >> 0) & 0x01),
      readWrite((raw >> 1) & 0x01),
      userSupervisor((raw >> 2) & 0x01),
      writeThrough((raw >> 3) & 0x01),
      cacheDisabled((raw >> 4) & 0x01),
      accessed((raw >> 5) & 0x01),
      reserved(0),
      size4Mb((raw >> 7) & 0x01),
      ignored(0),
      tableAddress((raw >> 12) & 0xFFFFF) {
}

PageDirectoryEntry::PageDirectoryEntry(bool present, bool readWrite, bool userSupervisor,
                                       bool writeThrough, bool cacheDisabled, bool size4Mb,
                                       uintptr_t tableAddress)
    : present(present),
      readWrite(readWrite),
      userSupervisor(userSupervisor),
      writeThrough(writeThrough),
      cacheDisabled(cacheDisabled),
      accessed(0),
      reserved(0),
      size4Mb(size4Mb),
      ignored(0),
      tableAddress((tableAddress >> 12) & 0xFFFFF) {
}

Page::Page()
    : present(0),
      readWrite(0),
      userSupervisor(0),
      writeThrough(0),
      cacheDisabled(0),
      accessed(0),
      dirty(0),
      reserved(0),
      global(0),
      ignored(0),
      pageAddress(0) {
}

Page::Page(uintptr_t raw)
    : present((raw >> 0) & 0x01),
      readWrite((raw >> 1) & 0x01),
      userSupervisor((raw >> 2) & 0x01),
      writeThrough((raw >> 3) & 0x01),
      cacheDisabled((raw >> 4) & 0x01),
      accessed((raw >> 5) & 0x01),
      dirty((raw >> 6) & 0x01),
      reserved(0),
      global((raw >> 8) & 0x01),
      ignored(0),
      pageAddress((raw >> 12) & 0xFFFFF) {
}

Page::Page(bool present, bool readWrite, bool userSupervisor, bool writeThrough, bool cacheDisabled,
           bool global, uintptr_t pageAddress)
    : present(present),
      readWrite(readWrite),
      userSupervisor(userSupervisor),
      writeThrough(writeThrough),
      cacheDisabled(cacheDisabled),
      accessed(0),
      dirty(0),
      reserved(0),
      global(global),
      ignored(0),
      pageAddress((pageAddress >> 12) & 0xFFFFF) {
}

PageTable::PageTable() {
    for (size_t i = 0; i < 1024; i++) {
        pages[i] = Page();
    }
}

Page &PageTable::getPage(size_t num) {
    return pages[num];
}

const Page &PageTable::getPage(size_t num) const {
    return pages[num];
}