/**
 * machine/memory/heap.h
**/

#ifndef DANAOS_MACHINE_MEMORY_HEAP_H_
#define DANAOS_MACHINE_MEMORY_HEAP_H_

#include "machine/locks/spinlock.h"
#include <stddef.h>
#include <stdint.h>

class Heap {
private:
    // TODO: address is redundant!
    struct Header {
        uintptr_t address;
        size_t length;
        Header *prev;
        Header *next;
        bool free;

        Header(uintptr_t address, size_t length, Header *prev, Header *next, bool free);
    };

    Header *firstHeader;
    Header *lastHeader;
    SpinLock heapLock;

    const bool IS_KERNEL;

    void expand(size_t size);

    void mergeForwards(Header *header);
    void mergeBackwards(Header *header);
    void mergeAll();

public:
    Heap(uintptr_t start, size_t initSize = 4096, bool isKernel = false);
    ~Heap();

    void *alloc(size_t size, bool pageAligned = false);
    void free(void *ptr);
};

#endif // DANAOS_MACHINE_MEMORY_HEAP_H_
