/**
 * libk/atomic.h
**/

#ifndef DANAOS_LIBK_ATOMIC_H_
#define DANAOS_LIBK_ATOMIC_H_

#include "machine/locks/scopedlock.h"
#include "machine/locks/spinlock.h"

template <class T>
class Atomic {
private:
    T value;
    SpinLock accessLock;

public:
    Atomic() : value(), accessLock() {
    }

    Atomic(const T &value) : value(value), accessLock() {
    }

    Atomic(T &&value) : value(value), accessLock() {
    }

    Atomic(const Atomic<T> &atomic) = delete;
    Atomic(Atomic<T> &&atomic)      = delete;

    // TODO: how?
    /*bool isLockFree() const {
        return accessLock.
    }*/

    T load() const {
        ScopedLock<SpinLock> lock(accessLock);
        return value;
    }

    void store(T val) {
        ScopedLock<SpinLock> lock(accessLock);
        value = val;
    }

    Atomic<T> &operator=(T val) {
        store(val);
    }

    Atomic<T> &operator=(const Atomic<T> &) = delete;
    Atomic<T> &operator=(Atomic<T> &&atomic) = delete;
};

#endif // DANAOS_LIBK_ATOMIC_H_