/**
 * libk/string.h
 *
 * Some utility functions regarding memory operations.
**/

#ifndef DANAOS_LIBK_STRING_H_
#define DANAOS_LIBK_STRING_H_

#include "libk/math.h"
#include <stddef.h>

namespace std {

void *memcpy(void *destination, const void *source, size_t num);
void *memmove(void *destination, const void *source, size_t num);
void *memset(void *ptr, char value, size_t num);

size_t strlen(const char *str);

// TODO: constexpr?
// TODO: better implementation (e.g. overallocation etc.)!
class string {
private:
    size_t currSize;
    size_t currCapacity;
    char *internalStr;

public:
    static constexpr size_t npos = -1;

    string();
    string(size_t rep, char c);
    string(const char *str);
    string(const char *str, size_t count);
    string(const string &str);
    string(const string &other, size_t pos);
    string(const string &other, size_t pos, size_t count);
    string(string &&str);
    ~string();

    bool empty() const;
    size_t size() const;
    size_t length() const;
    void reserve(size_t capacity);
    size_t capacity() const;

    char &front();
    const char &front() const;
    char &back();
    const char &back() const;

    string &resize(size_t count);
    string &resize(size_t count, char c);
    string substr(size_t pos = 0, size_t count = npos) const;

    size_t find(const string &str, size_t pos = 0) const;
    size_t find(const char *str, size_t pos = 0) const;
    size_t find(const char *str, size_t pos, size_t count) const;
    size_t find(char c, size_t pos = 0) const;

    char &operator[](size_t pos);
    const char &operator[](size_t pos) const;

    string &operator=(const string &str);
    string &operator=(string &&str);
    string &operator=(const char *str);
    string &operator=(char c);

    string &operator+=(const string &s);
    string &operator+=(const char *s);
    string &operator+=(char c);

    // TODO: substring-append
    string &append(size_t count, char c);
    string &append(const string &str);
    string &append(const char *str);
    string &append(const char *str, size_t count);

    const char *data() const;
};

string &operator+(string s1, const string &s2);
string &operator+(string s1, const char *s2);
string &operator+(string s, char c);
string &operator+(const char *s1, const string &s2);
string &operator+(char c, const string &s);

template <class T>
string to_string(T val);

} // namespace std

#endif // DANAOS_LIBK_STRING_H_