/**
 * libk/string.h
**/

#include "libk/string.h"
#include "main/kernel.h"
#include <stdint.h>

namespace std {

void *memcpy(void *destination, const void *source, size_t num) {
    size_t *dstLarge       = static_cast<size_t *>(destination);
    const size_t *srcLarge = static_cast<const size_t *>(source);

    // Copy whole machine words for as long as possible
    // TODO: is that even faster than simply using bytes? My guess would be that
    // the compiler optimizes this anyhow
    size_t i = 0;
    for (i = 0; i < (num / sizeof(size_t)); i++) {
        dstLarge[i] = srcLarge[i];
    }

    // If there are bytes left, copy these bytewise
    if (i * sizeof(size_t) < num) {
        char *dstChar       = static_cast<char *>(destination);
        const char *srcChar = static_cast<const char *>(source);
        for (i = i * sizeof(size_t); i < num; i++) {
            dstChar[i] = srcChar[i];
        }
    }

    return destination;
}

void *memmove(void *destination, const void *source, size_t num) {
    if (reinterpret_cast<uintptr_t>(destination) <= reinterpret_cast<uintptr_t>(source)) {
        return memcpy(destination, source, num);
    } else {
        // TODO: also copy whole words, like in memcpy!
        char *dst       = static_cast<char *>(destination);
        const char *src = static_cast<const char *>(source);

        // SInce source and destination may overlap in a way that writing to
        // destination via
        // forward traversal may overwrite the source, we have to traverse backwards
        for (size_t i = 1; i <= num; i++) {
            dst[num - i] = src[num - i];
        }
        return destination;
    }
}

void *memset(void *ptr, char value, size_t num) {
    char *target = static_cast<char *>(ptr);

    for (size_t i = 0; i < num; i++) {
        target[i] = value;
    }
    return ptr;
}

size_t strlen(const char *str) {
    size_t length = 0;
    while (*(str++) != 0) {
        length++;
    }

    return length;
}

string::string() : currSize(0), currCapacity(currSize), internalStr(new char[currCapacity + 1]) {
    internalStr[0] = '\0';
}

string::string(size_t rep, char c)
    : currSize(rep), currCapacity(currSize), internalStr(new char[currCapacity + 1]) {
    for (size_t i = 0; i < currSize; i++) {
        internalStr[i] = c;
    }
    this->internalStr[currSize] = '\0';
}

string::string(const char *str)
    : currSize(strlen(str)), currCapacity(currSize), internalStr(new char[currCapacity + 1]) {
    memcpy(this->internalStr, str, currSize);
    this->internalStr[currSize] = '\0';
}

string::string(const char *str, size_t count)
    : currSize(math::min(count, strlen(str))),
      currCapacity(currSize),
      internalStr(new char[currCapacity + 1]) {
    memcpy(this->internalStr, str, currSize);
    this->internalStr[currSize] = '\0';
}

string::string(const string &str)
    : currSize(str.currSize), currCapacity(currSize), internalStr(new char[currCapacity + 1]) {
    memcpy(this->internalStr, str.internalStr, currSize);
    this->internalStr[currSize] = '\0';
}

string::string(const string &str, size_t pos)
    : currSize(str.currSize - math::min(pos, str.currSize)),
      currCapacity(currSize),
      internalStr(new char[currCapacity + 1]) {
    pos = math::min(pos, str.currSize);
    memcpy(&this->internalStr[pos], &str.internalStr[pos], currSize);
    this->internalStr[currSize] = '\0';
}

string::string(const string &str, size_t pos, size_t count) {
    pos   = math::min(pos, str.currSize);
    count = math::min(count, str.currSize - pos);

    currSize     = count;
    currCapacity = currSize;
    internalStr  = new char[currSize + 1];

    memcpy(&this->internalStr[pos], &str.internalStr[pos], currSize);
    this->internalStr[currSize] = '\0';
}

string::string(string &&str)
    : currSize(str.currSize), currCapacity(str.currCapacity), internalStr(str.internalStr) {
    str.internalStr = nullptr;
}

string::~string() {
    if (internalStr != nullptr) {
        delete[] internalStr;
    }
}

bool string::empty() const {
    return currSize == 0;
}

size_t string::size() const {
    return currSize;
}

size_t string::length() const {
    return currSize;
}

void string::reserve(size_t capacity) {
    if (capacity > currCapacity) {
        currCapacity = capacity;
        char *newStr = new char[currCapacity + 1];

        memcpy(newStr, this->internalStr, currSize);
        newStr[currSize] = '\0';

        delete[] this->internalStr;
        this->internalStr = newStr;
    }
}

size_t string::capacity() const {
    return currCapacity;
}

char &string::front() {
    return this->operator[](0);
}
const char &string::front() const {
    return this->operator[](0);
}

char &string::back() {
    if (empty()) {
        return front();
    }
    return this->operator[](this->size() - 1);
}
const char &string::back() const {
    if (empty()) {
        return front();
    }
    return this->operator[](this->size() - 1);
}

string &string::resize(size_t count) {
    return this->resize(count, char());
}

string &string::resize(size_t count, char c) {
    // Size reduction, if wanted, is done implicitly (no reduction in capacity!)
    if (count > currSize) {
        if (currCapacity < count) {
            this->reserve(3 * count / 2);

            for (size_t i = currSize; i < count; i++) {
                this->internalStr[i] = c;
            }
        }
    }

    currSize                    = count;
    this->internalStr[currSize] = '\0';

    return *this;
}

string string::substr(size_t pos, size_t count) const {
    return string(*this, pos, count);
}

size_t string::find(const string &str, size_t pos) const {
    if (str.empty()) {
        return 0;
    }

    // TODO: this can be improved!
    size_t lastPos = pos;
    while ((lastPos = this->find(str[0])) != string::npos) {
        // If we've hit the end of the string, just break
        if (lastPos + str.length() > currSize) {
            break;
        }

        bool match = true;

        for (size_t i = 0; i < str.size(); i++) {
            if (this->internalStr[i + lastPos] != str[i]) {
                match = false;
                break;
            }
        }

        if (match) {
            return lastPos;
        }
    }

    return string::npos;
}

size_t string::find(const char *str, size_t pos) const {
    return this->find(str, pos, strlen(str));
}

size_t string::find(const char *str, size_t pos, size_t count) const {
    if (count == 0) {
        return 0;
    }

    // TODO: this can be improved!
    size_t lastPos = pos;
    while ((lastPos = this->find(str[0])) != string::npos) {
        // If we've hit the end of the string, just break
        if (lastPos + count > currSize) {
            break;
        }

        bool match = true;

        for (size_t i = 0; i < count; i++) {
            if (this->internalStr[i + lastPos] != str[i]) {
                match = false;
                break;
            }
        }

        if (match) {
            return lastPos;
        }
    }

    return string::npos;
}

size_t string::find(char c, size_t pos) const {
    for (size_t i = pos; i < currSize; i++) {
        if (internalStr[i] == c) {
            return i;
        }
    }

    return string::npos;
}

char &string::operator[](size_t pos) {
    return internalStr[pos];
}

const char &string::operator[](size_t pos) const {
    return internalStr[pos];
}

string &string::operator=(const string &str) {
    // Don't do stuff if it isn't necessary (same object)
    if (this == &str) {
        return *this;
    }

    currSize     = str.currSize;
    currCapacity = currSize;

    // Delete current buffer
    delete[] this->internalStr;
    this->internalStr = new char[currSize + 1];

    memcpy(this->internalStr, str.internalStr, currSize);
    this->internalStr[currSize] = '\0';

    return *this;
}

string &string::operator=(string &&str) {
    // Don't do stuff if it isn't necessary (same object)
    if (this == &str) {
        return *this;
    }

    currSize          = str.currSize;
    currCapacity      = currSize;
    this->internalStr = str.internalStr;
    str.internalStr   = nullptr;

    return *this;
}

string &string::operator=(const char *str) {
    currSize     = strlen(str);
    currCapacity = currSize;

    // Delete current buffer
    delete[] this->internalStr;
    this->internalStr = new char[currSize + 1];

    memcpy(this->internalStr, str, currSize);
    this->internalStr[currSize] = '\0';

    return *this;
}

string &string::operator=(char c) {
    currSize     = 1;
    currCapacity = currSize;

    // Delete current buffer
    delete[] this->internalStr;
    this->internalStr = new char[currSize + 1];

    this->internalStr[0]        = c;
    this->internalStr[currSize] = '\0';

    return *this;
}

string &string::operator+=(const string &s) {
    return this->append(s);
}

string &string::operator+=(const char *str) {
    return this->append(str);
}

string &string::operator+=(char c) {
    return this->append(1, c);
}

string &string::append(size_t count, char c) {
    // Create new buffer with matching length
    if (currCapacity < currSize + count) {
        this->reserve(3 * (currSize + count) / 2);
    }

    // Append characters onto the buffer
    for (size_t i = currSize; i < currSize + count; i++) {
        this->internalStr[i] = c;
    }

    currSize += count;
    this->internalStr[currSize] = '\0';

    return *this;
}

string &string::append(const string &s) {
    // Create new buffer with matching length
    if (currCapacity < currSize + s.currSize) {
        this->reserve(3 * (currSize + s.currSize) / 2);
    }

    // Append string onto the buffer
    memcpy(&this->internalStr[currSize], s.internalStr, s.currSize);

    currSize += s.currSize;
    this->internalStr[currSize] = '\0';

    return *this;
}

string &string::append(const char *str) {
    size_t strLen = strlen(str);
    // Create new buffer with matching length
    if (currCapacity < currSize + strLen) {
        this->reserve(3 * (currSize + strLen) / 2);
    }

    // Append string onto the buffer
    memcpy(&this->internalStr[currSize], str, strLen);

    currSize += strLen;
    this->internalStr[currSize] = '\0';

    return *this;
}

string &string::append(const char *str, size_t count) {
    count = math::min(count, strlen(str));
    // Create new buffer with matching length
    if (currCapacity < currSize + count) {
        this->reserve(3 * (currSize + count) / 2);
    }

    // Append string onto the buffer
    memcpy(&this->internalStr[currSize], str, count);

    currSize += count;
    this->internalStr[currSize] = '\0';

    return *this;
}

const char *string::data() const {
    return internalStr;
}

string &operator+(string s1, const string &s2) {
    return (s1 += s2);
}

string &operator+(string s1, const char *s2) {
    return (s1 += s2);
}

string &operator+(string s, char c) {
    return (s += c);
}

string &operator+(const char *s1, const string &s2) {
    return (string(s1) += s2);
}

string &operator+(char c, string s) {
    return string(1, c) += s;
}

template <>
string to_string<bool>(bool val) {
    if (val) {
        return std::string("true");
    } else {
        return std::string("false");
    }
}

template <>
string to_string<unsigned long>(unsigned long val) {
    std::string str;

    for (long i = math::logl(10, val); i >= 0; i--) {
        unsigned long currFig   = math::powl(10, i);
        unsigned long currDigit = val / currFig;

        // If currDigit > 9, continue with capital letters (only for hex base)
        str += ((char) (currDigit + 48 + (currDigit > 9 ? 7 : 0)));
        val -= currDigit * currFig;
    }

    return str;
}

template <>
string to_string<long>(long val) {
    if (val < 0) {
        return "-" + to_string<unsigned long>(-val);
    } else {
        return to_string<unsigned long>(val);
    }
}

template <>
string to_string<char>(char val) {
    return to_string<long>(val);
}

template <>
string to_string<unsigned char>(unsigned char val) {
    return to_string<unsigned long>(val);
}

template <>
string to_string<short>(short val) {
    return to_string<long>(val);
}

template <>
string to_string<unsigned short>(unsigned short val) {
    return to_string<unsigned long>(val);
}

template <>
string to_string<int>(int val) {
    return to_string<long>(val);
}

template <>
string to_string<unsigned int>(unsigned int val) {
    return to_string<unsigned long>(val);
}

} // namespace std
