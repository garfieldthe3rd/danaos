/**
 * libk/queue.h
**/

#ifndef DANAOS_LIBK_QUEUE_H_
#define DANAOS_LIBK_QUEUE_H_

#include <stddef.h>

template <class T>
class Queue {
private:
    struct QueueEntry {
        T data;
        QueueEntry *next;

        QueueEntry(const T &data) : data(data), next(nullptr) {
        }

        QueueEntry(T &&data) : data(data), next(nullptr) {
        }

        template <class... Args>
        QueueEntry(Args &&... args) : data(static_cast<Args &&>(args)...), next(nullptr) {
        }
    };

    void insert(QueueEntry *entry) {
        if (head == nullptr) {
            head = entry;
            tail = head;
        } else {
            tail->next = entry;
            tail       = entry;
        }

        currSize++;
    }

    QueueEntry *head;
    QueueEntry *tail;
    size_t currSize;

public:
    Queue() : head(nullptr), tail(head), currSize(0) {
    }

    Queue(const Queue &queue) : Queue() {
        if (queue.head != nullptr) {
            this->head = new QueueEntry(queue.head->data, nullptr);
            this->tail = this->head;
            currSize   = 1;
        }

        QueueEntry *curr = queue.head;
        while (curr != nullptr) {
            this->push(new QueueEntry(curr->data));
        }
    }

    Queue(Queue &&queue) : head(queue.head), tail(queue.tail), currSize(queue.currSize) {
        queue.head     = nullptr;
        queue.tail     = nullptr;
        queue.currSize = 0;
    }

    ~Queue() {
        while (head != nullptr) {
            QueueEntry *next = head->next;
            delete head;
            head = next;
        }
    }

    Queue &operator=(const Queue &queue) {
        if (this == &queue) {
            return *this;
        }

        while (head != nullptr) {
            QueueEntry *next = head->next;
            delete head;
            head = next;
        }
        tail     = nullptr;
        currSize = 0;

        if (queue.head != nullptr) {
            this->head = new QueueEntry(queue.head->data, nullptr);
            this->tail = this->head;
            currSize   = 1;
        }

        QueueEntry *curr = queue.head;
        while (curr != nullptr) {
            this->push(new QueueEntry(curr->data));
        }

        return *this;
    }

    Queue &operator=(Queue &&queue) {
        if (this == &queue) {
            return *this;
        }

        while (head != nullptr) {
            QueueEntry *next = head->next;
            delete head;
            head = next;
        }

        this->head     = queue.head;
        this->tail     = queue.tail;
        this->currSize = queue.currSize;
        queue.head     = nullptr;
        queue.tail     = nullptr;
        queue.currSize = 0;

        return *this;
    }

    void clear() {
        while (size()) {
            pop();
        }
    }

    void push(const T &data) {
        this->insert(new QueueEntry(data));
    }

    void push(T &&data) {
        this->insert(new QueueEntry(data));
    }

    template <class... Args>
    void emplace(Args &&... args) {
        this->insert(new QueueEntry(static_cast<Args &&>(args)...));
    }

    void pop() {
        if (head != nullptr) {
            QueueEntry *next = head->next;
            delete head;
            head = next;

            currSize--;
        }
    }

    size_t size() const {
        return currSize;
    }

    bool empty() const {
        return size() == 0;
    }

    T &front() {
        return head->data;
    }

    const T &front() const {
        return head->data;
    }

    T &back() {
        return tail->data;
    }

    const T &back() const {
        return tail->data;
    }
};

#endif // DANAOS_LIBK_QUEUE_H_