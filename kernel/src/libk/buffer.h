/**
 * libk/buffer.h
**/

#ifndef DANAOS_LIBK_BUFFER_H_
#define DANAOS_LIBK_BUFFER_H_

#include <stddef.h>

// TODO: limit capacity to > 0?
template <class T, size_t CAPACITY>
class Buffer {
private:
    // TODO: indicate buffer overflow?
    T buffer[CAPACITY];
    size_t begin;
    size_t end;

public:
    Buffer() : buffer{}, begin(0), end(0) {
    }

    Buffer(const Buffer<T, CAPACITY> &buf) : buffer{}, begin(buf.begin), end(buf.end) {
        for (size_t i = 0; i < CAPACITY; i++) {
            this->buffer[i] = buf.buffer[i];
        }
    }

    Buffer(Buffer<T, CAPACITY> &&buf) : buffer{}, begin(buf.begin), end(buf.end) {
        for (size_t i = 0; i < CAPACITY; i++) {
            this->buffer[i] = static_cast<T &&>(buf.buffer[i]);
        }
    }

    Buffer<T, CAPACITY> &operator=(const Buffer<T, CAPACITY> &buf) {
        if (this == &buf) {
            return *this;
        }

        for (size_t i = 0; i < CAPACITY; i++) {
            this->buffer[i] = buf.buffer[i];
        }
        this->begin = buf.begin;
        this->end   = buf.end;

        return *this;
    }

    Buffer<T, CAPACITY> &operator=(Buffer<T, CAPACITY> &&buf) {
        if (this == &buf) {
            return *this;
        }

        for (size_t i = 0; i < CAPACITY; i++) {
            this->buffer[i] = static_cast<T &&>(buf.buffer[i]);
        }
        this->begin = buf.begin;
        this->end   = buf.end;

        return *this;
    }

    const T &operator[](size_t index) const {
        // TODO: more efficient?
        return buffer[(index + begin) % CAPACITY];
    }

    constexpr size_t capacity() const {
        return CAPACITY;
    }

    size_t size() const {
        if (end < begin) {
            return (CAPACITY - begin - 1) + end;
        } else {
            return end - begin;
        }
    }

    void clear(const T &val = T()) {
        for (size_t i = 0; i < CAPACITY; i++) {
            buffer[(i + begin) % CAPACITY] = val;
        }
        begin = 0;
        end   = 0;
    }

    void push(const T &val) {
        // TODO: what happens if we get full?
        buffer[end++] = val;
        if (end >= CAPACITY) {
            end = 0;
        }
    }

    void pop() {
        if (begin != end) {
            begin++;
            if (begin >= CAPACITY) {
                begin = 0;
            }
        }
    }
};

#endif // DANAOS_LIBK_BUFFER_H_