/**
 * libk/math.h
**/

#ifndef DANAOS_LIBK_MATH_H_
#define DANAOS_LIBK_MATH_H_

namespace math {

long powl(long base, unsigned long exp);
unsigned long logl(unsigned long base, unsigned long arg);

template <class T>
T ceilDiv(T a, T b) {
    return 1 + (a - 1) / b;
}

template <class T>
T min(T a, T b) {
    return (a < b) ? a : b;
}

template <class T>
T max(T a, T b) {
    return (a > b) ? a : b;
}

} // namespace math

#endif // DANAOS_LIBK_MATH_H_