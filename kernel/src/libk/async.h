/**
 * libk/async.h
**/

#ifndef DANAOS_LIBK_ASYNC_H_
#define DANAOS_LIBK_ASYNC_H_

#include "machine/locks/scopedlock.h"
#include "machine/locks/spinlock.h"

template <class T>
class Promise;

// TODO: Improve! What if the future gets deconstructed on either side?
template <class T>
class Future {
private:
    friend class Promise<T>;

    bool done;
    T *val;

    SpinLock readyLock;
    SpinLock valLock;

    bool isReady() {
        ScopedLock<SpinLock> lock(readyLock);
        return done;
    }

    void setReady(bool ready) {
        ScopedLock<SpinLock> lock1(readyLock);
        ScopedLock<SpinLock> lock2(valLock);
        done = ready;
    }

    void setValue(const T &val) {
        ScopedLock<SpinLock> lock(valLock);
        if (this->val != nullptr) {
            delete this->val;
        }
        this->val = new T(val);
    }

public:
    Future() : done(false), val(nullptr) {
    }

    Future(const Future &future) = delete;

    Future(Future &&future) : done(future.done), val(future.val) {
        // TODO: hmmmm that's not atomic...
        future.val = nullptr;
        future.setReady(true);
    }

    ~Future() {
        // TODO: is that really smart??
        ScopedLock<SpinLock> lock1(valLock);
        ScopedLock<SpinLock> lock2(readyLock);
        if (val != nullptr) {
            delete val;
        }
    }

    Future &operator=(const Future &future) = delete;
    Future &operator                        =(Future &&future) {
        if (this == &future) {
            return *this;
        }

        this->setReady(future.isReady());
        this->setValue(future.get());
        // TODO: hmmmm that's not atomic...
        future.val = nullptr;
        future.setReady(true);

        return *this;
    }

    void wait() const {
        while (!isReady()) {
        }
    }

    T get() {
        ScopedLock<SpinLock> lock(valLock);
        return *val;
    }
};

template <class T>
class Promise {
private:
    Future<T> *future;

public:
    Promise() : future(nullptr) {
    }

    Promise(const Promise &promise) = delete;

    Promise(Promise &&promise) : future(promise.future) {
        promise.future = nullptr;
    }

    ~Promise() {
        // No need to delete future; it got created and transferred in getFuture()
    }

    Promise &operator=(const Promise &promise) = delete;

    Promise &operator=(Promise &&promise) {
        // TODO: lock?
        this->future   = promise.future;
        promise.future = nullptr;
    }

    void setValue(T &&val) {
        // TODO: lock this!
        future->setValue(val);
        future->setReady(true);
    }

    void setValue(const T &val) {
        // TODO: lock this!
        future->setValue(val);
        future->setReady(true);
    }

    Future<T> getFuture() {
        Future<T> f;
        this->future = &f;
        return f;
    }
};

#endif // DANAOS_LIBK_ASYNC_H_