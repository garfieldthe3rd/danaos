/**
 * libk/new.cc
 *
 * Implementation of placement new
**/

#include "libk/new.h"
#include "main/kernel.h"

void *operator new(size_t size) {
    return Kernel::heap->alloc(size);
}

void *operator new(size_t size, void *ptr) noexcept {
    (void) size;
    return ptr;
}

void *operator new[](size_t size) {
    return Kernel::heap->alloc(size);
}

void operator delete(void *ptr) {
    Kernel::heap->free(ptr);
}

void operator delete(void *ptr, size_t size) {
    (void) size;
    Kernel::heap->free(ptr);
}

void operator delete[](void *ptr) {
    Kernel::heap->free(ptr);
}

void operator delete[](void *ptr, size_t size) {
    (void) size;
    Kernel::heap->free(ptr);
}
