/**
 * util/bitmap.h
 *
 * 'Bitmap' is a simple bitmap stored as an array of 32-bit integers (this
 *should
 * probably changed for a 64-bit attempt). To accomodate for very early kernel
 *stages,
 * this particular implementation allows for manual specification of the array's
 *memory
 * address via one of the constructors.
**/

#include "libk/new.h"
#include <stddef.h>
#include <stdint.h>

#ifndef DANAOS_UTIL_BITMAP_H_
#define DANAOS_UTIL_BITMAP_H_

template <class T = size_t>
class Bitmap {
private:
    T *map;
    size_t bits;
    size_t bytes;
    size_t arrayLength;

    // Computes the index to the array element which contains the given bit
    static constexpr size_t bitToIndex(size_t bit) {
        return bit / (sizeof(T) * 8);
    }

    // Computes the offset within the array element which contains the given bit
    static constexpr size_t bitToOffset(size_t bit) {
        return bit % (sizeof(T) * 8);
    }

    // Computes the bit position from a given index and offset
    static constexpr size_t indexOffsetToBit(size_t index, size_t offset) {
        return offset + index * sizeof(T) * 8;
    }

    // These methods are only available to the physical memory manager, which
    // needs to relocate the internal bitmap during initialization
    Bitmap(size_t bits, uintptr_t placementAddress)
        : map(reinterpret_cast<T *>(placementAddress)),
          bits(bits),
          bytes(1 + (bits - 1) / 8),
          arrayLength(1 + (bytes - 1) / sizeof(T)) {
        // TODO: assert bits < INVALID_INDEX
    }

    void relocate(uintptr_t address, size_t bits) {
        this->map         = reinterpret_cast<T *>(address);
        this->bits        = bits;
        this->bytes       = 1 + (bits - 1) / 8;
        this->arrayLength = 1 + (bytes - 1) / sizeof(T);
        clear();
    }

    friend class MemoryManager;

public:
    // Error value for the index retrieving functions
    static const size_t INVALID_INDEX = static_cast<size_t>(~0);

    Bitmap(size_t bits)
        : map(nullptr),
          bits(bits),
          bytes(1 + (bits - 1) / 8),
          arrayLength(1 + (bytes - 1) / sizeof(T)) {
        map = new T[arrayLength];
        this->clear();
    }

    Bitmap(const Bitmap& bitmap) :
                    map(new T[bitmap.arrayLength]),
                    bits(bitmap.bits), bytes(bitmap.bytes),
                    arrayLength(bitmap.arrayLength) {
        for (size_t i = 0; i < arrayLength; i++) {
            this->map[i] = bitmap.map[i];
        }
    }

    Bitmap(Bitmap &&bitmap)
        : map(bitmap.map), bits(bitmap.bits), bytes(bitmap.bytes), arrayLength(bitmap.arrayLength) {
        bitmap.map         = nullptr;
        bitmap.bits        = 0;
        bitmap.bytes       = 0;
        bitmap.arrayLength = 0;
    }

    ~Bitmap() {
        if (map != nullptr) {
            delete[] map;
        }
    }

    void set(size_t bit) {
        // TODO: assert(bit < bits);
        map[bitToIndex(bit)] |= 1 << bitToOffset(bit);
    }

    bool getAndSet(size_t bit) {
        bool status = map[bitToIndex(bit)] & (1 << bitToOffset(bit));
        set(bit);
        return status;
    }
    // Set all bits
    void set() {
        for (size_t i = 0; i <= bitToIndex(bits); i++)
            map[i]    = ~(0);
    }

    void clear(size_t bit) {
        // TODO: assert(bit < bits);
        map[bitToIndex(bit)] &= ~(1 << bitToOffset(bit));
    }

    bool getAndClear(size_t bit) {
        bool status = map[bitToIndex(bit)] & (1 << bitToOffset(bit));
        clear(bit);
        return status;
    }
    // Clear all bits
    void clear() {
        for (size_t i = 0; i <= bitToIndex(bits); i++)
            map[i]    = 0;
    }

    bool get(size_t bit) const {
        // TODO: assert(bit < bits);
        return map[bitToIndex(bit)] & (1 << bitToOffset(bit));
    }

    size_t getSizeInBits() const {
        return bits;
    }

    size_t getSizeInBytes() const {
        return 1 + (bits - 1) / 8;
    }

    // Get the index of the first clear/set bit. Returns INVALID_INDEX if none are
    size_t getFirstClear() const {
        // Loop over the bitmap until we find an entry that is not completely set
        for (size_t index = 0; index <= bitToIndex(bits); index++) {
            // Found an entry with at least one clear bit
            if (map[index] != ~static_cast<size_t>(0)) {
                for (size_t offset = 0; offset < sizeof(T) * 8; offset++) {
                    if (!(map[index] & (1 << offset)))
                        return indexOffsetToBit(index, offset);
                }
            }
        }

        return INVALID_INDEX;
    }

    size_t getFirstSet() const {
        // Loop over the bitmap until we find an entry that is not completely set
        for (size_t index = 0; index < arrayLength; index++) {
            // Found an entry with at least one clear bit
            if (map[index] != 0) {
                for (size_t offset = 0; offset < sizeof(T) * 8; offset++) {
                    if (map[index] & (1 << offset))
                        return indexOffsetToBit(index, offset);
                }
            }
        }

        return INVALID_INDEX;
    }

    // Get the index of the last clear/set bit. Returns INVALID_INDEX if none are
    size_t getLastClear() const {
        // Loop over the bitmap until we find an entry that is not completely set
        for (size_t index = bitToIndex(bits) + 1; index > 0; index--) {
            // Found an entry with at least one clear bit
            if (map[index - 1] != ~static_cast<size_t>(0)) {
                for (size_t offset = sizeof(T) * 8; offset > 0; offset--) {
                    if (!(map[index - 1] & (1 << (offset - 1))))
                        return indexOffsetToBit(index - 1, offset - 1);
                }
            }
        }

        return INVALID_INDEX;
    }

    size_t getLastSet() const {
        // Loop over the bitmap until we find an entry that is not completely set
        for (size_t index = bitToIndex(bits) + 1; index > 0; index--) {
            // Found an entry with at least one clear bit
            if (map[index - 1] != 0) {
                for (size_t offset = sizeof(T) * 8; offset > 0; offset--) {
                    if (map[index - 1] & (1 << (offset - 1)))
                        return indexOffsetToBit(index - 1, offset - 1);
                }
            }
        }

        return INVALID_INDEX;
    }

    // TODO: resize()...
};

#endif // DANAOS_UTIL_BITMAP_H_